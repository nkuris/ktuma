﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class KtumaUser
    {
        int m_UserId;
        string m_UserName;
        string m_Password;
        bool m_IsActive;
        DateTime? m_DateCreated;
        int? m_CreatedBy;
        DateTime? m_LastUpdated;
        int? m_LastUpdatedBy;
        KtumaUser m_CreatedByUser;
        KtumaUser m_LastUpdatedByUser;
        Sviva m_Sviva;
        private int m_UnitId;
        private int m_GroupId;
        Group m_UserGroup;
        string m_YehidaName;

        public Sviva Sviva
        {
            get { return m_Sviva; }
            set { m_Sviva = value; }
        }
        public KtumaUser CreatedByUser
        { 
            get { return m_CreatedByUser; }
            set { m_CreatedByUser = value; }
        }
        

        public KtumaUser LastUpdatedByUser
        {
            get { return m_LastUpdatedByUser; }
            set { m_LastUpdatedByUser = value; }
        }

        public int? UpdatedBy
        {
            get { return m_LastUpdatedBy; }
            set { m_LastUpdatedBy = value; }
        }

        public DateTime? LastUpdated
        {
            get { return m_LastUpdated; }
            set { m_LastUpdated = value; }
        }

        public int? CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }

        public DateTime? DateCreated
        {
            get { return m_DateCreated; }
            set { m_DateCreated = value; }
        }

        public bool IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }
        KtumaUserType m_KtumaUserType;

        public KtumaUserType KtumaUserType
        {
            get { return m_KtumaUserType; }
            set { m_KtumaUserType = value; }
        }
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public string UserName
        {
            get { return m_UserName; }
            set { m_UserName = value; }
        }
        public int UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }

        public int GroupId
        {
            get { return m_GroupId; }
            set { m_GroupId = value; }
        }

        public int UnitId
        {
            get { return m_UnitId; }
            set { m_UnitId = value; }
        }
        public Group UserGroup
        {
            get { return m_UserGroup; }
            set { m_UserGroup = value; }
        }
        public string YehidaName
        {
            get { return m_YehidaName; }
            set { m_YehidaName = value; }
        }

        public KtumaUser() { }

        public KtumaUser(int userId, string userName, string password, bool isActive, KtumaUserType kut, Sviva sv)
        {
            m_UserId = userId;
            m_UserName = userName;
            m_Password = password;
            m_Sviva = sv;
            m_KtumaUserType = kut;
        }


        public KtumaUser(int userId, string userName, string password, bool isActive, DateTime? dateCreated,
            int? createdBy, DateTime? lastUpdated, int? lastUpdatedBy,  KtumaUserType kut, Sviva sv
            , int groupId, int unitId, Group group, string yehidaName)
        {
            m_UserId = userId;
            m_UserName = userName;
            m_Password = password;
            m_DateCreated = dateCreated;
            m_CreatedBy = createdBy;
            m_IsActive = isActive;
            m_LastUpdated = lastUpdated;
            m_LastUpdatedBy = lastUpdatedBy;
            m_KtumaUserType = kut;
            m_Sviva = sv;
            m_UnitId = unitId;
            m_GroupId = groupId;
            UserGroup = group;
            m_YehidaName = yehidaName;
        }


        public KtumaUser(int userId, string userName, string password, bool isActive, DateTime dateCreated,
           int createdBy, DateTime? lastUpdated, int? lastUpdatedBy, KtumaUserType kut, KtumaUser createdByUser,
            KtumaUser lastUpdatedByUser, Sviva sv)
        {
            m_UserId = userId;
            m_UserName = userName;
            m_Password = password;
            m_DateCreated = dateCreated;
            m_CreatedBy = createdBy;
            m_IsActive = isActive;
            m_LastUpdated = lastUpdated;
            m_LastUpdatedBy = lastUpdatedBy;
            m_KtumaUserType = kut;
            m_CreatedByUser = createdByUser;
            m_LastUpdatedByUser = lastUpdatedByUser;
            m_Sviva = sv;
        }

        public KtumaUser(int userId, string userName)
        {
            m_UserId = userId;
            m_UserName = userName;
        }
    }
}

public class Group
{
    private int m_GroupId;

    public int GroupId
    {
        get { return m_GroupId; }
        set { m_GroupId = value; }
    }

    private string m_GroupName;

    public string GroupName
    {
        get { return m_GroupName; }
        set { m_GroupName = value; }
    }

    public Group() { }

    public Group(int groupId, string groupName)
    {
        m_GroupId = groupId;
        m_GroupName = groupName;
    }
}
