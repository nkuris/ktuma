﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class Hanchaiut
    {
        private int m_Color;

        public int Color
        {
            get { return m_Color; }
            set { m_Color = value; }
        }
        private string m_Sector;

        public string Sector
        {
            get { return m_Sector; }
            set { m_Sector = value; }
        }
        private string m_Dilemot;

        public string Dilemot
        {
            get { return m_Dilemot; }
            set { m_Dilemot = value; }
        }
        private string m_Job;

        public string Job
        {
            get { return m_Job; }
            set { m_Job = value; }
        }
        private string m_Mosach;

        public string Mosach
        {
            get { return m_Mosach; }
            set { m_Mosach = value; }
        }
        private string m_Itcansoot;

        public string Itcansoot
        {
            get { return m_Itcansoot; }
            set { m_Itcansoot = value; }
        }
        private int m_Midrag;

        public int Midrag
        {
            get { return m_Midrag; }
            set { m_Midrag = value; }
        }
        private string m_Sherootim;

        public string Sherootim
        {
            get { return m_Sherootim; }
            set { m_Sherootim = value; }
        }
        public Hanchaiut()
        {

        }

        public Hanchaiut(int color, string sector, string dilemot,string job, string mosach, string itcansoot, int midrag, string sherootim)
        {
            m_Color = color;
            m_Sector = sector;
            m_Dilemot = dilemot;
            m_Job = job;
            m_Mosach = mosach;
            m_Itcansoot = itcansoot;
            m_Midrag = midrag;
            m_Sherootim = sherootim;
        }
    }
    
}
