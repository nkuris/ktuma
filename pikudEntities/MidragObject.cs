﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class MidragObject
    {
        private int m_MidragFinal;
        private Hanchaiut m_Hanchaiut;

        public Hanchaiut Hanchaiut
        {
            get { return m_Hanchaiut; }
            set { m_Hanchaiut = value; }
        }
        private List<string> m_ItgonenutRegionsList;

        public List<string> ItgonenutRegionsList
        {
            get { return m_ItgonenutRegionsList; }
            set { m_ItgonenutRegionsList = value; }
        }

        public int MidragFinal
        {
            get { return m_MidragFinal; }
            set { m_MidragFinal = value; }
        }


        public MidragObject() { }
        public MidragObject(int midragFinal,int color, string sector, string dilemot,string job, string mosach, string itcansoot, int midrag, string sherootim)
        {
            m_MidragFinal = midragFinal;
            m_ItgonenutRegionsList = new List<string>();
            m_Hanchaiut = new Hanchaiut(color, sector, dilemot, job, mosach, itcansoot, midrag, sherootim); 
        }

        public string ToStringAllRegions(int midragFinal)
        {
            string res = string.Empty;
            if (m_ItgonenutRegionsList.Count > 0)
            {
                for (int i = 0; i < m_ItgonenutRegionsList.Count - 1; i++)
                {
                    res += m_ItgonenutRegionsList[i] + ",";
                }
                //add the last one
                res += m_ItgonenutRegionsList[m_ItgonenutRegionsList.Count - 1];
            }
            return res;
        }
    }
}
