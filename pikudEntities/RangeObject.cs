﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class RangeObject
    {
        private int m_RangeId;

        public int RangeId
        {
            get { return m_RangeId; }
            set { m_RangeId = value; }
        }
        private string m_RangeText;

        public string RangeText
        {
            get { return m_RangeText; }
            set { m_RangeText = value; }
        }

        public RangeObject(int rangeId, string rangeText)
        {
            // TODO: Complete member initialization
            this.m_RangeId = rangeId;
            this.m_RangeText = rangeText;
        }
    }
}
