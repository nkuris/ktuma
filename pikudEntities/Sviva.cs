﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class Sviva
    {
        private int m_SvivaId;

        public int SvivaId
        {
            get { return m_SvivaId; }
            set { m_SvivaId = value; }
        }
        private string m_SvivaName;

        public string SvivaName
        {
            get { return m_SvivaName; }
            set { m_SvivaName = value; }
        }

        public Sviva() { }

        public Sviva(int svivaId, string svivaName)
        {
            m_SvivaId = svivaId;
            m_SvivaName = svivaName;
        }
    }
}
