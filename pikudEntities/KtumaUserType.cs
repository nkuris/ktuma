﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pikudEntities
{
    public class KtumaUserType
    {
        private int m_UserTypeId;

        public int UserTypeId
        {
            get { return m_UserTypeId; }
            set { m_UserTypeId = value; }
        }
        private string  m_Type;

        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        private string m_TypeDescription;

        public string TypeDescription
        {
            get { return m_TypeDescription; }
            set { m_TypeDescription = value; }
        }

        public KtumaUserType() { }

        public KtumaUserType(int userTypeId, string type, string typeDescription)
        {
            m_UserTypeId = userTypeId;
            m_Type = type;
            m_TypeDescription = typeDescription;
        }
    }
}
