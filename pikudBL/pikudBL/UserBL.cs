﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pikudEntities;

namespace PikudBL
{
    public class UserBL
    {

        public KtumaUser GetUserData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            KtumaUser user = null;
            DataSet DS = clsUtil.FillDS("dbo.GetUserData", arrInParamsNames, arrInParamsValues);
            if (DS != null && DS.Tables.Count > 0)
            {
                DataTable userData = DS.Tables[0];
                if (userData != null && userData.Rows.Count > 0)
                {
                    DataRow curDr = userData.Rows[0];
                    user = CreateKtumaUserFromDr(curDr);
                }
            }


            return user;
        }

        public KtumaUser Login(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            KtumaUser user = null;
            DataSet DS = clsUtil.FillDS("dbo.Login", arrInParamsNames, arrInParamsValues);
            if (DS != null && DS.Tables.Count > 0)
            {
                DataTable userData = DS.Tables[0];
                if (userData != null && userData.Rows.Count > 0)
                {
                    DataRow curDr = userData.Rows[0];
                    user = CreateKtumaUserFromDr(curDr);
                }
            }


            return user;
        }

        private KtumaUser CreateKtumaUserFromDr(DataRow curDr)
        {
            DateTime? lastUpdatedDate = null;
            DateTime? createdDate = null;
            int? lastUpUserId = null;
            bool isActive = true;
            int? createdByUserId = null;
            if (curDr["IsActive"] != DBNull.Value)
            {
                isActive = bool.Parse(curDr["IsActive"].ToString());
            }
            if (curDr["CreatedBy"] != DBNull.Value)
            {
                createdByUserId = int.Parse(curDr["CreatedBy"].ToString());
            }
            if (curDr["DateCreated"] != DBNull.Value)
            {
                createdDate = DateTime.Parse(curDr["DateCreated"].ToString());
            }
            if (curDr["LastUpdated"] != DBNull.Value)
            {
                lastUpdatedDate = DateTime.Parse(curDr["LastUpdated"].ToString());
            }
            if (curDr["LastUpdatedBy"] != DBNull.Value)
            {
                lastUpUserId = int.Parse(curDr["LastUpdatedBy"].ToString());
            }

            Sviva userSviva = null;
            KtumaUserType kut = null;
            KtumaUser user = null;
            Group gr = null;
            if (curDr["GroupId"] != DBNull.Value)
            {
                gr = new Group(int.Parse(curDr["GroupId"].ToString()), curDr["GroupName"].ToString());
            }
            if (curDr["svivaId"] != DBNull.Value)
            {
                userSviva = new Sviva(int.Parse(curDr["svivaId"].ToString()), curDr["svivaName"].ToString());
            }
            if (curDr["UserId"] != DBNull.Value)
            {
                kut = new KtumaUserType(int.Parse(curDr["UserTypeId"].ToString()), (curDr["Type"].ToString()), curDr["TypeDescription"].ToString());
                user = new KtumaUser(int.Parse(curDr["UserId"].ToString()), curDr["user_name"].ToString()
                , curDr["Password"].ToString(), isActive, createdDate, createdByUserId, lastUpdatedDate,
                lastUpUserId, kut, userSviva, int.Parse(curDr["GroupId"].ToString()), int.Parse(curDr["UnitId"].ToString()), gr,curDr["YehidaName"].ToString());
            }            

            if (lastUpUserId.HasValue)
            {
                KtumaUser lastUpdatedByUser = new KtumaUser(lastUpUserId.Value, curDr["lastUpdatedByUserName"].ToString());
                user.LastUpdatedByUser = lastUpdatedByUser;
            }
            if (createdByUserId.HasValue)
            {
                KtumaUser createdByUser = new KtumaUser(createdByUserId.Value, curDr["createdByUserName"].ToString());
                user.CreatedByUser = createdByUser;
            }
            return user;
        }

        public DataTable DeleteUser(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.deleteUser", arrInParamsNames, arrInParamsValues);
        }

        public UserListObj saveUser(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            DataTable res = clsUtil.FillDt("dbo.saveUser", arrInParamsNames, arrInParamsValues);
            UserListObj ulb = new UserListObj(res, "saveAction");
            return ulb;
        }
        public List< pikudEntities.KtumaUser> loadUsers()
        {
            DataTable dt = clsUtil.FillDtNoParams("loadUsers");
            return CreateKtumaUserList(dt);
        }

        public List<KtumaUser> CreateKtumaUserList(DataTable dt)
        {
            List<KtumaUser> res = new List<KtumaUser>();
            KtumaUser kt;
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    kt = CreateKtumaUserFromDr(dr);
                    res.Add(kt);
                }
            }
            return res;
        }

    
        public DataTable loadUsers(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.loadUsers", arrInParamsNames, arrInParamsValues);
        }
    }
}
