﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class ModBL : BaseBL
    {
        public static DataTable GetModData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetModFinal", arrInParamsNames, arrInParamsValues);
        }
    }
}
