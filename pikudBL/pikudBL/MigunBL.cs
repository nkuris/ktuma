﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class MigunBL : BaseBL
    {

        public static DataTable GetMigunData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetMigun", arrInParamsNames, arrInParamsValues);
        }
    }
}
