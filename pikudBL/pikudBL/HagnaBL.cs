﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class HagnaBL : BaseBL
    {
        public static DataTable GetHagnaData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetHagna", arrInParamsNames, arrInParamsValues);
        }
    }
}
