﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class ResourceBL :BaseBL
    {
        public bool DeleteResource(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.ExecuteSQL("dbo.deleteResource", arrInParamsNames, arrInParamsValues);
        }
        public DataTable SaveResource(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.saveResource", arrInParamsNames, arrInParamsValues);
        }

        public DataTable UpdateComboIndex(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.UpdateComboIndex", arrInParamsNames, arrInParamsValues);
        }
    }
}
