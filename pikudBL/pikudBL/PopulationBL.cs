﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class PopulationBL : BaseBL
    {
        public static DataTable GetPopulationData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            DataSet origDS = clsUtil.FillDS("dbo.GetPopulation", arrInParamsNames, arrInParamsValues);
            //DataTable returnTable = setItgonenutToRegionColumn(origDS);
            return origDS.Tables[0];
        }

        private static DataTable setItgonenutToRegionColumn(DataSet origDS)
        {
            origDS.Tables[0].Columns.Add("Exceptions", typeof(String));
            origDS.Tables[0].Columns.Add("ExplanationToException", typeof(String));
            Dictionary<int, ItgonenutException> itgonenutRegionsTo = new Dictionary<int, ItgonenutException>();
            string curRashutNames, curExceptionsExplanations;
            for (int i = 1; i <= 34; i++)
            {
                itgonenutRegionsTo.Add(i, new ItgonenutException());
            }
            for (int j = 0; j < origDS.Tables[1].Rows.Count; j++)
            {
                int curItgonenut = int.Parse(origDS.Tables[1].Rows[j]["ItgonenutRegionExceptTo"].ToString());
                if (curItgonenut != 0)
                {
                    itgonenutRegionsTo[curItgonenut].RashutNames += origDS.Tables[1].Rows[j]["rashutName"].ToString() +
                        "(" + origDS.Tables[1].Rows[j]["Name"].ToString() + ")" + "<br>";
                    itgonenutRegionsTo[curItgonenut].ExceptionsExplanations += origDS.Tables[1].Rows[j]["ExplanationToException"].ToString() + "<br>";
                }
            }
            for (int i = 0; i < 33; i++)
            {
                curRashutNames = itgonenutRegionsTo[i + 1].RashutNames;
                curExceptionsExplanations = itgonenutRegionsTo[i + 1].ExceptionsExplanations;
                origDS.Tables[0].Rows[i]["Exceptions"] = curRashutNames;
                origDS.Tables[0].Rows[i]["ExplanationToException"] = curExceptionsExplanations;
            }
            return origDS.Tables[0];
        }
    }

    public class ItgonenutException
    {
        private int m_ItgonenutId;
        private string m_RashutNames;
        private string m_ExceptionsExplanations;
        public ItgonenutException() { }

        public ItgonenutException(int itgonenutId, string rashutNames, string exceptionsExplanations)
        {
            m_ItgonenutId = itgonenutId;
            m_RashutNames = rashutNames;
            m_ExceptionsExplanations = exceptionsExplanations;
        }

        public int ItgonenutId
        {
            get { return m_ItgonenutId; }
            set { m_ItgonenutId = value; }
        }
        public string RashutNames
        {
            get { return m_RashutNames; }
            set { m_RashutNames = value; }
        }
        public string ExceptionsExplanations
        {
            get { return m_ExceptionsExplanations; }
            set { m_ExceptionsExplanations = value; }
        }

    }
}
