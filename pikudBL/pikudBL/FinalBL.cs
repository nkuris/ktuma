﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class FinalBL : BaseBL
    {
        public static DataTable GetFinalData(string svivaId)
        {
            string strInParamsNames = "svivaId";
            string strInParamsValues = svivaId;
            string[] arrInParamsNames = strInParamsNames.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
            string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
            return clsUtil.FillDt("dbo.GetFinal_Grade", arrInParamsNames, arrInParamsValues);
        }
    }
}
