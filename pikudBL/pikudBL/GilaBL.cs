﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class GilaBL : BaseBL
    {
        public static DataTable GetGilaData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getGila", arrInParamsNames, arrInParamsValues);
        }
    }
}
