﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using pikudEntities;

namespace PikudBL
{
    public class MidragBL : BaseBL
    {
        private List<MidragObject> midragList;

        public static DataTable GetMidragData()
        {
            return clsUtil.FillDtNoParams("dbo.GetMidragFinal");
        }

        public List<MidragObject> getMidragList(string svivaId)
        {             
            string strInParamsNames = "svivaId";
            string strInParamsValues = svivaId;
            string[] arrInParamsNames = strInParamsNames.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
            string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
            midragList = new List<MidragObject>();
            DataTable data = clsUtil.FillDt("dbo.GetMidragFinal", arrInParamsNames, arrInParamsValues);
            foreach (DataRow dr in data.Rows)
            {
                if (!midragList.Exists(p => p.MidragFinal == int.Parse(dr["Midrag_Final"].ToString())))
                {
                    midragList.Add(new MidragObject(int.Parse(dr["Midrag_Final"].ToString()),
                        int.Parse(dr["color"].ToString()), dr["sector"].ToString(), dr["dilemot"].ToString(), dr["job"].ToString(),
                        dr["mosach"].ToString(), dr["itcansoot"].ToString(), int.Parse(dr["midrag"].ToString()), dr["sherootim"].ToString()));
                }
                midragList.Find(p => p.MidragFinal == int.Parse(dr["Midrag_Final"].ToString())).ItgonenutRegionsList.Add(dr["Name"].ToString());
            }
            return midragList;
        }
    }
}
