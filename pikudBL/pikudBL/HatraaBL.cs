﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class HatraaBL
    {
        public static DataTable GetHatraaData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetHatraa", arrInParamsNames, arrInParamsValues);
        }
    }
}
