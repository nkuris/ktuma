﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class DistrictFillBL: BaseBL
    {
        public static DataTable GetDistrictFillData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getDistrictFill", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable GetDistrictFillDataHistory(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getDistrictFillHistory", arrInParamsNames, arrInParamsValues);
        }
    }
}
