﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class GeneralBL : BaseBL
    {
        public static DataTable ChangeCurrentSvivaId(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.ChangeCurrentSvivaId", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable UndoCheckout(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.undoCheckout", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable UndoCheckoutByRashutID(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.undoCheckoutByRashutId", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable Checkout(string pageName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.checkout" + pageName, arrInParamsNames, arrInParamsValues);
        }

        public static DataTable SaveData(string pageName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.save" + pageName, arrInParamsNames, arrInParamsValues);
        }

        public static DataTable LoadAdminSviva()
        {
            return clsUtil.FillDtNoParams("dbo.loadAdminSviva");
        }

        public static bool ResetTables()
        {
            bool res = clsUtil.ExecuteSQLNoParams("dbo.resetTables");
            return res;            
        }

        public static DataSet GetMdTables()
        {
            return clsUtil.getMdTables();
        }

        public static string[] GetDelimiter()
        {
            return clsUtil.arrDelimiter2;
        }
    }
}
