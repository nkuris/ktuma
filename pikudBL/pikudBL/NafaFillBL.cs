﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class NafaFillBL : BaseBL
    {
        public static DataTable GetNafaFillData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getNafaFill", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable GetNafaFillHistory(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetNafaFillHistory", arrInParamsNames, arrInParamsValues);
        }

    }
}
