﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class ShigraBL
    {
        public static DataTable GetshigraData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getShigra", arrInParamsNames, arrInParamsValues);
        }
    }
}
