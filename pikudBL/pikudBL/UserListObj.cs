﻿using pikudEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class UserListObj
    {
        List<KtumaUser> _list;

        public List<KtumaUser> List
        {
            get { return _list; }
            set { _list = value; }
        }

        string _action;

        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }

        string _actionResult;

        public string ActionResult
        {
            get { return _actionResult; }
            set { _actionResult = value; }
        }

        public UserListObj() { }

        public UserListObj(List<KtumaUser> list, string action, string actionResult)
        {
            _list = list;
            _action = action;
            _actionResult = actionResult;
        }

        public UserListObj(DataTable data, string action)
        {
            _action = action;
            if (data != null && data.Rows.Count > 0)
            {
                UserBL ubl = new UserBL();
                _list = ubl.CreateKtumaUserList(data);
                _actionResult = data.Rows[0]["actionResult"].ToString();
            }
            else
            {

            }

        }

    }
}
