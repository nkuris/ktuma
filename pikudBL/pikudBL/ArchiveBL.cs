﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class ArchiveBL : BaseBL
    {
        public static ArchiveTableResult ArchiveTables(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            ArchiveTableResult res = null;
            DataTable dt = clsUtil.FillDt("dbo.ArchiveTables", arrInParamsNames, arrInParamsValues);
            int? svivaId = null;
            int? archiveTableId = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                ArchiveTableActionResult atar = ArchiveTableResult.getActionResultFromString(dt.Rows[0]["actionStatus"].ToString());
                if (atar == ArchiveTableActionResult.success)
                {
                    svivaId = int.Parse(dt.Rows[0]["svivaid"].ToString());
                    archiveTableId = int.Parse(dt.Rows[0]["archiveTableId"].ToString());
                }
                res = new ArchiveTableResult(atar, svivaId, archiveTableId);
            }
            return res;
        }

        public static string ArchiveTablesNafot(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.ExecuteSQLScalar("dbo.ArchiveTablesNafot", arrInParamsNames, arrInParamsValues);
        }

        public static string ArchiveTablesDistricts(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.ExecuteSQLScalar("dbo.ArchiveTablesDistricts", arrInParamsNames, arrInParamsValues);
        }


        public static string ArchiveTablesExceptions(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.ExecuteSQLScalar("dbo.ArchiveTablesExceptions", arrInParamsNames, arrInParamsValues);
        }
    }

    public enum ArchiveTableActionResult { alreadyExists = 0, hasMissingMidragFinalValues=1, hasMissingFinalGradeValues = 2, success=3 }

    public class ArchiveTableResult
    {

        
        public ArchiveTableResult() { }

        public ArchiveTableResult(string actionString, int? svivaId, int? archiveTableId) :
            this(action: getActionResultFromString(actionString), svivaId: svivaId, archiveTableId: archiveTableId)
        {

        }

        public string CreateTaskOrderData(string stat)
        {
            string res = stat + " ";
            res += this.ArchiveTableId;
            return res;
        }

        public static ArchiveTableActionResult getActionResultFromString(string actionString)
        {
            ArchiveTableActionResult res;
            switch (actionString)
            {
                case "alreadyExists":
                    res = ArchiveTableActionResult.alreadyExists;
                    break;
                case "hasMissingMidragFinalValues":
                    res = ArchiveTableActionResult.hasMissingMidragFinalValues;
                    break;
                case "hasMissingFinalGradeValues":
                    res = ArchiveTableActionResult.hasMissingFinalGradeValues;
                    break;
                default:
                    //success
                    res = ArchiveTableActionResult.success;
                    break;
            }
            return res;
        }

        public ArchiveTableResult(ArchiveTableActionResult action, int? svivaId, int? archiveTableId)
        {
            m_actionResult = action;
            m_CurrentSvivaId = svivaId;
            m_ArchiveTableId = archiveTableId;
        }
        private ArchiveTableActionResult m_actionResult;
        private int? m_CurrentSvivaId;
        private int? m_ArchiveTableId;
        
        public int? ArchiveTableId
        {
            get { return m_ArchiveTableId; }
            set { m_ArchiveTableId = value; }
        }

        public ArchiveTableActionResult ActionResult
        {
            get
            {
                return m_actionResult;
            }
            set { m_actionResult = value; }
        }

        public int? CurrentSvivaId
        {
            get { return m_CurrentSvivaId; }
            set { m_CurrentSvivaId = value; }
        }

        

    }
}
