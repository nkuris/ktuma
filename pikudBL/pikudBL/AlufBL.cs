﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class AlufBL : BaseBL
    {
        public static DataTable GetAlufData(string[] arrInParamsNames,string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.Get_Aluf", arrInParamsNames, arrInParamsValues);
        }
    }
}
