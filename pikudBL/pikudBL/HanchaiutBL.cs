﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class HanchaiutBL : BaseBL
    {
        public static DataTable GetHanchaiutData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.GetHanchaiut", arrInParamsNames, arrInParamsValues);
        }
    }
}
