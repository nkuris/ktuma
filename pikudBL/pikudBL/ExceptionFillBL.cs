﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PikudBL
{
    public class ExceptionFillBL : BaseBL
    {

        public static DataTable GetExceptionFillData(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getExceptionFill", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable GetExceptionFillDataHistory(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.getExceptionFillHistory", arrInParamsNames, arrInParamsValues);
        }

        public static DataTable deleteException(string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsUtil.FillDt("dbo.deleteException", arrInParamsNames, arrInParamsValues);
        }
    }
}
