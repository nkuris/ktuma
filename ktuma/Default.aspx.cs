﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PikudBL;

namespace pikudGui
{
    public partial class _Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["userID"] = "-1";
            Session["UserName"] = "guest";
            Session["UserType"] = "-1";
            Session["User"] = null;
            Session["stat"] = null;
            Session["UserGroupId"] = null;
            Session["ktumaPikudBoolean"] = null;
            string logout = Request["Logout"];
            Response.Redirect(Helper.RedirectToHOmePageByReffere(Request, logout));
        }

    }
}