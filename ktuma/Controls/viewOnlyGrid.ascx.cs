﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PikudDB;

namespace pikudGui
{
    public partial class controls_viewOnlyGrid : controlsBasePage
    {
        DataTable dtGrid = null;
        public string strField1
        {
            get
            {
                if (dtGrid.Rows.Count > 0 && dtGrid.Rows[0]["TaskOrder_fileResult"] != null)
                {
                    return dtGrid.Rows[0]["TaskOrder_fileResult"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }


        public int intRowsCount
        {
            get
            {
                return dtGrid.Rows.Count;
            }
        }

        public Boolean blnPnlMainVisible
        {
            get { return pnlMain.Visible; }
            set { pnlMain.Visible = value; }
        }

        public int intTotaPages
        {
            get { return Convert.ToInt16(ViewState["intTotaPages"]); }
            set { ViewState["intTotaPages"] = value; }
        }

        public int PageNum
        {
            get { return Convert.ToInt16(ViewState["PageNum"]); }
            set { ViewState["PageNum"] = value; }
        }

        public string RedirectUrlAndParams
        {
            get { return ViewState["RedirectUrl"].ToString(); }
            set { ViewState["RedirectUrl"] = value; }
        }

        public string RedirectParamsCellNums
        {
            get { return ViewState["RedirectParamsCellNums"].ToString(); }
            set { ViewState["RedirectParamsCellNums"] = value; }
        }

        public int hiddenColumnsCount
        {
            get { return Convert.ToInt32(ViewState["hiddenColumns"]); }
            set { ViewState["hiddenColumns"] = value; }
        }

        public int columnsTableNum
        {
            get { return Convert.ToInt32(ViewState["columnsTableNum"]); }
            set { ViewState["columnsTableNum"] = value; }
        }

        public string strInParamsNames
        {
            get { return ViewState["strInParamsNames"].ToString(); }
            set { ViewState["strInParamsNames"] = value; }
        }
        public string strInParamsValues
        {
            get { return ViewState["strInParamsValues"].ToString(); }
            set { ViewState["strInParamsValues"] = value; }
        }
        public string strSpName
        {
            get { return ViewState["strSpName"].ToString(); }
            set { ViewState["strSpName"] = value; }
        }

        public Boolean noCommandField
        {
            get { return Convert.ToBoolean(ViewState["noCommandField"]); }
            set { ViewState["noCommandField"] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (PageNum == 0)
            {
                PageNum = 1;
            }
           
        }

        private void IMG_Download_Click(object sender, ImageClickEventArgs e)
        {
            string strPath = ConfigurationManager.AppSettings["UserManualPath"].ToString();
            System.IO.FileInfo file = new System.IO.FileInfo(strPath);

            if (file.Exists == true)
            {
                Response.Clear();
                Response.AppendHeader("Content-Disposition", "inline;filename=" + file.Name);

                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/doc";
                Response.WriteFile(file.FullName);
                Response.End();
                Response.Flush();

            }
        }

        private void buildColumns(DataColumnCollection columns)
        {
            DataTable dtColsHeaders = clsDbGlobal.getParamsByTableNum(0, columnsTableNum);
            if (noCommandField == false)
            {
                CommandField cf = new CommandField();
                cf.HeaderText = "לחץ לפרטים";
                cf.SelectText = "בחר";
                cf.ShowSelectButton = true;
                grid.Columns.Add(cf);
            }
            //for (int idx = 0; idx < columns.Count - hiddenColumnsCount; idx++)
            for (int idx = 0; idx < columns.Count; idx++)
            {
                BoundField bf = new BoundField();
                bf.DataField = columns[idx].ToString();
                bf.HeaderText = dtColsHeaders.Rows[idx]["param_value"].ToString();
                grid.Columns.Add(bf);
            }
        }

        public void bind()
        {
            string[] arrInParamsNames = (strInParamsNames + ",pageNumber").Split(',');
            string[] arrInParamsValues = (strInParamsValues + "," + PageNum.ToString()).Split(',');
            string strOutParamsNames = "Rows,Pages";
            string strOutParamsdbType = "int,int";
            string strOutParamsdbSize = "4,4";
            string strOutParamsValues = "0,0";
            string[] arrOutParamsNames = strOutParamsNames.Split(',');
            string[] arrOutParamsdbType = strOutParamsdbType.Split(',');
            string[] arrOutParamsdbSize = strOutParamsdbSize.Split(',');
            string[] arrOutParamsValues = strOutParamsValues.Split(',');

            dtGrid = clsDbGlobal.GetDtFromSP(strSpName, arrInParamsNames, arrInParamsValues,
                        arrOutParamsNames, arrOutParamsdbType, arrOutParamsdbSize, arrOutParamsValues);
            if (dtGrid != null && dtGrid.Rows.Count > 0)
            {
                grid.Columns.Clear();
                buildColumns(dtGrid.Columns);
                grid.DataSource = dtGrid;
                grid.DataBind();
                intTotaPages = Convert.ToInt32(arrOutParamsValues[1]);
                Navigation(Convert.ToInt32(arrOutParamsValues[0]));

                //hide after bind, so they are exist in grid
                //adding - 1 because the first column is a button
                if (noCommandField == false)
                {
                    for (int idx = (dtGrid.Columns.Count + 1) - hiddenColumnsCount; idx <= dtGrid.Columns.Count; idx++)
                    {
                        //BoundField bf = new BoundField();
                        //bf.DataField = columns[idx].ToString();
                        grid.Columns[idx].Visible = false;
                    }
                }
                else
                {
                    for (int idx = (dtGrid.Columns.Count) - hiddenColumnsCount; idx < dtGrid.Columns.Count; idx++)
                    {
                        //BoundField bf = new BoundField();
                        //bf.DataField = columns[idx].ToString();
                        grid.Columns[idx].Visible = false;
                    }
                }
            }
            else
            {
                pnlMain.Visible = false;
            }

        }

        public int getGridColumnIdByName(string strName)
        {
            return 0;
        }

        protected void grid_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Session["paramNames"] = null;
            Session["paramValues"] = null;

            string[] arrRedirectParams = RedirectUrlAndParams.Split(arrDelimiter, StringSplitOptions.None);
            string[] arrRedirectCellNums = RedirectParamsCellNums.Split(arrDelimiter, StringSplitOptions.None);

            for (int idx = 1; idx < arrRedirectParams.Length; idx++)
            {
                int cellNum = Convert.ToInt32(arrRedirectCellNums[idx - 1]);
                Session["paramNames"] += arrRedirectParams[idx];
                Session["paramValues"] += grid.Rows[e.NewSelectedIndex].Cells[cellNum].Text;
                if (idx < arrRedirectParams.Length - 1)
                {
                    Session["paramNames"] += arrDelimiter[0];
                    Session["paramValues"] += arrDelimiter[0];
                }
            }
            Response.Redirect(arrRedirectParams[0]);
        }


        private void Navigation(int totalRecords)
        {
            GoToPageTxt.Text = PageNum.ToString();
            CurrentPage.Text = PageNum.ToString();
            TotalPages.Text = intTotaPages.ToString();
        }


        protected void NavigationLink_Click(Object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "First":
                    PageNum = 1;
                    break;
                case "Last":
                    PageNum = Convert.ToInt16(TotalPages.Text);
                    break;
                case "Next":
                    if (PageNum < intTotaPages)
                    {
                        PageNum = Convert.ToInt16(CurrentPage.Text) + 1;
                    }
                    break;
                case "Prev":
                    if (PageNum > 1)
                    {
                        PageNum = Convert.ToInt16(CurrentPage.Text) - 1;
                    }
                    break;
            }
            bind();
        }



        protected void GoToPageImb_Click(object sender, ImageClickEventArgs e)
        {
            if (GoToPageTxt.Text != "")
            {
                int maxPage = Convert.ToInt32(TotalPages.Text);
                int goToPage = -1;
                if (int.TryParse(GoToPageTxt.Text, out goToPage))
                {
                    // Your conditions
                    if (goToPage > 0)
                    {
                        PageNum = goToPage;
                        bind();
                    }

                }
            }
        }


        //protected void grid_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    LblMessage.Text = "row selected=" + grid.Rows[grid.SelectedIndex].Cells[1].Text;
        //}
        //protected void grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{

        //    LblMessage.Text = "row deleting=" + grid.Rows[e.RowIndex].Cells[1].Text;
        //}
        //protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    LblMessage.Text = "row editing=" + grid.Rows[e.NewEditIndex].Cells[1].Text;
        //}
        //protected void grid_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    LblMessage.Text = "columns sorting, - SortExpression=" + e.SortExpression; //dir=" + e.SortDirection +
        //}
    }
}