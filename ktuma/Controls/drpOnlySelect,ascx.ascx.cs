﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using PikudBL;
using System.Drawing;


namespace pikudGui
{
    public partial class Controls_drpOnlySelect : controlsBasePage
    {
        //string mSelectParamsNames = "";
        //string[] mArrSelectParamsNames = null;
        //string mSelectParamsValues = "";
        //string[] mArrSelectParamsValues = null;
        //string mSelectParamsTypes = "";
        //string [] mArrSelectParamsTypes = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl.Width = widthWide - 30;
            if (!IsPostBack)
            {
                if (fillOnLoad)
                {
                    fill();
                }

                drp.Attributes.Add("onkeypress", "return KeySortDropDownList_onkeypress(this)");
            }
        }

        public void fill()
        {
            if (inParamsNames != null)
            {
                string[] arrInParamsNames = inParamsNames.Split(',');
                string[] arrInParamsValues = inParamsValues.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
                FillCombo(SelectCommand, arrInParamsNames, arrInParamsValues, drp
                                , drpDataValueField, drpDataTextField);
            }
            else
            {
                FillComboNoParams(SelectCommand, drp
                                , drpDataValueField, drpDataTextField);
            }

        }

        public void FillCombo
                    (string spName, string[] arrInParamsNames, string[] arrInParamsValues,
                    DropDownList cboToFill, string strItemField, string strTextField)
        {
            DataTable dt = clsUtil.FillDt(spName, arrInParamsNames, arrInParamsValues);
            cboToFill.Items.Clear();

            if (dt == null)
            {
                return;
            }
            if (!blnRemoveEmptyByCtl)
            {
                cboToFill.Items.Add(new ListItem("", ""));//add empty
            }
            foreach (DataRow dr in dt.Rows)
                cboToFill.Items.Add(new ListItem(dr[strTextField].ToString(), dr[strItemField].ToString()));
            dt = null;
        }


        public void FillComboNoParams
                    (string spName,
                    DropDownList cboToFill, string strItemField, string strTextField)
        {
            DataTable dt = clsUtil.FillDtNoParams(spName);
            if (dt == null)
            {
                return;
            }
            cboToFill.Items.Clear();
            if (!blnRemoveEmptyByCtl)
            {
                cboToFill.Items.Add(new ListItem("", ""));//add empty
            }

            foreach (DataRow dr in dt.Rows)
                cboToFill.Items.Add(new ListItem(dr[strTextField].ToString(), dr[strItemField].ToString()));
            dt = null;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the date picker control is enabled.
        ///// </summary>
        //[Category("Behavior")]
        //[Bindable(true, BindingDirection.TwoWay)]
        //[Browsable(true)]
        //[DefaultValue(true)]
        //public bool Enabled
        //{
        //    get { return enabled; }
        //    set { enabled = value; }
        //}


        [Category("Appearance")]
        [Description("enter the textBox type internal-data loaded from excel file, external-added to data")]
        [Browsable(true)]
        public Boolean txtIsExternal
        {
            //get { return dateFormat.Replace("\\/", "/"); }
            get { return Convert.ToBoolean(ViewState["txtIsExternal"]); }
            set
            {
                ViewState["txtIsExternal"] = value;
                if (txtIsExternal)
                    drp.BackColor = Color.FromArgb(240, 250, 139);
                else
                    drp.BackColor = Color.White;
            }
        }



        /// <summary>
        /// Gets or sets the date format to use, e.g. "dd.MM.yyyy" or "MM/dd/yyyy".
        /// </summary>
        [Category("Appearance")]
        [Description("enter the label caption")]
        [Browsable(true)]
        public string LabelCaption
        {
            set { lbl.Text = value; }
        }

        [Category("Appearance")]
        [Description("enter the label width")]
        [Browsable(true)]
        public Unit LabelWidth
        {
            set { lbl.Width = value; }
        }


        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public string drpDataTextField
        {
            get { return ViewState["drpDataTextField"].ToString(); }
            set { ViewState["drpDataTextField"] = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public string drpDataValueField
        {
            get { return ViewState["drpDataValueField"].ToString(); }
            set { ViewState["drpDataValueField"] = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public string SelectCommand
        {
            get { return ViewState["SelectCommand"].ToString(); }
            set { ViewState["SelectCommand"] = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public string inParamsNames
        {
            get
            {
                if (ViewState["inParamsNames"] == null)
                    return null;
                else
                    return ViewState["inParamsNames"].ToString();
            }
            set { ViewState["inParamsNames"] = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public string inParamsValues
        {
            get
            {
                if (ViewState["inParamsValues"] == null)
                    return null;
                else
                    return ViewState["inParamsValues"].ToString();
            }
            set { ViewState["inParamsValues"] = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public DropDownList dropDown
        {
            get { return drp; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DefaultValue(true)]
        public Boolean fillOnLoad
        {
            get { return Convert.ToBoolean(ViewState["fillOnLoad"]); }
            set { ViewState["fillOnLoad"] = value; }
        }


        [Category("Behavior")]
        [Browsable(true)]
        public Boolean blnRemoveEmptyByCtl
        {
            get
            {
                if (ViewState["blnRemoveEmptyByCtl"] == null)
                    return false;
                else
                    return Convert.ToBoolean(ViewState["blnRemoveEmptyByCtl"]);
            }
            set { ViewState["blnRemoveEmptyByCtl"] = value; }
        }

    }
}