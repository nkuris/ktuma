﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/Controls/drpOnlySelect,ascx.ascx.cs" Inherits="pikudGui.Controls_drpOnlySelect" %>

   <script language="javascript" type="text/javascript">
        function KeySortDropDownList_onkeypress(dropdownlist)   
         {
            // check the keypressBuffer attribute is defined on the dropdownlist
            var undefined;
            if (dropdownlist.keypressBuffer == undefined) {
                dropdownlist.keypressBuffer = '';
            }
            // get the key that was pressed 
            var key = String.fromCharCode(window.event.keyCode);
            dropdownlist.keypressBuffer += key;
            // find if it is the start of any of the options 
            var optionsLength = dropdownlist.options.length;
            for (var n = 0; n < optionsLength; n++) {
                var optionText = dropdownlist.options[n].text;
                if (optionText.indexOf(dropdownlist.keypressBuffer, 0) == 0) {
                    dropdownlist.selectedIndex = n;
                    return false; // cancel the default behavior since 
                    // we have selected our own value 
                }
            }
            // reset initial key to be inline with default behavior 
            dropdownlist.keypressBuffer = key;
            return true; // give default behavior
        }
    </script>

<%--<div dir="rtl">--%>
<table cellpadding="0" cellspacing="0"-><tr><td align="right">
    <asp:Label ID="lbl" runat="server" Text=""></asp:Label>
</td><td>
    <asp:DropDownList ID="drp" runat="server">
    </asp:DropDownList>
</td></tr></table>
<%--</div>--%>
