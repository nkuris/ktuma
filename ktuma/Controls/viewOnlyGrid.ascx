﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="viewOnlyGrid.ascx.cs" Inherits="pikudGui.controls_viewOnlyGrid" %>
<asp:Panel runat="server" id="pnlMain" Direction="RightToLeft" Visible="false">
<table class="tableRegularRight">
<tr><td>
	<asp:imagebutton id="NextPage" Runat="server" ImageAlign="AbsBottom" ImageUrl="Images/next.gif" ToolTip="הבא"
		CommandName="Next" OnCommand="NavigationLink_Click"></asp:imagebutton>
	<asp:imagebutton id="LastPage" Runat="server" ImageAlign="AbsBottom" ImageUrl="Images/last.gif" ToolTip="Last"
		CommandName="לעמוד האחרון" OnCommand="NavigationLink_Click"></asp:imagebutton>&nbsp;&nbsp;
	<asp:imagebutton id="GoToPageImb" Runat="server" ImageAlign="AbsBottom" ImageUrl="Images\GoToPage.gif"
		ToolTip="עבור לעמוד מספר" OnClick="GoToPageImb_Click"></asp:imagebutton>&nbsp;
	<asp:textbox id="GoToPageTxt" runat="server" Width="30px" Text="1" Font-Bold="True"></asp:textbox>&nbsp;
	<asp:imagebutton id="FirstPage" Runat="server" ImageAlign="AbsBottom" ImageUrl="Images/first.gif"
		ToolTip="לעמוד הראשון" CommandName="First" OnCommand="NavigationLink_Click"></asp:imagebutton>
	<asp:imagebutton id="PreviousPage" Runat="server" ImageAlign="AbsBottom" ImageUrl="Images/previous.gif"
		ToolTip="הקודם" CommandName="Prev" OnCommand="NavigationLink_Click"></asp:imagebutton>&nbsp;&nbsp;

    <asp:Label ID="CurrentPage" runat="server" Text="1" Font-Bold="True"></asp:Label>
	<asp:label id="SepLbl" runat="server" Font-Size="Large" CssClass="pageLinks" Font-Bold="true">/</asp:label>
    &nbsp;<asp:Label ID="TotalPages" runat="server" Font-Bold="True"></asp:Label>&nbsp;    
</td></tr>
<tr><td>
    <asp:GridView ID="grid" runat="server"
        AutoGenerateColumns="False" BackColor="White" 
        BorderColor="#999999" BorderStyle="None"
        BorderWidth="1px" CellPadding="3" GridLines="Vertical"
         OnSelectedIndexChanging="grid_SelectedIndexChanging">
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <PagerStyle ForeColor="Black" HorizontalAlign="Center" BackColor="#999999" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#FF9333" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="Gainsboro" />
    </asp:GridView>
</td></tr>
</table>
</asp:Panel>