﻿<%@ WebHandler Language="C#" Class="pikudGui.RegionHandler" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using PikudBL;
using PikudDB;
using pikudEntities;
using System.Data;
using Newtonsoft.Json;
using System.Linq;

namespace pikudGui
{
    public class RegionHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        string[] delimiter;
        public void ProcessRequest(HttpContext context)
        {
            delimiter = GeneralBL.GetDelimiter();
            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            string strOperation = forms.Get("op");
            string strResponse = string.Empty;
            string svivaId = context.Session["SvivaId"] != null ? context.Session["SvivaId"].ToString() : "null";

            if (strOperation == "loadGila")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(GilaBL.GetGilaData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadNafaFill")
            {
                string userName = forms.Get("userName");
                string userId = forms.Get("userId");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId" + delimiter[0] + "userId";
                string strInParamsValues = userName + delimiter[0] + svivaId + delimiter[0] + userId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(NafaFillBL.GetNafaFillData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadExceptionFill")
            {
                string userName = forms.Get("userName");
                string userId = forms.Get("userId");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId" + delimiter[0] + "userId";
                string strInParamsValues = userName + delimiter[0] + svivaId + delimiter[0] + userId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(ExceptionFillBL.GetExceptionFillData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadDistrictFill")
            {
                string userName = forms.Get("userName");
                string userId = forms.Get("userId");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId" + delimiter[0] + "userId";
                string strInParamsValues = userName + delimiter[0] + svivaId + delimiter[0] + userId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(DistrictFillBL.GetDistrictFillData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadDistrictFillHistory")
            {

                string fromDateString, toDateString;
                DateTime? fromDate, toDate;
                if (forms.Get("searchFromDate") != null)
                {
                    fromDate = DateTime.ParseExact(forms.Get("searchFromDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    fromDateString = fromDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    fromDateString = "null";
                    fromDate = null;
                }
                if (forms.Get("searchToDate") != null)
                {
                    toDate = DateTime.ParseExact(forms.Get("searchToDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    toDateString = toDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    toDateString = "null";
                    toDate = null;
                }
                string initialSearch = forms.Get("initialSearch");
                string itgonenut_regions = string.IsNullOrEmpty(forms.Get("searchItgonenutRegions")) ? "null" : forms.Get("searchItgonenutRegions");
                string districts = string.IsNullOrEmpty(forms.Get("searchDistricts")) ? "null" : forms.Get("searchDistricts");
                string strInParamsNames = "fromDate" + delimiter[0] + "toDate" + delimiter[0] + "svivaId" + delimiter[0] +
                        "districts" + delimiter[0] + "itgonenut_regions" + delimiter[0] + "initialSearch";
                string strInParamsValues = fromDateString + delimiter[0] + toDateString + delimiter[0] + svivaId + delimiter[0]
                        + districts + delimiter[0] + itgonenut_regions + delimiter[0] + initialSearch;

                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(DistrictFillBL.GetDistrictFillDataHistory(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadExceptionFillHistory")
            {

                string fromDateString, toDateString;
                DateTime? fromDate, toDate;
                if (forms.Get("searchFromDate") != null)
                {
                    fromDate = DateTime.ParseExact(forms.Get("searchFromDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    fromDateString = fromDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    fromDateString = "null";
                    fromDate = null;
                }
                if (forms.Get("searchToDate") != null)
                {
                    toDate = DateTime.ParseExact(forms.Get("searchToDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    toDateString = toDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    toDateString = "null";
                    toDate = null;
                }
                string nafot = string.IsNullOrEmpty(forms.Get("searchNafot")) ? "null" : forms.Get("searchNafot");
                string initialSearch = forms.Get("initialSearch");
                string rashuyot = string.IsNullOrEmpty(forms.Get("searchRashuyot")) ? "null" : forms.Get("searchRashuyot");
                string districts = string.IsNullOrEmpty(forms.Get("searchDistricts")) ? "null" : forms.Get("searchDistricts");
                string strInParamsNames = "fromDate" + delimiter[0] + "toDate" + delimiter[0] + "svivaId" + delimiter[0] +
                        "districts" + delimiter[0] + "rashuyot" + delimiter[0] + "initialSearch" + delimiter[0] + "nafot";
                string strInParamsValues = fromDateString + delimiter[0] + toDateString + delimiter[0] + svivaId + delimiter[0]
                        + districts + delimiter[0] + rashuyot + delimiter[0] + initialSearch + delimiter[0] + nafot;

                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(ExceptionFillBL.GetExceptionFillDataHistory(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadNafotFillHistory")
            {

                string fromDateString, toDateString;
                DateTime? fromDate, toDate;
                if (forms.Get("searchFromDate") != null)
                {
                    fromDate = DateTime.ParseExact(forms.Get("searchFromDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    fromDateString = fromDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    fromDateString = "null";
                    fromDate = null;
                }
                if (forms.Get("searchToDate") != null)
                {
                    toDate = DateTime.ParseExact(forms.Get("searchToDate"), "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    toDateString = toDate.Value.ToString("M/d/yyyy HH:mm");
                }
                else
                {
                    toDateString = "null";
                    toDate = null;
                }
                string initialSearch = forms.Get("initialSearch");
                string itgonenut_regions = string.IsNullOrEmpty(forms.Get("searchItgonenutRegions")) ? "null" : forms.Get("searchItgonenutRegions");
                string districts = string.IsNullOrEmpty(forms.Get("searchDistricts")) ? "null" : forms.Get("searchDistricts");
                string nafot = string.IsNullOrEmpty(forms.Get("searchNafot")) ? "null" : forms.Get("searchNafot");
                string strInParamsNames = "fromDate" + delimiter[0] + "toDate" + delimiter[0] + "svivaId" + delimiter[0] +
                        "districts" + delimiter[0] + "itgonenut_regions" + delimiter[0] + "initialSearch" + delimiter[0] + "nafot";
                string strInParamsValues = fromDateString + delimiter[0] + toDateString + delimiter[0] + svivaId + delimiter[0]
                        + districts + delimiter[0] + itgonenut_regions + delimiter[0] + initialSearch + delimiter[0] + nafot;

                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(NafaFillBL.GetNafaFillHistory(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadPopulation")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(PopulationBL.GetPopulationData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadItgonenutRegions")
            {
                DataTable nafot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[1];
                context.Response.Write(toJson(nafot));
            }
            else if (strOperation == "loadRashuyot")
            {
                DataTable rashuyot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[11];
                context.Response.Write(toJson(rashuyot));
            }
            else if (strOperation == "loadDistricts")
            {
                DataTable nafot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[10];
                context.Response.Write(toJson(nafot));
            }
            else if (strOperation == "loadNafot")
            {
                DataTable nafot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[9];
                context.Response.Write(toJson(nafot));
            }
            else if (strOperation == "loadCombos")
            {
                context.Response.Write(toJsonDS(((DataSet)HttpContext.Current.Application["dsMdTables"])));
            }
            else if (strOperation == "loadCombosByPage")
            {
                string page = forms.Get("page");
                DataTable curCombos = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[2];
                DataTable selectedTable = curCombos.AsEnumerable()
                            .Where(r => r.Field<string>("ComboScreen") == page).OrderBy(r => r.Field<int>("ComboInternalID"))
                            .CopyToDataTable();
                context.Response.Write(toJson(selectedTable));
            }
            else if (strOperation == "loadScreens")
            {
                DataSet res = new DataSet();
                DataSet dsMdTables = ((DataSet)HttpContext.Current.Application["dsMdTables"]);
                res.Tables.Add(dsMdTables.Tables[6].Copy()); //all screens              
                context.Response.Write(toJsonDS(res));
            }
            else if (strOperation == "loadResources")
            {
                DataSet res = new DataSet();
                DataSet dsMdTables = ((DataSet)HttpContext.Current.Application["dsMdTables"]);
                DataTable resourcesTable = dsMdTables.Tables[2].Copy();
                DataView dv = new DataView(resourcesTable, null, "ComboId asc, ComboInternalId asc", DataViewRowState.CurrentRows);
                res.Tables.Add(dv.ToTable());//all resources 
                context.Response.Write(toJsonDS(res));
            }
            else if (strOperation == "loadPageData")
            {
                DataSet res = new DataSet();
                DataSet dsMdTables = ((DataSet)HttpContext.Current.Application["dsMdTables"]);
                res.Tables.Add(dsMdTables.Tables[6].Copy()); //all screens
                res.Tables.Add(dsMdTables.Tables[5].Copy()); //all combos
                DataTable resourcesTable = dsMdTables.Tables[2].Copy();
                DataView dv = new DataView(resourcesTable, null, "ComboId asc, ComboInternalId asc", DataViewRowState.CurrentRows);
                res.Tables.Add(dv.ToTable());//all resources
                context.Response.Write(toJsonDS(res));
            }
            else if (strOperation == "loadNafotMehozot")
            {
                DataTable curParams = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[8];
                context.Response.Write(toJson(curParams));
            }
            else if (strOperation == "Login")
            {
                UserBL ubl = new UserBL();
                string ktumaPikudBoolean = forms.Get("ktumaPikudBoolean");
                string uName = forms.Get("userName");
                string pass = forms.Get("password");
                string strInParamsNames = "username" + delimiter[0] + "password";
                string strInParamsValues = uName + delimiter[0] + pass;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                KtumaUser ktUser = ubl.Login(arrInParamsNames, arrInParamsValues);
                if (ktUser != null)
                {
                    if (ktUser.IsActive)
                    {
                        context.Session["User"] = ktUser;
                        context.Session["UserType"] = ktUser.KtumaUserType.UserTypeId.ToString();
                        context.Session["UserGroupId"] = ktUser.GroupId.ToString();
                        context.Session["UserName"] = ktUser.UserName;
                        context.Session["userID"] = ktUser.UserId.ToString();
                        context.Session["SvivaId"] = ktUser.Sviva.SvivaId.ToString();
                        context.Session["SvivaName"] = ktUser.Sviva.SvivaName;
                        context.Session["stat"] = "MakeWord";
                        context.Session["ktumaPikudBoolean"] = ktumaPikudBoolean;
                    }
                    else
                    {

                        context.Response.Write(toJson(ktUser));
                        context.Session["SvivaId"] = null;
                        context.Session["SvivaName"] = null;
                        return;
                    }
                }
                else
                {
                    context.Session["SvivaId"] = null;
                    context.Session["SvivaName"] = null;
                }
                context.Response.Write(toJson(ktUser != null));
            }
            else if (strOperation == "changeSviva")
            {
                string svivaIdToChange = forms.Get("svivaId");
                string strInParamsNames = "svivaId";
                string strInParamsValues = svivaIdToChange;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                DataTable res = GeneralBL.ChangeCurrentSvivaId(arrInParamsNames, arrInParamsValues);
                DataTable dataTable = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[3];
                DataRow[] rowsToUpdate = dataTable.Select("prm_name = 'CurrentSvivaId'");
                rowsToUpdate[0]["prm_Value"] = svivaIdToChange;

                Sviva sv = Helper.GetSvivaFromApplication();
                context.Session["SvivaId"] = sv.SvivaId.ToString();
                context.Session["SvivaName"] = sv.SvivaName.ToString();
                context.Response.Write(toJson(sv));

            }
            else if (strOperation == "saveDataToHistory")
            {
                string section = forms.Get("section");
                string saveDate = forms.Get("saveDate");
                string strInParamsNames = "svivaId" + delimiter[0] + "saveDate" + delimiter[0] + "userId";
                string strInParamsValues = svivaId + delimiter[0] + saveDate + delimiter[0] + context.Session["userID"].ToString();
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string res = string.Empty;
                switch (section)
                {
                    case "Nafa":
                        res = ArchiveBL.ArchiveTablesNafot(arrInParamsNames, arrInParamsValues);
                        break;
                    case "District":
                        res = ArchiveBL.ArchiveTablesDistricts(arrInParamsNames, arrInParamsValues);
                        break;
                    default: //hahragot
                        res = ArchiveBL.ArchiveTablesExceptions(arrInParamsNames, arrInParamsValues);
                        break;

                }
                context.Response.Write(toJson(res));
            }
            else if (strOperation == "Logout")
            {
                context.Session["User"] = null;
                context.Session["UserType"] = null;
                context.Session["UserGroupId"] = null;
                context.Session["UserName"] = null;
                context.Session["userID"] = null;
                context.Session["Sviva"] = null;
                context.Session["SvivaId"] = null;
                context.Session["SvivaName"] = null;
                context.Session["stat"] = null;
                context.Response.Write(toJson(true));
            }
            else if (strOperation == "loadParams")
            {
                DataTable curParams = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[3];
                context.Response.Write(toJson(curParams));
            }
            else if (strOperation == "loadRanges")
            {
                string res = toJson(((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[0]);
                context.Response.Write(res);
            }
            else if (strOperation == "loadFinal_Grade")
            {
                context.Response.Write(toJson(FinalBL.GetFinalData(svivaId)));
            }
            else if (strOperation == "loadAluf")
            {
                string Itgonenut_Id = forms.Get("Itgonenut_Id");
                string userName = forms.Get("userName");
                string strInParamsNames = "Itgonenut_Id" + delimiter[0] + "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = Itgonenut_Id + delimiter[0] + userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(AlufBL.GetAlufData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadHanchaiut")
            {
                string Itgonenut_Id = forms.Get("Itgonenut_Id");
                string strInParamsNames = "Itgonenut_Id" + delimiter[0] + "svivaId";
                string strInParamsValues = Itgonenut_Id + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(HanchaiutBL.GetHanchaiutData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadHagna")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(HagnaBL.GetHagnaData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadHaatra")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(HatraaBL.GetHatraaData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadShigra")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(ShigraBL.GetshigraData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadMidrag")
            {
                MidragBL mbl = new MidragBL();

                List<MidragObject> res = mbl.getMidragList(svivaId);
                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(res);
                context.Response.Write(json);
            }
            else if (strOperation == "loadMigun")
            {
                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(MigunBL.GetMigunData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadMod")
            {

                string userName = forms.Get("userName");
                string strInParamsNames = "userName" + delimiter[0] + "svivaId";
                string strInParamsValues = userName + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                context.Response.Write(toJson(ModBL.GetModData(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "undoCheckout")
            {
                string itgonenut_Id = forms.Get("Itgonenut_Id");
                string rashutId = forms.Get("rashutid");
                string exceptionFillRowId = forms.Get("exceptionFillRowId");
                string tableName = forms.Get("page");
                string strInParamsNames;
                string strInParamsValues;
                string[] arrInParamsNames;
                string[] arrInParamsValues;
                if (tableName == "NafaFill")
                {
                    strInParamsNames = "rashutId" + delimiter[0] + "svivaid";
                    strInParamsValues = rashutId + delimiter[0] + svivaId;
                    arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    context.Response.Write(toJson(GeneralBL.UndoCheckoutByRashutID(arrInParamsNames, arrInParamsValues)));
                }
                else if (tableName == "ExceptionFill")
                {
                    strInParamsNames = "exceptionFillRowId" + delimiter[0] + "svivaid";
                    strInParamsValues = exceptionFillRowId + delimiter[0] + svivaId;
                    arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    context.Response.Write(toJson(GeneralBL.UndoCheckoutByRashutID(arrInParamsNames, arrInParamsValues)));
                }
                else
                {
                    if (tableName == "DistrictFill")
                    {
                        tableName = "districtEditing";
                    }
                    strInParamsNames = "Itgonenut_Id" + delimiter[0] + "tableName" + delimiter[0] + "svivaid";
                    strInParamsValues = itgonenut_Id + delimiter[0] + tableName + delimiter[0] + svivaId;
                    arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                    context.Response.Write(toJson(GeneralBL.UndoCheckout(arrInParamsNames, arrInParamsValues)));
                }





            }

            else if (strOperation == "checkout")
            {
                string pageName = forms.Get("page");
                string Itgonenut_id = forms.Get("Itgonenut_Id");
                string rashutId = forms.Get("rashutId");
                string exceptionFillRowId = forms.Get("ExceptionFillRowId");
                KtumaUser curUser = ((KtumaUser)context.Session["User"]);
                string user_name = curUser.UserName;
                string strInParamsNames;
                string strInParamsValues;
                if (pageName == "ExceptionFill")
                {
                    strInParamsNames = "ExceptionFillRowId" + delimiter[0] + "user_name" + delimiter[0] + "SvivaId";
                    strInParamsValues = exceptionFillRowId + delimiter[0] + user_name + delimiter[0] + svivaId;
                }
                else if (pageName == "NafaFill")
                {
                    strInParamsNames = "rashutId" + delimiter[0] + "user_name" + delimiter[0] + "SvivaId";
                    strInParamsValues = rashutId + delimiter[0] + user_name + delimiter[0] + svivaId;
                }
                else
                {
                    strInParamsNames = "Itgonenut_id" + delimiter[0] + "user_name" + delimiter[0] + "SvivaId";
                    strInParamsValues = Itgonenut_id + delimiter[0] + user_name + delimiter[0] + svivaId;
                }


                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                context.Response.Write(toJson(GeneralBL.Checkout(pageName, arrInParamsNames, arrInParamsValues)));

            }
            else if (strOperation == "save")
            {
                string pageName = forms.Get("page");
                string userId = forms.Get("userId");
                string explantion, comander_Affect, recommendedPolicy, explanationRecommendedPolicy, exceptionPolicy, adequacy;
                string rashutId, exceptionFillRowId, parameter, instituteName, explanationToException, securityTour, itgonenutRegionExceptTo;
                string Itgonenut_Id = forms.Get("ItgonenutId");
                string strInParamsNames = "userId" + delimiter[0] + "Itgonenut_Id" + delimiter[0] + "svivaId";
                string strInParamsValues = userId + delimiter[0] + Itgonenut_Id + delimiter[0] + svivaId;
                switch (pageName)
                {
                    case "Gila":
                        string amida = forms.Get("amida");
                        string gilui = forms.Get("gilui");
                        string hatraa = forms.Get("hatraa");
                        string harigim = forms.Get("harigim");
                        explantion = forms.Get("explantion");
                        comander_Affect = forms.Get("comander_Affect") != "0" ? forms.Get("comander_Affect") : null;
                        strInParamsNames += delimiter[0] + "Amida" + delimiter[0] + "Gilui" + delimiter[0] + "Hatraa" + delimiter[0] + "Harigim" +
                        delimiter[0] + "Explanation" + delimiter[0] + "Comander_Affect";
                        strInParamsValues += delimiter[0] + amida + delimiter[0] + gilui + delimiter[0] + hatraa + delimiter[0] + harigim
                            + delimiter[0] + explantion + delimiter[0] + comander_Affect;
                        break;
                    case "Migun":
                        string prati = forms.Get("prati");
                        string tziboori = forms.Get("tziboori");
                        string mosach = forms.Get("mosach");
                        strInParamsNames += delimiter[0] + "prati" + delimiter[0] + "tziboori" + delimiter[0] + "mosach";
                        strInParamsValues += delimiter[0] + prati + delimiter[0] + tziboori + delimiter[0] + mosach;
                        break;
                    case "Hagna":
                        string kium = forms.Get("kium");
                        string mediniut = forms.Get("mediniut");
                        string efectiviut = forms.Get("efectiviut");
                        string prisa = forms.Get("prisa");
                        explantion = forms.Get("explantion");
                        comander_Affect = forms.Get("comander_Affect") != "0" ? forms.Get("comander_Affect") : null;
                        strInParamsNames += delimiter[0] + "kium" + delimiter[0] + "mediniut" + delimiter[0] + "efectiviut" + delimiter[0] + "prisa" +
                        delimiter[0] + "Explanation" + delimiter[0] + "Comander_Affect";
                        strInParamsValues += delimiter[0] + kium + delimiter[0] + mediniut + delimiter[0] + efectiviut + delimiter[0] + prisa + delimiter[0]
                            + explantion + delimiter[0] + comander_Affect;
                        break;
                    case "Mod":
                        string haka = forms.Get("haka");
                        string dapa = forms.Get("dapa");
                        string teshen = forms.Get("teshen");
                        string region_factor = forms.Get("region_factor");
                        explantion = forms.Get("explantion");
                        harigim = forms.Get("harigim");
                        comander_Affect = forms.Get("comander_Affect") != "0" ? forms.Get("comander_Affect") : null;
                        strInParamsNames += delimiter[0] + "haka" + delimiter[0] + "dapa" + delimiter[0] + "teshen" + delimiter[0] + "region_factor" + delimiter[0] + "Explanation" +
                            delimiter[0] + "Comander_Affect" + delimiter[0] + "harigim";
                        strInParamsValues += delimiter[0] + haka + delimiter[0] + dapa + delimiter[0] + teshen + delimiter[0] + region_factor + delimiter[0] + explantion + delimiter[0] + comander_Affect + delimiter[0] + harigim;
                        break;
                    case "Population":
                        string recommendation = forms.Get("recommendation");
                        explantion = forms.Get("explanation");
                        string exception = forms.Get("exception");
                        string exception_explanation = forms.Get("exception_explanation");
                        strInParamsNames += delimiter[0] + "recommendation" + delimiter[0] + "explantion" + delimiter[0] +
                            "exception" + delimiter[0] + "exception_explanation";
                        strInParamsValues += delimiter[0] + recommendation + delimiter[0] + explantion + delimiter[0] + exception + delimiter[0] + exception_explanation;
                        break;
                    case "Aluf":
                        string explantion_champ_affect = forms.Get("explantion_champ_affect");
                        string champ_affect = (forms.Get("champ_affect") != "0" && forms.Get("champ_affect") != "") ? forms.Get("champ_affect") : null;
                        strInParamsNames += delimiter[0] + "champ_affect" + delimiter[0] + "explantion_champ_affect";
                        strInParamsValues += delimiter[0] + champ_affect + delimiter[0] + explantion_champ_affect;
                        break;
                    case "Shigra":
                        string kamut = forms.Get("kamut");
                        explantion = forms.Get("explanation");
                        comander_Affect = forms.Get("comander_Affect") != "0" ? forms.Get("comander_Affect") : null;
                        strInParamsNames += delimiter[0] + "kamut" + delimiter[0] + "explanation" + delimiter[0] + "comander_Affect";
                        strInParamsValues += delimiter[0] + kamut + delimiter[0] + explantion + delimiter[0] + comander_Affect;
                        break;
                    case "Hanchaiut":
                        string midrag = forms.Get("midrag");
                        strInParamsNames = "Itgonenut_Id" + delimiter[0] + "midrag" + delimiter[0] + "svivaId";
                        strInParamsValues = Itgonenut_Id + delimiter[0] + midrag + delimiter[0] + svivaId;
                        break;
                    case "Haatra":
                        explantion = forms.Get("Explantion_Commander_Affect");
                        comander_Affect = (forms.Get("Comander_Affect") != "0" && forms.Get("Comander_Affect") != "") ? forms.Get("Comander_Affect") : null;
                        strInParamsNames += delimiter[0] + "explanation" + delimiter[0] + "comander_Affect";
                        strInParamsValues += delimiter[0] + explantion + delimiter[0] + comander_Affect;
                        break;
                    case "MidragFinal":
                        break;
                    case "NafaFill":
                        rashutId = forms.Get("rashutId");
                        recommendedPolicy = (forms.Get("recommendedPolicy") != "0" && forms.Get("recommendedPolicy") != "") ? forms.Get("recommendedPolicy") : null;
                        explanationRecommendedPolicy = (forms.Get("ExplanationRecommendedPolicy") != "0" && forms.Get("ExplanationRecommendedPolicy") != "") ? forms.Get("ExplanationRecommendedPolicy") : null;
                        exceptionPolicy = (forms.Get("ExceptionPolicy") != "0" && forms.Get("ExceptionPolicy") != "") ? forms.Get("ExceptionPolicy") : null;
                        strInParamsNames = "userId" + delimiter[0] + "svivaId" + delimiter[0] + "rashutId" + delimiter[0] + "recommendedPolicy" + delimiter[0] + "ExplanationRecommendedPolicy"
                            + delimiter[0] + "exceptionPolicy";
                        strInParamsValues = userId + delimiter[0] + svivaId + delimiter[0] + rashutId + delimiter[0] + recommendedPolicy + delimiter[0] + explanationRecommendedPolicy
                                + delimiter[0] + exceptionPolicy;
                        break;
                    case "DistrictFill":
                        recommendedPolicy = (forms.Get("recommendedPolicy") != "0" && forms.Get("recommendedPolicy") != "") ? forms.Get("recommendedPolicy") : null;
                        explanationRecommendedPolicy = (forms.Get("ExplanationRecommendedPolicy") != "0" && forms.Get("ExplanationRecommendedPolicy") != "") ? forms.Get("ExplanationRecommendedPolicy") : null;
                        adequacy = (forms.Get("adequacy") != "0" && forms.Get("Adequacy") != "") ? forms.Get("Adequacy") : null;
                        strInParamsNames += delimiter[0] + "recommendedPolicy" + delimiter[0] + "explanationRecommendedPolicy" + delimiter[0] + "adequacy";
                        strInParamsValues += delimiter[0] + recommendedPolicy + delimiter[0] + explanationRecommendedPolicy + delimiter[0] + adequacy;
                        break;
                    case "ExceptionFill":
                        rashutId = forms.Get("rashutId");
                        exceptionFillRowId = forms.Get("exceptionFillRowId");
                        parameter = forms.Get("parameter");
                        instituteName = forms.Get("instituteName");
                        exception = forms.Get("exception");
                        explanationToException = forms.Get("explanationToException");
                        itgonenutRegionExceptTo = forms.Get("itgonenutRegionExceptTo");
                        securityTour = forms.Get("securityTour");
                        strInParamsNames = "userId" + delimiter[0] + "svivaId" + delimiter[0] + "rashutId" + delimiter[0] + "exceptionFillRowId"
                       + delimiter[0] + "parameter" + delimiter[0] + "instituteName" + delimiter[0] + "exception" +
                       delimiter[0] + "explanationToException" + delimiter[0] + "itgonenutRegionExceptTo" + delimiter[0] + "securityTour";
                        strInParamsValues = userId + delimiter[0] + svivaId + delimiter[0] + rashutId + delimiter[0] + exceptionFillRowId
                                + delimiter[0] + parameter + delimiter[0] + instituteName + delimiter[0] + exception +
                                delimiter[0] + explanationToException + delimiter[0] + itgonenutRegionExceptTo + delimiter[0] + securityTour;
                        break;
                    default:
                        break;

                }




                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.None);
                context.Response.Write(toJson(GeneralBL.SaveData(pageName, arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "deleteException")
            {
                string exceptionFillRowId = forms.Get("exceptionFillRowId");
                string strInParamsNames = "exceptionFillRowId";
                string strInParamsValues = exceptionFillRowId;

                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.None);
                context.Response.Write(toJson(ExceptionFillBL.deleteException(arrInParamsNames, arrInParamsValues)));
            }
            else if (strOperation == "loadAdminSviva")
            {
                DataTable dt = GeneralBL.LoadAdminSviva();
                context.Response.Write(toJson(dt));
            }
            else if (strOperation == "moveToArchive")
            {

                string fromDate = forms.Get("fromDate") == null ? "null" : forms.Get("fromDate");
                string toDate = forms.Get("toDate") == null ? "null" : forms.Get("toDate");
                string strInParamsNames = "fromDate" + delimiter[0] + "ToDate" + delimiter[0] + "svivaId";

                string strInParamsValues = fromDate + delimiter[0] + toDate + delimiter[0] + svivaId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);


                ArchiveTableResult atr = ArchiveBL.ArchiveTables(arrInParamsNames, arrInParamsValues);
                if (atr.ActionResult == ArchiveTableActionResult.success)
                {
                    string taskOrder_user = (string)HttpContext.Current.Session["UserID"];
                    string taskOrder_Data = atr.CreateTaskOrderData(context.Session["stat"].ToString());

                    string strValues = taskOrder_Data + delimiter[0] + taskOrder_user;
                    string strFields = "taskOrder_Data" + delimiter[0] + "taskOrder_User";
                    arrInParamsNames = strFields.Split(delimiter, StringSplitOptions.None);
                    arrInParamsValues = strValues.Split(delimiter, StringSplitOptions.None);
                    int taskOrderId = int.Parse(clsUtil.ExecuteSQLScalar("dbo.insertUpdateTaskOrder", arrInParamsNames, arrInParamsValues).ToString());

                }
                context.Response.Write(toJson(atr.ActionResult));
            }

            else if (strOperation == "resetTables")
            {
                bool res = GeneralBL.ResetTables();
                context.Response.Write(toJson(res));
            }
            else if (strOperation == "loadUserTypes")
            {
                string page = forms.Get("page");
                DataTable userTypes = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[4];
                context.Response.Write(toJson(userTypes));
            }

            else if (strOperation == "loadSvivot")
            {

                DataTable svivot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[7];
                context.Response.Write(toJson(svivot));
            }
            else if (strOperation == "deleteUser")
            {
                string strInParamsNames = "userToDeleteId" + delimiter[0] + "curUser";
                string userToDeleteId = forms.Get("userIdToDeleteId");
                KtumaUser curUser = ((KtumaUser)context.Session["User"]);
                UserListObj ulb;
                if (curUser == null)
                {
                    ulb = new UserListObj(null, "deleteUser", "-2");
                    context.Response.Write(toJson(ulb));
                    return;
                }
                string strInParamsValues = userToDeleteId + delimiter[0] + curUser.UserId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                UserBL ub = new UserBL();
                DataTable res = ub.DeleteUser(arrInParamsNames, arrInParamsValues);
                ulb = new UserListObj(res, "deleteAction");
                context.Response.Write(toJson(ulb));
            }
            else if (strOperation == "deleteResource")
            {
                string strInParamsNames = "resourceIdToDeleteId" + delimiter[0] + "curUser";
                string resourceIdToDeleteId = forms.Get("resourceIdToDeleteId");
                KtumaUser curUser = ((KtumaUser)context.Session["User"]);
                if (curUser == null)
                {
                    context.Response.Write(toJson(false));
                    return;
                }
                string strInParamsValues = resourceIdToDeleteId + delimiter[0] + curUser.UserId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                ResourceBL rbl = new ResourceBL();
                bool res = rbl.DeleteResource(arrInParamsNames, arrInParamsValues);
                updateApplicationResourcesTableFull();// refresh all the application tables
                context.Response.Write(toJson(res));
            }
            else if (strOperation == "saveUser")
            {
                string strInParamsNames = "userId" + delimiter[0] + "curUser" + delimiter[0] +
                    "userName" + delimiter[0] + "password" + delimiter[0] + "userTypeId" + delimiter[0] + "svivaId" +
                    delimiter[0] + "unitId" + delimiter[0] + "groupId";
                string userName = forms.Get("uName");
                string password = forms.Get("uPassword");
                string userTypeId = forms.Get("uType");
                string userId = forms.Get("userId");
                string unit = forms.Get("unit");
                string[] ResponsibilityArr = unit.Split('_');
                string unitId = ResponsibilityArr[0];
                string groupId = ResponsibilityArr[1];
                KtumaUser curUser = (KtumaUser)context.Session["User"];
                UserListObj ulb;
                if (curUser == null)
                {//session ended
                    ulb = new UserListObj(null, "saveAction", "-2");
                    context.Response.Write(toJson(ulb));
                    return;
                }
                string strInParamsValues = userId + delimiter[0] + curUser.UserId + delimiter[0] +
                    userName + delimiter[0] + password + delimiter[0] + userTypeId + delimiter[0] + svivaId
                    + delimiter[0] + unitId + delimiter[0] + groupId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                UserBL ub = new UserBL();
                ulb = ub.saveUser(arrInParamsNames, arrInParamsValues);
                context.Response.Write(toJson(ulb));
            }
            else if (strOperation == "saveResource")
            {
                string strInParamsNames = "resourceId" + delimiter[0] + "resourceText" + delimiter[0] + "CurUser" + delimiter[0] + "comboId";
                string resourceId = forms.Get("resourceId");
                string resourceText = forms.Get("resourceText");
                string comboId = forms.Get("comboId");
                KtumaUser curUser = (KtumaUser)context.Session["User"];
                DataTable res;
                if (curUser == null)
                {//session ended                                      
                    context.Response.Write(toJson(new DataTable()));
                    return;
                }
                string strInParamsValues = resourceId + delimiter[0] + resourceText + delimiter[0] + curUser.UserId + delimiter[0] + comboId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                ResourceBL rb = new ResourceBL();
                res = rb.SaveResource(arrInParamsNames, arrInParamsValues);
                if (resourceId != "-1")
                {
                    updateApplicationResourcesTable(resourceId, resourceText);
                }
                else
                {//full refresh
                    updateApplicationResourcesTableFull();
                }
                context.Response.Write(toJson(res));
            }
            else if (strOperation == "loadUsers")
            {
                string searchName = string.IsNullOrEmpty(forms.Get("searchName")) ? "null" : forms.Get("searchName");
                string strInParamsNames = "searchName";
                string strInParamsValues = searchName;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                UserBL ub = new UserBL();
                DataTable res = ub.loadUsers(arrInParamsNames, arrInParamsValues);
                UserListObj ulb = new UserListObj(res, "loadUsers");
                context.Response.Write(toJson(ulb));
            }
            else if (strOperation == "UpdateComboIndex")
            {
                string ResourceId = forms.Get("ResourceId");
                string Up = forms.Get("Up");
                KtumaUser curUser = (KtumaUser)context.Session["User"];
                if (curUser == null)
                {
                    context.Response.Write(toJson(false));
                    return;
                }
                string strInParamsNames = "ResourceId" + delimiter[0] + "Up" + delimiter[0] + "curUser";
                string strInParamsValues = ResourceId + delimiter[0] + Up + delimiter[0] + curUser.UserId;
                string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                ResourceBL rb = new ResourceBL();
                DataTable res = rb.UpdateComboIndex(arrInParamsNames, arrInParamsValues);
                updateApplicationResourcesTable(res.Rows[0]["curResourceid"].ToString(),
                    res.Rows[0]["ResourceIdNewIndex"].ToString(),
                    res.Rows[0]["ResourceToReplaceWithId"].ToString(),
                    res.Rows[0]["ResourceToReplaceWithIdNewIndex"].ToString());
                context.Response.Write(toJson(res));
            }



            else
            {
                string strOut = string.Empty;
                //          AddEdit(forms, collectionEmployee, out strOut);
                context.Response.Write(strOut);
            }

        }

        public void updateApplicationResourcesTableFull()
        {
            HttpContext.Current.Application["dsMdTables"] = GeneralBL.GetMdTables();
        }

        public void updateApplicationResourcesTable(string resourceid, string resourceTextNew)
        {
            DataSet dsMdTables = ((DataSet)HttpContext.Current.Application["dsMdTables"]);
            DataTable resources = dsMdTables.Tables[2];
            DataRow curResourceRow = resources.Select("ResourceId=" + resourceid)[0];
            curResourceRow["resourceText"] = resourceTextNew;
        }


        public void updateApplicationResourcesTable(string resourceid, string resourceIdNewIndex,
            string resourceToReplaceWithId, string resourceToReplaceWithIdNewIndex)
        {
            DataSet dsMdTables = ((DataSet)HttpContext.Current.Application["dsMdTables"]);
            DataTable resources = dsMdTables.Tables[2];
            DataRow curResourceRow = resources.Select("ResourceId=" + resourceid)[0];
            DataRow ResourceRowToReplace = resources.Select("ResourceId=" + resourceToReplaceWithId)[0];
            curResourceRow["ComboInternalId"] = int.Parse(resourceIdNewIndex);
            ResourceRowToReplace["ComboInternalId"] = int.Parse(resourceToReplaceWithIdNewIndex);

        }


        public string toJson<T>(T res)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return jsSerializer.Serialize(res);
        }



        public List<Dictionary<string, object>> getTableAsJsonObj(DataTable table)
        {
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return parentRow;
        }


        public string toJson(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        public string toJsonIncRanges(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Dictionary<string, List<Dictionary<string, object>>> ranges = new Dictionary<string, List<Dictionary<string, object>>>();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            //get ranges
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                RangeObject ra = new RangeObject(int.Parse(row["RangeId"].ToString()), row["Text"].ToString());
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                if (!ranges.Keys.Contains(JsonConvert.SerializeObject(ra)))
                {//new rangeid
                    List<Dictionary<string, object>> curRange = new List<Dictionary<string, object>>();
                    curRange.Add(childRow);
                    ranges.Add(JsonConvert.SerializeObject(ra), curRange);
                }
                else
                {//find the range
                    var r = ranges[ranges.Keys.FirstOrDefault(x => x == JsonConvert.SerializeObject(ra))];
                    List<Dictionary<string, object>> curRow = r;
                    curRow.Add(childRow);

                }
            }
            //return JsonConvert.SerializeObject(ranges);
            return jsSerializer.Serialize(ranges);
        }




        public string toJsonDS(DataSet ds)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Dictionary<string, object> parentDs = new Dictionary<string, object>();

            for (int i = 0; i < ds.Tables.Count; i++)
            {
                parentDs.Add(i.ToString(), getTableAsJsonObj(ds.Tables[i]));
            }
            return jsSerializer.Serialize(parentDs);
        }






        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


    }
}
