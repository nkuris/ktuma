﻿using pikudEntities;
using pikudGui;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPages_Heading : System.Web.UI.MasterPage
{
    public string svivaName;
    public string strReturnHomeURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        string userType = (String)Session["UserType"];
        if (userType == null)
        {
            HID_UserTypeId.Value = "-1";
        }
        else
        {
            HID_UserTypeId.Value = userType;
        }
        BTN_ReturnMainPortal.Text = "חזור לפורטל אדום";
        BTN_ReturnMainPortal.Click += BTN_ReturnMainPortal_Click;
        if (!IsPostBack)
        {
            if (Session["User"] != null && Page.ToString() != "ASP.default_aspx")
            {
                LBL_UserName.Text = "שלום " + Session["UserName"];
            }
           
        }
        if (Session["SvivaName"] == null)
        {
            Sviva sv = Helper.GetSvivaFromApplication();
            svivaName = sv.SvivaName;
        }
        else
        {
            svivaName = Session["SvivaName"].ToString();
        }
        if (Session["ktumaPikudBoolean"] != null)
        {
            if ((string)Session["ktumaPikudBoolean"] == "0")
            {
                strReturnHomeURL = "../pages/NafaDistrictHomePage.aspx";
            }
            else
            {
                strReturnHomeURL = "../pages/homepage.aspx";
            }
        }
        else
        {
            strReturnHomeURL = Helper.RedirectToHOmePageByReffere(Request, null);
        }

    }

    void BTN_ReturnMainPortal_Click(object sender, EventArgs e)
    {
        Response.Redirect(ConfigurationManager.AppSettings["MainPortalLink"].ToString());
    }


}
