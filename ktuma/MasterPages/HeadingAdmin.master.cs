﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPages_HeadingAdmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BTN_ReturnMainPortal.Text = "חזור לפורטל אדום";
        BTN_ReturnMainPortal.Click += BTN_ReturnMainPortal_Click;
        BTN_Exit.Click += BTN_Exit_Click;        
        if (!Page.ToString().ToLower().Contains("default"))
        {
            BTN_Exit.Visible = true;
            BTN_ReturnMainPortal.Visible = true;
        }
    }

    void BTN_Exit_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/default.aspx", true);
    }

    void BTN_ReturnMainPortal_Click(object sender, EventArgs e)
    {
        Response.Redirect(ConfigurationManager.AppSettings["MainPortalLink"].ToString());
    }


}
