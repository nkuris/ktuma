﻿using pikudEntities;
using pikudGui;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class MasterPages_HeadingNafot : System.Web.UI.MasterPage
{
    public string svivaName;
    public string pageName;
    public string strSearchNafot;
    public string strSearchDistricts;
    public string strSearchItgonenut_Regions;
    public string strSearchRashuyot;
    public string strReturnHomeURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        strSearchNafot = setNafotStringFromDataTable();
        strSearchDistricts = setDistrictsStringFromDataTable();
        strSearchItgonenut_Regions = setItgonenutRegionsDataTable();
        strSearchRashuyot = setRashuyotDataTable();
        string userType = (String)Session["UserType"];
        
        if (userType == null)
        {
            HID_UserTypeId.Value = "-1";
        }
        else
        {
            HID_UserTypeId.Value = userType;
        }
        
        BTN_ReturnMainPortal.Text = "חזור לפורטל אדום";
        BTN_ReturnMainPortal.Click += BTN_ReturnMainPortal_Click;
        BTN_GoToTableFill.Click += BTN_GoToTableFill_Click;
        BTN_GoToHistory.Click += BTN_GoToHistory_Click;
        if (!IsPostBack)
        {
           
            if (Session["User"] != null && Page.ToString() != "ASP.default_aspx")
            {
                LBL_UserName.Text = "שלום " + Session["UserName"];
            }
            if (Session["SvivaName"] == null)
            {
                Sviva sv = Helper.GetSvivaFromApplication();
                svivaName = sv.SvivaName;
            }
            else
            {
                svivaName = Session["SvivaName"].ToString();
            }
            if(Session["ktumaPikudBoolean"] != null)
            {
                if((string)Session["ktumaPikudBoolean"] == "0")
                {
                    strReturnHomeURL = "../pages/NafaDistrictHomePage.aspx";
                }
                else
                {
                    strReturnHomeURL = "../pages/homepage.aspx";
                }
            }
            else
            {
                strReturnHomeURL = Helper.RedirectToHOmePageByReffere(Request, null);
            }
            BTN_GoToTableFill.Visible = false;
            KtumaUser kut = (KtumaUser)Session["User"];
            string currentPage = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            if (currentPage == "NafaFill.aspx")
            {
                BTN_GoToTableFill.Visible = false;
                BTN_GoToHistory.Text = "צפייה בהסטוריה מדיניות התגוננות נפה";
                pageName = "מדיניות התגוננות נפה";
                BTN_SaveData.Text = "שליחת דו\"ח מדיניות נפה";                
                if (kut != null && kut.GroupId == 3 && kut.KtumaUserType.UserTypeId == 1 /*מנהל נפה*/)
                {
                    BTN_SaveData.Visible = true;
                }
                else
                {
                    BTN_SaveData.Visible = false;
                }

            }
            else if (currentPage == "DistrictFill.aspx")
            {
                BTN_GoToTableFill.Visible = false;
                BTN_GoToHistory.Text = "צפייה בהסטוריה מדיניות התגוננות מחוזות";
                pageName = "מדיניות התגוננות מחוז";
                BTN_SaveData.Text = "שליחת דו\"ח מדיניות מחוז";                
                if (kut != null && kut.GroupId == 2 && kut.KtumaUserType.UserTypeId == 1 /*מנהל מחוז*/)
                {
                    BTN_SaveData.Visible = true;
                }
                else
                {
                    BTN_SaveData.Visible = false;
                }

            }
            else if (currentPage == "ExceptionFill.aspx")
            {
                BTN_GoToTableFill.Visible = false;
                BTN_GoToHistory.Text = "צפייה בהסטוריה מדיניות התגוננות החרגות";
                pageName = "מדיניות התגוננות החרגות";
                BTN_SaveData.Text = "שליחת דו\"ח מדיניות החרגות";
                if (kut != null && kut.GroupId == 2 && kut.KtumaUserType.UserTypeId == 1 /*מנהל מחוז*/)
                {
                    BTN_SaveData.Visible = true;
                }
                else
                {
                    BTN_SaveData.Visible = false;
                }
            }
            else if (currentPage == "NafaFillHistory.aspx")
            {
                pageName = "מדיניות התגוננות נפה - הסטוריה";
                if (kut != null && kut.GroupId == 3)
                {
                    BTN_GoToTableFill.Visible = true;
                }
                BTN_GoToTableFill.Text = "טבלה למילוי מדיניות התגוננות נפות";
                BTN_GoToHistory.Visible = false;
            }
            else if (currentPage == "DistrictFillHistory.aspx")
            {
                pageName = "מדיניות התגוננות מחוז - הסטוריה";
                if (kut != null && kut.GroupId == 2)
                {
                    BTN_GoToTableFill.Visible = true;
                }                
                BTN_GoToTableFill.Text = "טבלה למילוי מדיניות התגוננות מחוזות";
                BTN_GoToHistory.Visible = false;
            }
            else if (currentPage == "ExceptionFillHistory.aspx")
            {
                pageName = "מדיניות התגוננות החרגות - הסטוריה";
                if (kut != null && kut.GroupId == 2)
                {
                    BTN_GoToTableFill.Visible = true;
                }                
                BTN_GoToTableFill.Text = "טבלה למילוי מדיניות התגוננות החרגות";
                BTN_GoToHistory.Visible = false;
            }
            

            
        }
        
    }

    private string setRashuyotDataTable()
    {
        string res = string.Empty;
        DataTable rashuyot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[11];
        if (rashuyot != null && rashuyot.Rows.Count > 0)
        {
            for (int i = 0; i < rashuyot.Rows.Count; i++)
            {
                res += rashuyot.Rows[i]["rashutId"].ToString() + ",";
            }
            res += rashuyot.Rows[rashuyot.Rows.Count - 1]["rashutId"].ToString();
        }
        return res;
    }

    private string setItgonenutRegionsDataTable()
    {
        string res = string.Empty;
        DataTable itRegions = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[1];
        if (itRegions != null && itRegions.Rows.Count > 0)
        {
            for (int i = 0; i < itRegions.Rows.Count; i++)
            {
                res += itRegions.Rows[i]["Id"].ToString() + ",";
            }
            res += itRegions.Rows[itRegions.Rows.Count - 1]["Id"].ToString();
        }
        return res;
    }

    private string setNafotStringFromDataTable()
    {
        string res = string.Empty;
        DataTable nafot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[9];
        if (nafot != null && nafot.Rows.Count > 0)
        {
            for (int i = 0; i < nafot.Rows.Count; i++)
            {
                res += nafot.Rows[i]["Id"].ToString() + ",";
            }
            res += nafot.Rows[nafot.Rows.Count - 1]["Id"].ToString();
        }

        return res;
    }

    private string setDistrictsStringFromDataTable()
    {
        string res = string.Empty;
        DataTable districts = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[10];
        if (districts != null && districts.Rows.Count > 0)
        {
            for (int i = 0; i < districts.Rows.Count; i++)
            {
                res += districts.Rows[i]["Id"].ToString() + ",";
            }
            res += districts.Rows[districts.Rows.Count - 1]["Id"].ToString();
        }

        return res;
    }

    private void BTN_GoToHistory_Click(object sender, EventArgs e)
    {
        string currentPage = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
        string pageName = string.Empty;
        if (currentPage == "NafaFill.aspx")
        {
            pageName = "NafaFillHistory.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
        else if (currentPage == "DistrictFill.aspx")
        {
            pageName = "DistrictFillHistory.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
        else
        {
            pageName = "ExceptionFillHistory.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
    }

    private void BTN_GoToTableFill_Click(object sender, EventArgs e)
    {
        string currentPage = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
        string pageName = string.Empty;
        if (currentPage == "NafaFillHistory.aspx")
        {
            pageName = "NafaFill.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
        else if (currentPage == "DistrictFillHistory.aspx")
        {
            pageName = "DistrictFill.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
        else
        {
            pageName = "ExceptionFill.aspx";
            Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        }
    }

    void BTN_ReturnMainPortal_Click(object sender, EventArgs e)
    {
        Response.Redirect(ConfigurationManager.AppSettings["MainPortalLink"].ToString());
    }
}
