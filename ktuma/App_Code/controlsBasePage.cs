﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using PikudBL;
using System.Diagnostics;

namespace pikudGui
{
    public class controlsBasePage : System.Web.UI.UserControl
    {
        public static string[] arrDelimiter = new string[1] { "!$!" };
        public static string[] arrCommaDelimiter = new string[1] { "," };
        public static string nbsp1 = "&nbsp;";
        public static string strRoleShort = "קצר";
        public static string strRoleLong = "ארוך";
        public static int widthWide = Convert.ToInt32(ConfigurationManager.AppSettings["widthWide"]);
        public static int widthSmall = Convert.ToInt32(ConfigurationManager.AppSettings["widthSmall"]);
        public static int widthExtraWide = Convert.ToInt32(ConfigurationManager.AppSettings["widthExtraWide"]);

        public static string strRegx = @"^[0-9a-zA-Z" + //any letter or number
                    "א-ת" + // heb letters
                    @"!@#$%^&*_:;,.=+<>?\s'" + //some special chars and space
                    @"""" + //double quotes
                    @"\-\(\)\{\}\[\]/\\" + //some reserved chars that not allowed without slash
                    @"-]*$"; //close the regx

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        //public static string replaceGereshEncoded(string strValue)
        //{
        //    string temp = strValue.Replace("&#39;&#39;", "&#39;");
        //    //strValue = temp.Replace("''", "'");//.Replace("&#39;&#39;", "&#39;");
        //    return temp;
        //}

        //public static string htmlDecode(string strValue)
        //{
        //    string temp = HttpContext.Current.Server.HtmlDecode(strValue);
        //    //strValue = temp.Replace("''", "'");//.Replace("&#39;&#39;", "&#39;");
        //    return temp;
        //}
        #region helperFunction
        public static void writeLog(string msg, Boolean isErr)
        {
            string sSource = "rafGui";
            if (isErr)
            {
                EventLog.WriteEntry(sSource, " Gui layer error found: \n\n" + msg
                                            , EventLogEntryType.Error, 1);
            }
            else
            {
                EventLog.WriteEntry(sSource, " Gui layer info msg: \n\n" + msg
                                            , EventLogEntryType.Information, 2);
            }
        }
        #endregion
   }
}