﻿using pikudEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Helper
/// </summary>
namespace pikudGui
{
    public static class Helper
    {

        public static Sviva GetSvivaFromApplication()
        {            
            Sviva res = null;
            DataTable Svivot = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[7];
            DataTable dtParams = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[3];
            int currentSvivaId = int.Parse((from myRow in dtParams.AsEnumerable()
                                            where myRow.Field<string>("prm_name") == "CurrentSvivaId"
                                            select myRow).Single()["prm_value"].ToString());
            var svivRow = (from myRow in Svivot.AsEnumerable()
                        where myRow.Field<int>("svivaId") == currentSvivaId
                        select myRow).Single();
            if (svivRow != null)
            {
                res = new Sviva(int.Parse(svivRow["svivaId"].ToString()), svivRow["svivaName"].ToString());
            }
            return res;

        }

        public static string RedirectToHOmePageByReffere(HttpRequest Request, string logout)
        {
            string strReturnHomeURL = "../Pages/NafaDistrictHomePage.aspx";
            if (Request.UrlReferrer != null)
            {
                string previousPageName = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
                if(previousPageName == "HomePage.aspx" && logout!= null && logout == "1")
                {
                    strReturnHomeURL = "~/pages/homepage.aspx";
                }
                if (previousPageName == "NafaFill.aspx" ||
                    previousPageName == "NafaFillHistory.aspx" ||
                    previousPageName == "DistrictFill.aspx" ||
                    previousPageName == "DistrictFillHistory.aspx" ||
                    previousPageName == "ExceptionFill.aspx" ||
                    previousPageName == "ExceptionFillHistory.aspx" ||
                    previousPageName == "NafaDistrictHomePage.aspx"
                    )
                {
                    strReturnHomeURL = "../Pages/NafaDistrictHomePage.aspx";
                }
                else
                {
                    strReturnHomeURL = "../pages/HomePage.aspx";
                }
            }
            else
            {
                strReturnHomeURL = "~/pages/HomePage.aspx";
            }
            return strReturnHomeURL;
        }

    }

}