﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using PikudBL;

using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Web.Script.Serialization;
using pikudEntities;
namespace pikudGui
{
    public enum UIMODE
    {
        NEW,
        EDIT,
        DELETE
    }

    public class BasePage : System.Web.UI.Page
    {
        public static string strRegx = @"^[0-9a-zA-Z" + //any letter or number
                    "א-ת" + // heb letters
                    @"!@#$%^&*_:;,.=+<>?\s'-" + //some special chars and space
                    @"""" + //double quotes
                    @"\-\(\)\{\}\[\]/\\" + //some reserved chars that not allowed without slash
                    @"-]*$"; //close the regx
        public static string constFooterColor = "LightSkyBlue"; //"LemonChiffon";
        public static int widthWide = Convert.ToInt32(ConfigurationManager.AppSettings["widthWide"]);
        public static int widthSmall = Convert.ToInt32(ConfigurationManager.AppSettings["widthSmall"]);
        public static int widthExtraWide = Convert.ToInt32(ConfigurationManager.AppSettings["widthExtraWide"]);
        public static string nbsp1 = "&nbsp;";
        public static string nbsp2 = "&nbsp;&nbsp;";
        public static string nbsp4 = "&nbsp;&nbsp;&nbsp;&nbsp;";
        public static string[] arrDelimiter = new string[1] { "!$!" };
        public static string[] arrCommaDelimiter = new string[1] { "," };
        public static string[] arrDelimiter2 = new string[1] { "!@!" };
        public static string[] arrYanivYhc = new string[1] { "yanivyhc" };
        public static string strNoData = "לא נמצאו נתונים";
        public static string strGlobalError = "אירעה שגיאה כללית - ";
        public static string strSaveSuccess = "השמירה בוצעה בהצלחה";

        public static string strNoRowsFound = "לא קיימות רשומות לפרמטרים שנבחרו";

        public void checkSession()
        {
            if (Session["UserName"] == null && Page.ToString() != "ASP.default_aspx")
            {
                Response.Redirect("~/default.aspx?Logout=1&");
                //System.Drawing.Color.LightSkyBlue
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            this.Load += new EventHandler(this.Page_Load);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = -1;
            checkSession();
        }


        public void setMasterLblMsg(string strMsg)
        {
            Label olbl = (Label)Master.FindControl("lblMsg");
            olbl.Text = strMsg;
        }

        //public void setMenu2Start(Boolean bln,string strAdd)
        //{
        //    ((Menu)Master.FindControl("menu2")).Visible = bln;

        //    //((Menu)Master.FindControl("menu1")).Visible = !bln;
        //    if (bln) //is showing menu2 then start with - "pratim ishim"
        //    {
        //        //it will ake the value of the session
        //        //on the page_load of master page
        //        Session["lblMenuChoosen"] = strMenu2Start + strAdd;
        //    }
        //}


        public void setEnabledRecursive(Control ctl, Boolean isEnabled)
        {
            //((Label)Master.FindControl("lblMsg")).Text = counter.ToString();
            foreach (Control ChiledCtl in ctl.Controls)
            {
                if (ChiledCtl.GetType() == typeof(TextBox))
                {
                    ((TextBox)ChiledCtl).Enabled = false;
                }
                else if (ChiledCtl.GetType() == typeof(DropDownList))
                {
                    ((DropDownList)ChiledCtl).Enabled = false;
                }
                else
                {
                    setEnabledRecursive(ChiledCtl, isEnabled);
                }
            }
        }

        public static void fillGenericCombo
                (string SelectCommand, string inParamsNames, string inParamsValues,
                        DropDownList cboToFill, string drpDataValueField, string drpDataTextField)
        {
            if (inParamsNames != null)
            {
                string[] arrInParamsNames = inParamsNames.Split(',');
                string[] arrInParamsValues = inParamsValues.Split(arrDelimiter, StringSplitOptions.RemoveEmptyEntries);
                FillCombo(SelectCommand, arrInParamsNames, arrInParamsValues, cboToFill
                                , drpDataValueField, drpDataTextField);
            }
            else
            {
                FillComboNoParams(SelectCommand, cboToFill
                                , drpDataValueField, drpDataTextField);
            }

        }

        private static void FillCombo
                    (string spName, string[] arrInParamsNames, string[] arrInParamsValues,
                    DropDownList cboToFill, string strItemField, string strTextField)
        {
            DataTable dt = clsUtil.FillDt(spName, arrInParamsNames, arrInParamsValues);
            cboToFill.Items.Clear();

            foreach (DataRow dr in dt.Rows)
                cboToFill.Items.Add(new ListItem(dr[strTextField].ToString(), dr[strItemField].ToString()));
            dt = null;
        }


        private static void FillComboNoParams
                    (string spName,
                    DropDownList cboToFill, string strItemField, string strTextField)
        {
            DataTable dt = clsUtil.FillDtNoParams(spName);
            if (dt == null)
            {
                return;
            }
            cboToFill.Items.Clear();

            foreach (DataRow dr in dt.Rows)
                cboToFill.Items.Add(new ListItem(dr[strTextField].ToString(), dr[strItemField].ToString()));
            dt = null;
        }

        #region helperFunction
        public static void writeLog(string msg, Boolean isErr)
        {
            string sSource = "rafGui";
            //string sLog = "rafGui";

            //if (!EventLog.SourceExists(sSource))
            //   EventLog.CreateEventSource(sSource, sLog);

            if (isErr)
            {
                EventLog.WriteEntry(sSource, " Gui layer error found: \n\n" + msg
                                            , EventLogEntryType.Error, 1);
            }
            else
            {
                EventLog.WriteEntry(sSource, " Gui layer info msg: \n\n" + msg
                                            , EventLogEntryType.Information, 2);
            }
        }
        #endregion


        public static string replaceNbspAsSpaceString(string strValue)
        {
            string temp = strValue.Replace("&nbsp;", " ");
            return temp;
        }

        public static string getParamValueFromParamsTableByKey(string prm_name)
        {
            string prm_value = string.Empty;
            DataTable dtParams = ((DataSet)HttpContext.Current.Application["dsMdTables"]).Tables[3];
            prm_value = (from myRow in dtParams.AsEnumerable()
                         where myRow.Field<string>("prm_name") == prm_name
                         select myRow).Single()["prm_value"].ToString();
            return prm_value;
        }

    }
}