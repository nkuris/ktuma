﻿var allOrigData;
//color ranges
var data_color_red_gt_or_eq, data_color_red_lt_or_eq, data_color_orangeRed_gt_or_eq;
var data_color_orangeRed_lt_or_eq, data_color_green_gt_or_eq, data_color_green_lt_or_eq,
data_color_orange_gt_or_eq, data_color_orange_lt_or_eq;

var data_refreshTimeoutInMS, data_undoCheckoutTimeoutInMS;
var timer_gear = -1;
var glbEditedLine_DataId = -1;
var glbTimerObject;

var glbEditedLine_Val24_BeforeRefresh;
var glbEditedLine_Val48_BeforeRefresh;
var glbEditedLine_Val72_BeforeRefresh;


function JSDateToFormat(date) {
    var day = date.getDate();       // yields date
    var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
    var year = date.getFullYear();  // yields year

    var time = day + "/" + month + "/" + year;
    return time;
}


function displayTime(jsonDate, full) {
    var d = new Date(parseInt(jsonDate.substr(6)));
    if (full) {
        return JSDateToFullFormat(d);
    }
    else {
        return JSDateToDateFormat(d);
    }
}


function createTDWithWrap(Itgonenut_Id, fieldName, value)
{
    return '<td style="max-width:150px;word-wrap:break-word;" id="TD_' + fieldName + '_' + Itgonenut_Id + '" >' + value + '</td>';
}

function JSDateToDateFormat(date) {
    var day = date.getDate();       // yields date
    var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    var year = date.getFullYear();  // yields year
    
    var time = day + "/" + month + "/" + year;
    return time;
}

function JSDateToFullFormat(date)
{
    var day = date.getDate();       // yields date
    var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    var year = date.getFullYear();  // yields year
    var hour = date.getHours();     // yields hours 
    var minute = date.getMinutes(); // yields minutes
    var second = date.getSeconds(); // yields seconds
    if (second  < 10) {
        second = '0' + second;
    }
    if (minute  < 10) {
        minute = '0' + minute;
    }
    if (hour  < 10) {
        hour = '0' + hour;
    }
    var time = day + "/" + month + "/" + year + " " + hour + ':' + minute + ':' + second;
    return time;
}

function loadAdminSviva(userSvivaId)
{
    var jsonObj =
                     { op: 'loadAdminSviva' };

    $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (response) {
            var adminSvivaId = response[0].SvivaId;
            if (userSvivaId != undefined && userSvivaId != '') {
                if (adminSvivaId != userSvivaId) {
                    alert('שים לב - סביבת העבודה שונתה על ידי מנהל המערכת, הסביבה הנכונה היא ' + response[0].SvivaName + ', אנא בצע כניסה מחדש למערכת.');
                }
            }
        }
    });
    
}

function setInputOnlyPositiveOrZero(id) {
    $('#' + id).attr('onkeydown', 'return isNumberKey(event)');
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57 ||  event == 8 || event== 46 ))
        return false;

    return true;
}

function doRefresh(isFullRefresh, timeInMS, alwaysFullRefresh) //
{
    timeInMS = 10000;
    // alert('a');
    if (isFullRefresh)//
    {
        refresh(true);
    }
    else//
    {
        refresh(false);
    }    
    setTimeout("doRefresh(" + false + ", " + timeInMS + ")", timeInMS);
}

function setRangesCol(tblID)
{
    
    var jsonObj =
                     { op: 'loadRanges' };

    $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (response) {
            var id = $('#' + tblID + ' tr');
            //console.log(data);
            var curRow = 1;
            var myform1 = $(id).eq(curRow)
            var rowSpan = response[0].num;
            var curText = response[0].Text;
            myform1.prepend('<td style="border:1px solid black;" rowspan=' + rowSpan + '>' + curText + '</td>')
            for(var i=1; i<response.length; i++)
            {
                curRow = curRow + rowSpan;
                myform1 = $(id).eq(curRow)
                rowSpan = response[i].num;
                curText = response[i].Text;
                myform1.prepend('<td style="border:1px solid black;" rowspan=' + rowSpan + '>' + curText + '</td>')
            }
            

        }
    });
    
}

function getClassByValue(val)
{
    if (parseInt(val) >= parseInt(data_color_red_gt_or_eq) && parseInt(val) <= parseInt(data_color_red_lt_or_eq)) //
    {
        return 'data_color_red';
    }
    else if (parseInt(val) >= parseInt(data_color_orangeRed_gt_or_eq) && parseInt(val) <= parseInt(data_color_orangeRed_lt_or_eq))
    {
        return 'data_color_orangeRed';
    }
    else if (parseInt(val) >= parseInt(data_color_orange_gt_or_eq) && parseInt(val) <= parseInt(data_color_orange_lt_or_eq))
    {
        return 'data_color_orange';
    }
    else if (parseInt(val) >= parseInt(data_color_green_gt_or_eq) && parseInt(val) <= parseInt(data_color_green_lt_or_eq))
    {
        return 'data_color_green';
    }
}

function getClassByExactValuePop(val) {
    if (val == 1 || val == 2) //
    {
        return 'data_color_green';
    }
    else if (val == 3 || val == 4) //
    {
        return 'data_color_orange';
    }
    else if (val == 5 || val == 6) //
    {
        return 'data_color_orangeRed';
    }
    else if (val == 7 || val == 8){
        return 'data_color_red';
    }
}

function getClassByExactValue(val) {
    if (val == 4) //
    {
        return 'data_color_red';
    }
    else if (val == 3) //
    {
        return 'data_color_orangeRed';
    }
    else if (val == 2) //
    {
        return 'data_color_orange';
    }
    else
    {
        return 'data_color_green';
    }
}

function changeColoredDataValueInLine(myInput, val) //
{
    //value textBox by fieldId with colors
    if (val >= data_color_red_gt_or_eq && val <= data_color_red_lt_or_eq) //
    {
        myInput.attr('class', 'data_color_red');
    }
    else if (val >= data_color_orangeRed_gt_or_eq && val <= data_color_orangeRed_lt_or_eq) //
    {
        myInput.attr('class', 'data_color_orangeRed');
    }
    else if (val >= data_color_orange_gt_or_eq && val <= data_color_orange_lt_or_eq) //
    {
        myInput.attr('class', 'data_color_orange');
    }
    else if (val >= data_color_green_gt_or_eq && val <= data_color_green_lt_or_eq) //
    {
        myInput.attr('class', 'data_color_green');
    }
}

function changeColoredDataValueInLineByValPop(myInput, val) //
{

    //value textBox by fieldId with colors
    if (val == 7 || val == 8) //
    {
        myInput.attr('class', 'data_color_red');
    }
    else if (val == 5 || val == 6) //
    {
        myInput.attr('class', 'data_color_orangeRed');
    }
    else if (val == 3 || val == 4) //
    {
        myInput.attr('class', 'data_color_orange');
    }
    else if (val == 1 || val == 2) //
    {
        myInput.attr('class', 'data_color_green');
    }
}
    


function changeColoredDataValueInLineByVal(myInput, val) //
{
    //value textBox by fieldId with colors
    if (val == 4) //
    {
        myInput.attr('class', 'data_color_red');
    }
    else if (val == 3) //
    {
        myInput.attr('class', 'data_color_orangeRed');
    }
    else if (val == 2) //
    {
        myInput.attr('class', 'data_color_orange');
    }
    else if (val == 1) {
        myInput.attr('class', 'data_color_green');
    }
    else {
        myInput.attr('class', 'data_color_white');
    }

}
function startTimerForUndoCheckout(data_id, timeInMS)//
{
    //send undo(data_id) to setTimeout after 6000 mili seconds
    //glbTimerObject = setTimeout("UndoCheckout(" + data_id + ")", (120 * 1000));
    glbTimerObject = setTimeout("UndoCheckout(" + data_id + ")", timeInMS); //(120 * 1000));
    timer_gear = 1;
}

function createEditButtonIncCancelButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick)
{
    var curClass;
    if (disableAllEditButtons != '')
    {
        curClass = "btnEditDisabled";
    }
    else
    {
        curClass = "btnEdit";
    }
    return '<td id="TD_Edit_' + Itgonenut_Id + '" >' +
        '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick
        + '<input type="button" value="בטל" class="btnSaveDispNone" id="btnUndo' + Itgonenut_Id + strUndoClick + '</td>';//edit button (checkOut)
}

function createEditButtonIncCancelButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick)
{
    var curClass;
    if (disableAllEditButtons != '')
    {
        curClass = "btnEditDisabled";
    }
    else
    {
        curClass = "btnEdit";
    }
    return '<td id="TD_Edit_' + Itgonenut_Id + '" >' +
        '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick
        + '<input type="button" value="בטל" class="btnSaveDispNone" id="btnUndo' + Itgonenut_Id + strUndoClick + '</td>';//edit button (checkOut)
}

function createEditButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick) {
    var curClass;
    if (disableAllEditButtons != '') {
        curClass = "btnEditDisabled";
    }
    else {
        curClass = "btnEdit";
    }
    return '<td id="TD_Edit_' + Itgonenut_Id + '" >' +
        '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick + '</td>';//edit button (checkOut)
}

function createDelAndEditButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick,strDelete) {
    var curClass;
    if (disableAllEditButtons != '') {
        curClass = "btnEditDisabled";
    }
    else {
        curClass = "btnEdit";
    }
    return '<td id="TD_Edit_' + Itgonenut_Id + '" >' +
        '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick
        + '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="מחק" id="btnDelete' + Itgonenut_Id + strDelete
        + '</td>';//edit button (checkOut)
}

function createDeleteButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick) {
    var curClass;
    if (disableAllEditButtons != '') {
        curClass = "btnEditDisabled";
    }
    else {
        curClass = "btnEdit";
    }
    return '<td id="TD_Delete_' + Itgonenut_Id + '" >' +
        '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="מחק" id="btnEdit' + Itgonenut_Id + strClick + '</td>';//edit button (checkOut)
}

function createEditButtonNoExternal(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick) {
    var curClass;
    if (disableAllEditButtons != '') {
        curClass = "btnEditDisabled";
    }
    else {
        curClass = "btnEdit";
    }
    return '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick
}

function createEditButtonNoExternalIncCancelButton(Itgonenut_Id, disableAllEditButtons, strClick, strUndoClick) {
    var curClass;
    if (disableAllEditButtons != '') {
        curClass = "btnEditDisabled";
    }
    else {
        curClass = "btnEdit";
    }
    return '<input type="button" ' + disableAllEditButtons + ' class="' + curClass + '" value="ערוך" id="btnEdit' + Itgonenut_Id + strClick
        + '<input type="button" value="בטל" class="btnSaveDispNone" id="btnUndo' + Itgonenut_Id + strUndoClick;//edit button (checkOut)
}

function createSaveButtonIncCancelButton(Itgonenut_Id, strClick, strUndoClick) {
    return '<td>' + '<input type="button" value="שמור" class="btnSave" id="btnEdit' + Itgonenut_Id + strClick
       + '<input type="button" value="בטל" class="btnSave" id="btnUndo' + Itgonenut_Id + strUndoClick + '</td>';
}

function createSaveButton(Itgonenut_Id, strClick, strUndoClick)
{
    return '<td>' + '<input type="button" value="שמור" class="btnSave" id="btnEdit' + Itgonenut_Id + strClick + '</td>';
}
function createSTRClick(Itgonenut_Id)
{
    return '" OnClick="checkOutOrSave(' + Itgonenut_Id + ');" />';
}

function createSTRDeleteClick(rowId) {
    return '" OnClick="deleteRow(' + rowId + ');" />';
}

function createSTRUndoClick(Itgonenut_Id) {
    return '" OnClick="UndoCheckout(' + Itgonenut_Id + ');" />';
}

function setValuesForUndoCheckout(editedObj, originalText)
{
    var arr = originalText.split(" - ");
    editedObj.val = arr[0];
    editedObj.text = arr[1];
}

function setValuesForUndoCheckoutFrom(editedObj, fieldName, ItgonenutId) {
    editedObj.val = $("#SELECT_" + fieldName + "_" + ItgonenutId).val();
    editedObj.text = $("#SELECT_" + fieldName + "_" + ItgonenutId + ' option:selected').text();
}

function setValuesForUndoCheckoutFromTextOnly(editedObj, fieldName, ItgonenutId) {
    var str = $("#SELECT_" + fieldName + "_" + ItgonenutId + ' option:selected').text();
    arr = str.split(" - ");
    editedObj.val = arr[0];
    editedObj.text = arr[1]
}


function setColoredDataValueInLineByVal(myInput, val) //
{
    //value textBox by fieldId with colors
    if (val == 4) //
    {
        myInput.css('background-color', 'red');
    }
    else if (val == 3) //
    {
        myInput.css('background-color', 'orangeRed');
    }
    else if (val == 2) //
    {
        myInput.css('background-color', 'orange');
    }
    else if(val == 1)
    {
        myInput.css('background-color', 'green');
    }
    else {
        myInput.css('background-color', '#d2d9f6');
    }

}

function setDialogStyle(approveButtonId, cancelButtonId) {
    $('.ui-dialog-titlebar').css('text-align', 'center');
    $('.ui-dialog-titlebar').css('background-color', '#FF9333');
    $('.ui-dialog-titlebar').css('color', '#ffffff');
    $('.ui-dialog-titlebar').css('font-size', '20px');
    $('.ui-dialog-titlebar').css('height', '20px');
    $('#' + approveButtonId).css('text-align', 'center');
    $('#' + approveButtonId).css('background-color', '#FF9333');
    $('#' + approveButtonId).css('color', '#ffffff');
    $('#' + approveButtonId).css('font-size', '16px');
    $('#' + approveButtonId).css('width', '60px');
    $('#' + approveButtonId).css('border-radius', '10px');
    $('#' + cancelButtonId).css('text-align', 'center');
    $('#' + cancelButtonId).css('background-color', '#FF9333');
    $('#' + cancelButtonId).css('color', '#ffffff');;
    $('#' + cancelButtonId).css('font-size', '16px');
    $('#' + cancelButtonId).css('width', '60px');
    $('#' + cancelButtonId).css('border-radius', '10px');
}

function createItgonenutRegionsCombo(dataTable, filterField, td, rowIndex, includeNoOption, selectedId, cboWidth) {
    if (cboWidth == null) {
        cboWidth = '150';
    }
    cbo = $("<select id='SELECT_" + filterField + "_" + rowIndex + "' style='width:" + cboWidth + "px;max-width: " + cboWidth + "px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;' ></select > ");
   
    if (includeNoOption) {
        cbo.append($("<option>").attr('value', '0').text(''));
    }
    $(dataTable).each(function () {
        text = ''
        cbo.append($("<option>").attr('value', this.Id).text(this.Name));

    });
    //set selectedtext
    if (selectedId != null && selectedId > 0) {
        cbo.val(selectedId);
    }
    td.append(cbo);
    console.log(dataTable);
}

function createRashuyotCombo(dataTable, filterField, td, rowIndex, includeNoOption, selectedId, unitId, cboWidth) {
    filteredDataTable = jQuery.grep(dataTable, function (n, i) {
        return n['DistrictId'] == unitId;
    });
    cbo = $("<select id='SELECT_" + filterField + "_" + rowIndex + "' style='width:" + cboWidth + "px; max-width:" + cboWidth + "px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'></select>");
    if (includeNoOption) {
        cbo.append($("<option>").attr('value', '0').text(''));
    }
    $(filteredDataTable).each(function () {
        text = ''
        cbo.append($("<option>").attr('value', this.rashutId).text(this.RashutName));

    });
    //set selectedtext
    if (selectedId != null && selectedId > 0) {
        cbo.val(selectedId);
    }
    td.append(cbo);

}


function createCombo(dataTable, filterField, td, ItgonenutId, includeNoOption, selectedId, textIncNumbering, noFilteringApplied, cboWidth) {

    if (!noFilteringApplied) {
        filteredDataTable = jQuery.grep(dataTable, function (n, i) {
            return n['TableColumn'] == filterField;
        });
    }
    else {
        filteredDataTable = dataTable;
    }
    cbo = $("<select id='SELECT_" + filterField + "_" + ItgonenutId + "' style='width:" + cboWidth + "px; max-width:" + cboWidth + "px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'></select>");
    if (includeNoOption) {
        cbo.append($("<option>").attr('value', '0').text(''));
    }
    $(filteredDataTable).each(function () {
        text = ''
        if (textIncNumbering) {
            text = this.ComboInternalId + ' - ' + this.ResourceText;
        }
        else {
            text = this.ResourceText;
        }
        cbo.append($("<option>").attr('value', this.ComboInternalId).text(text));

    });
    //set selected
    if (selectedId != null && selectedId > 0) {
        cbo.val(selectedId);
    }
    td.append(cbo);

}

function numberInRange(input, minVal, maxVal)
{
    return (input >= minVal && input<= maxVal)
}

function createInput(td, fieldName, ItgonenutId, formerValue) {
    var inp = $('<input type="text" id="INP_' + fieldName + '_' + ItgonenutId + '" value=""  class="inputReg" />')
    inp.val(formerValue);
    td.append(inp);
}

function createTextArea(td, fieldName, ItgonenutId, formerValue, textAreaWidth) {
    var inp = $('<textarea rows="5"   id="INP_' + fieldName + '_' + ItgonenutId + '" maxlength="250" style="width:' + textAreaWidth + 'px;"   ></textarea>');
    inp.val(formerValue);    
    td.append(inp);
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


function setTDValTextAndColorObj(td, obj, isDouble) {
    setTDValTextAndColor(td, obj.val, obj.text, isDouble);
}

function createOrigTD(bgclass, fieldName, itgonenutId, text, value)
{
    if (value == null) {
        return '<td id="TD_' + fieldName + '_' + itgonenutId + '">' + '</td>';
    }
    else {
        return '<td id="TD_' + fieldName + '_' + itgonenutId + '" class="' + bgClass + '">' + value + ' - ' + text + '</td>';
    }
}

function createOrigTDIncNull(bgclass, fieldName, id, text, value) {
    if (value == null) {
        return '<td id="TD_' + fieldName + '_' + id + '">' + text + '</td>';
    }
    else {
        return '<td id="TD_' + fieldName + '_' + id + '" class="' + bgClass + '">' + value + ' - ' + text + '</td>';
    }
}

function setTDValTextAndColor(td, val, text, isDouble) {
    if (isDouble) {
        changeColoredDataValueInLineByValPop(td, val);
    } else {
        changeColoredDataValueInLineByVal(td, val);
    }

    if (val == 0 || val == null) {
        td.html('');
    }
    else if (text != null && text != '' && (text != val)) {
        td.html(val + ' - ' + text);
    }
    else {
        td.html(val);
    }


}

function setTDValTextAndColorShowTextForNull(td, val, text, isDouble) {
    if (val == null) {
        val = 0;
    }
    if (isDouble) {
        changeColoredDataValueInLineByValPop(td, val);
    } else {
        changeColoredDataValueInLineByVal(td, val);
    }
    
    if (val == 0 && text != null && text != '' && (text != val)) {
        td.html(text);
    }
    else if (val != 0 && text != null && text != '' && (text != val)) {
        td.html(val + ' - ' + text);
    }
    else {
        td.html(val);
    }


}


//enable back all buttons with edit (not save" by common css class
//change save class back to edit class
//then change his text to edit again and enable all edit buttons
function resetEditButtonsConfiguration(ItgonenutId, btnEditId, editButtonsClass, btnUndoId, TdUserNameId)
{
    $("#" + btnEditId + ItgonenutId).attr('class', 'btnEdit');
    $("#" + btnEditId + ItgonenutId).val("ערוך");
    $("." + editButtonsClass).removeAttr('disabled').attr('class', 'btnEdit');
    $("#" + btnUndoId + ItgonenutId).attr('class', 'btnSaveDispNone');
    $('#' + TdUserNameId + '_' + ItgonenutId).html('');
}


function checkSession(userAttr)
{
    if (userAttr == null || userAttr == '') {
        window.location = "../default.aspx";
    }
}
function checkLoginInput(txt_uname, txt_password) {

    var username = $('#' +txt_uname).val();
    var password = $('#' + txt_password).val();
    if (username == '') {
        alert("הכנס שם משתמש");
        return false;
    }
    if (password == '') {
        alert("הכנס סיסמא")
        return false;
    }
    return true;    
}


//change edit class to save class, to avoid disable this button
//then change his text to save and disable all other edit buttons
function ShowSaveButtonsConfiguration(ItgonenutId, btnEditId, editButtonsClass, btnUndoId, TdUserNameId, userNameTableCellHTML) {
    $("#" + btnEditId + ItgonenutId).attr('class', 'btnSave');
    $("#" + btnUndoId + ItgonenutId).attr('class', 'btnSave');
    $("#" + btnEditId + ItgonenutId).val("שמור");
    $("." + editButtonsClass).attr('class', 'btnEditDisabled').attr('disabled', 'disabled');
    $("#" + TdUserNameId + "_" + ItgonenutId).html(userNameTableCellHTML);
}

function clearDate(datepickerId) {
    $('#' + datepickerId).datepicker('setDate', null);
}

function loadParams(userName, refreshThePage, alwaysFullRefresh) {
   
    var jsonObj =
           { op: 'loadParams', 'userName': userName };

    $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (data) {
            //console.log(data);
            data_color_red_gt_or_eq = data[0]['prm_value'];
            data_color_red_lt_or_eq = data[1]['prm_value'];
            data_color_orangeRed_gt_or_eq = data[2]['prm_value'];
            data_color_orangeRed_lt_or_eq = data[3]['prm_value'];
            data_color_orange_gt_or_eq = data[4]['prm_value'];
            data_color_orange_lt_or_eq = data[5]['prm_value'];
            data_color_green_gt_or_eq = data[6]['prm_value'];
            data_color_green_lt_or_eq = data[7]['prm_value'];
            data_refreshTimeoutInMS = data[8]['prm_value'];
            data_undoCheckoutTimeoutInMS = data[9]['prm_value'];
            if (refreshThePage) {
                doRefresh(true, data_refreshTimeoutInMS, alwaysFullRefresh) //callback success
            }
        }
    });


}

function loadDistrictsDT() {
    var jsonObj =
    {
        op: 'loadDistricts'

    };

    $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (data) {
            if (data[0] != null) {
                curDistrictsDT = data;
                searchDistricts = setInitialSerachValue(curDistrictsDT);
            }
        }
    });
}

function setInitialSerachValue(valuesDT) {
    var res = '';
    for (var i = 0; i < valuesDT.length - 1; i++) {
        res += valuesDT[i].Id + ',';
    }
    res += valuesDT[valuesDT.length - 1].Id;
    return res;
}

function returnToHomePage(returnToHomePageURL) {
    window.location = returnToHomePageURL;
}

function loadNafotDT() {

    var jsonObj =
    {
        op: 'loadNafot'

    };
    $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (data) {
            if (data[0] != null) {
                curNafotDT = data;
                searchNafot = setInitialSerachValue(curNafotDT);
            }
        }
    });
}


function loadRashuyotDT() {
    var jsonObj =
    {
        op: 'loadRashuyot'

    };

    return $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (data) {
            if (data[0] != null) {
                curRashuyotDT = data;
            }
        }
    });
}



 function loadItgonenutRegionsDT() {
    var jsonObj =
    {
        op: 'loadItgonenutRegions'

    };

    return $.ajax({
        type: "POST",
        url: "../Handler.ashx",
        data: jsonObj,
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        },
        success: function (data) {
            if (data[0] != null) {
                curItgonenutRegionsDT = data;
                searchItgonenutRegions = setInitialSerachValue(curItgonenutRegionsDT);
            }
        }
    });
}

function loadNafot(data, selectedIds, allSelected, includeZeroValue) {

    var cbo = $("<select id='SELECT_" + "Nafot" + "'  style='max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; vertical-align:bottom; height:100px;overflow-y: scroll; width:180px;' multiple></select>");
    var intArrSelectedIds = selectedIds != '' ? selectedIds.split(',').map(Number) : null;
    var intObjectID;
    $(data).each(function () {
        intObjectID = parseInt(this.Id);
        setComboInnerOptions(cbo, intObjectID, this.NafaName, intArrSelectedIds, allSelected);

    });
    if (includeZeroValue) {
        intObjectID = 0;
        setComboInnerOptions(cbo, intObjectID, 'ללא מידע', intArrSelectedIds, allSelected);
    }
    $('#SPN_Nafot').append(cbo);
}

function loadDistricts(data, selectedIds, allSelected, includeZeroValue) {
    var cbo = $("<select id='SELECT_" + "Districts" + "'  style='max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; vertical-align:bottom; height:100px;overflow-y: scroll; width:180px;' multiple></select>");
    var intArrSelectedIds = selectedIds != '' ? selectedIds.split(',').map(Number) : null;
    var intObjectID;
    $(data).each(function () {
        intObjectID = parseInt(this.Id);
        setComboInnerOptions(cbo, intObjectID, this.DistrictName, intArrSelectedIds, allSelected);
    });
    if (includeZeroValue) {
        intObjectID = 0;
        setComboInnerOptions(cbo, intObjectID, 'ללא מידע', intArrSelectedIds, allSelected);
    }
    $('#SPN_Districts').append(cbo);
}

function loadItgonenutRegions(data, selectedIds, allSelected, includeZeroValue) {
    var cbo = $("<select id='SELECT_" + "ItgonenutRegions" + "'  style='max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; vertical-align:bottom; height:100px;overflow-y: scroll; width:180px;' multiple></select>");
    var intArrSelectedIds = selectedIds != '' ? selectedIds.split(',').map(Number) : null;
    var intObjectID;
    $(data).each(function () {
        intObjectID = parseInt(this.Id);
        setComboInnerOptions(cbo, intObjectID, this.Name, intArrSelectedIds, allSelected);
    });
    if (includeZeroValue) {
        intObjectID = 0;
        setComboInnerOptions(cbo, intObjectID, 'ללא מידע', intArrSelectedIds, allSelected);
    }
    $('#SPN_ItgonenutRegions').append(cbo);
}


function setComboInnerOptions(cbo, value, text, intArrSelectedIds, allSelected) {
    if (allSelected == 'True' || (intArrSelectedIds != null && intArrSelectedIds.includes(value))) {
        setComboOption(cbo, value, text, true);
    }
    else {
        setComboOption(cbo, value, text, false);
    }
}

function setComboOption(cbo, value, text, isSelected) {
    if (isSelected) {
        cbo.append($("<option selected='selected'>").attr('value', value).text(text));
    }
    else {
        cbo.append($("<option>").attr('value', value).text(text));
    }
}

function getIdsFromDDL(selectDDLId, onlySelected, returnVal) {
    var res = '';
    if (onlySelected) {
        $("#" + selectDDLId + " option:selected").each(function () {
            returnVal == 'val' ? res += $(this).val() + "," : res += $(this).text() + ",";
        });
    } else {
        $("#" + selectDDLId + " option").each(function () {
            returnVal == 'val' ? res += $(this).val() + "," : res += $(this).text() + ",";
        });
    }
    if (res != null && res != '') {
        res = res.substring(0, res.length - 1);
    }
    return res;
}