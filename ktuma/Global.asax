<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        Application["dsMdTables"] = PikudBL.clsUtil.getMdTables();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        Exception lastError = Server.GetLastError();

        //write to log
        string strErr = "\n\n" + "exception on global.asax, lastError= \n\n" +
                lastError.Message + "\n stack=" + lastError.StackTrace + "\n";
        if (lastError.InnerException != null)
        {
            strErr += "innerException=" + lastError.InnerException;
        }
        pikudGui.BasePage.writeLog(strErr, true);

        if (lastError is HttpRequestValidationException)
        {
            Context.ClearError();
            Response.Redirect("~/CustomErrors/RequestValidationError.htm", true);
        }
        else if (lastError.InnerException != null &&
            lastError.InnerException.Message == "Maximum request length exceeded.")
        {
            Context.ClearError();
            Response.Write(strErr);
            //Response.End();
            Response.Redirect("~/CustomErrors/RequestValidationError.htm", true);
        }
        else
        {
            Context.ClearError();
            Response.Write(strErr);
            Response.End();
            //Response.Redirect("~/CustomErrors/ServerError.html", true);
        }
    }

    void Session_Start(object sender, EventArgs e) 
    {
        Session["userID"] = null;
        Session["userType"] = null;
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
