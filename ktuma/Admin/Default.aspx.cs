﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PikudBL;
using pikudEntities;


namespace pikudGui
{
    public partial class _Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.DefaultButton = this.BTN_Login.UniqueID;
            BTN_Login.Text = "התחבר";
            BTN_Exits.Text = "יציאה";
            BTN_Exits.Visible = false;
            BTN_Login.Click += BTN_Login_Click;

            //            string strFullUser = Request.LogonUserIdentity.Name;
            ////            strMachine = "\n" + "machine=" + Request.UserHostAddress;
            //            if (strFullUser.IndexOf('\\') == -1)
            //            {
            //                return;
            //            }
            //            string strUser = strFullUser.Split('\\')[1];
            //            string strInParamsNames = "userId";
            //            string strInParamsValues = strUser;
            //            string[] arrInParamsNames = strInParamsNames.Split(arrDelimiter, StringSplitOptions.None);
            //            string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.None);
            //            Response.Redirect("~/GalAdom.aspx");
            //DataTable dt = clsUtil.FillDt("authUser", arrInParamsNames, arrInParamsValues);

            //if (dt == null || dt.Rows.Count == 0)
            //{
            //    Session["userID"] = "guest";
            //    Session["user_DistId"] = "9999";
            //    Session["user_respId"] = "9999";
            //    Session["user_role"] = "guest";
            //    Response.Redirect("tichnon.aspx");
            //}
            //else
            //{
            //    Session["userID"] = dt.Rows[0]["user_ID"];
            //    Session["user_DistId"] = dt.Rows[0]["user_DistId"];
            //    Session["user_respId"] = dt.Rows[0]["user_respId"];
            //    Session["user_role"] = dt.Rows[0]["user_role"];
            //    Response.Redirect("~pages/HomePage.aspx");
            //}
        }

        void BTN_Login_Click(object sender, EventArgs e)
        {
            UserBL ubl = new UserBL();
            LBL_Error.Visible = false;
            string uName = TXT_UserName.Text;
            string pass = TXT_ToPassword.Text;
            string strInParamsNames = "username,password";
            string strInParamsValues = uName + "," + pass;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            KtumaUser ktUser = ubl.GetUserData(arrInParamsNames, arrInParamsValues);
            if (ktUser != null)
            {
                Session["User"] = ktUser;
                Session["Sviva"] = ktUser.Sviva;
                //setSessionUserId();
                Response.Redirect("~/admin/users.aspx");
            }
            else
            {
                LBL_Error.Visible = true;
                Session["Sviva"] = null;
                Session["User"] = null;

            }
        }

        
    }
}