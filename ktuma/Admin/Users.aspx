﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingAdmin.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Admin_Users" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <fieldset id="FLD_users" style =" direction:rtl;">
        <legend>ניהול משתמשים</legend>   
         <input type="button" id="INP_AddUser"
             value="הזן משתמש" onclick="EditInsertModal(true, -1)" class="btnEditWidthKtuma" style="margin-bottom:10px;" />
        <input type="text" id="INP_SearchUser" style="margin-bottom:10px;" />
        <span class="ui-icon ui-icon-search" id="BTN_SearchUser" onclick="SearchUser()" style="cursor:pointer;"></span>
         <table style="width: 100%;" class="tblRes" id="tblData">
                    <tr class="TBLHeader">
                        <th class="TBLHeader">מזהה משתמש</th>
                        <th class="TBLHeader">שם משתמש</th>
                        <th class="TBLHeader">סיסמא</th>
                        <th class="TBLHeader">סוג יוזר</th>
                        <th class="TBLHeader">משתמש מעדכן</th>
                        <th class="TBLHeader">תאריך עדכון</th>
                        <th class="TBLHeader">פעיל</th>
                        <th class="TBLHeader"></th>
                    </tr>
             </table>
   
        <div id="dialog-modal" style="display:none">
            <table class="innerTable" id="tblDataDialog">
            </table>
        </div>
    </fieldset>
    <script type="text/javascript">
        var userTypesDT;
        $(document).ready(function () {
            loadUsers();
            loadUserTypes();

        })

            function loadUsers() {
                var pageName = 'Users';
                var jsonObj =
                        { page: pageName, op: 'loadUsers', 'searchName': null };

                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);

                        fillTable(data);
                    }
                });
            }

            function fillTable(data) {
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.List.length; i++)//
                {
                    var tr = buildUserTR(data.List[i]);
                    $("#tblData").append(tr);
                }
            }



            function SearchUser() {
                var searchName = $('#INP_SearchUser').val();
                {
                    if (searchName == null || searchName.trim() == '') {
                        alert('הזן שם משתמש');
                    }
                    else {
                        var pageName = 'Users';
                        var jsonObj =
                                { page: pageName, op: 'loadUsers', 'searchName': searchName };

                        $.ajax({
                            type: "POST",
                            url: "../Handler.ashx",
                            data: jsonObj,
                            dataType: "json",
                            failure: function (response) {
                                alert(response.d);
                            },
                            success: function (data) {
                                //console.log(data);

                                fillTable(data);
                            }
                        });
                    }
                }
            }

            function loadUserTypes() {
                var userName = '<%=Session["UserName"]%>';
                var pageName = 'Users';
                var jsonObj =
                        { page: pageName, op: 'loadUserTypes', 'userName': userName };

                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);

                        userTypesDT = data;
                    }
                });


            }



            function EditInsertModal(show, obj) {
                var userIdToEditId;
                if (obj != -1) {
                    var objId = obj.attr('id')
                    userIdToEditId = objId.replace('INP_EditUser_', '');
                }
                else {
                    userIdToEditId = -1;
                }
                if (show) {
                    $("#dialog-modal").dialog({
                        height: 250,
                        width: 350,
                        closeOnEscape: false,
                        modal: true,
                        title: userIdToEditId == -1 ? "הזנת משתמש חדש" : "עדכון משתמש מספר " + userIdToEditId,
                        open: function (event, ui) {
                            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                        },
                        buttons: [
                                            {
                                                id: "addRowConfirm",
                                                text: "אשר",
                                                click: function () {
                                                    saveUser(userIdToEditId);

                                                    $(this).dialog("close");
                                                    resetButtons();

                                                }
                                            },
                                              {
                                                  id: "addRowCancel",
                                                  text: "בטל",
                                                  click: function () {
                                                      $(this).dialog("close");
                                                      resetButtons();

                                                  }
                                              }
                        ]
                    })
                    buildEditTable(userIdToEditId);


                    //disable
                    $(".btnEditKtumaSemi").attr('class', 'btnEditDisabledKtumaSemi').attr('disabled', 'disabled');
                    $(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');


                }
                else {

                }
            }

            function resetButtons() {
                //reset
                $(".btnEditDisabledKtumaSemi").attr('class', 'btnEditKtumaSemi').removeAttr('disabled');
                $(".btnEditWidthKtumaDisabled").attr('class', 'btnEditWidthKtuma').removeAttr('disabled');
            }

            function buildEditTable(userId) {
                tblData = $('#tblDataDialog');
                tblData.empty();

                var currentRow = $('#TR_User_' + userId);
                var userName = currentRow.find('td > span[id*=lblUserName]').text();
                var userPassword = currentRow.find('td > span[id*=lblPassword]').text();
                var userType = currentRow.find('td > span[id*=lblUserType]').text();

                trFirstRow = $('<tr class="dialogTR">');

                var tdNameLabel = $('<td class="searchTdCenterBold">שם משתמש</td>');
                var inputName = $('<input type="textbox" id="INP_UserName_Edit" style=width:150px;">')
                inputName.val(userName);
                var tdNameInp = $('<td class="searchTdCenterBold">');
                tdNameInp.append(inputName);

                trSecondRow = $('<tr class="dialogTR">');
                var tdPasswordLabel = $('<td class="searchTdCenterBold">סיסמא</td>');
                var inputPassword = $('<input type="textbox" id="INP_Password_Edit" style=width:150px;">')
                inputPassword.val(userPassword);
                var tdPasswordInp = $('<td class="searchTdCenterBold">');
                tdPasswordInp.append(inputPassword);

                trThirdRow = $('<tr class="dialogTR">');
                var tdTypeDescriptionLabel = $('<td class="searchTdCenterBold">סוג משתמש</td>');
                var tdTypeDescriptionDDL = $('<td class="searchTdCenterBold">');
                cboUserTypes = createUserTypesDDL(userTypesDT, tdTypeDescriptionDDL, userType, userId);
                trFirstRow.append(tdNameLabel).append(tdNameInp);
                trSecondRow.append(tdPasswordLabel).append(tdPasswordInp);
                trThirdRow.append(tdTypeDescriptionLabel).append(tdTypeDescriptionDDL);
                tblData.append(trFirstRow).append(trSecondRow).append(trThirdRow);
                setDialogStyle('addRowConfirm', 'addRowCancel');

            }


            function createUserTypesDDL(userTypesDT, td, selectedText, userId) {
                cbo = $("<select id='SELECT_UserTypeEdit_" + userId + "' style='max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;width:100%;'></select>");
                $(userTypesDT).each(function () {
                    if (selectedText == this.TypeDescription) {
                        cbo.append($("<option selected >").attr('value', this.UserTypeId).text(this.TypeDescription));
                    }
                    else {
                        cbo.append($("<option>").attr('value', this.UserTypeId).text(this.TypeDescription));
                    }
                });
                td.append(cbo);
            }



            function saveUser(userId) {
                var uName = $('#INP_UserName_Edit').val();
                var uPassword = $('#INP_Password_Edit').val();
                var uType = $('select[id*=SELECT_UserTypeEdit_]').val();
                var pageName = 'Users';
                var jsonObj =
                        { page: pageName, op: 'saveUser', 'uName': uName, 'uPassword': uPassword, 'uType': uType, 'userId': userId };

                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        var res = data.ActionResult;
                        if (res == 1) {
                            alert('המשתמש הוזן בהצלחה')
                            addUserToGrid(data);
                        }
                        else if (res == 2) {
                            alert('המשתמש עודכן בהצלחה')
                            updateUserInGrid(data)
                        }
                        else if (res == -1) {
                            alert('המשתמש לא מוכר במערכת')
                        }
                        else {
                            alert('ארעה שגיאה, נא לפנות למהל המערכת');
                        }



                    }
                });
            }

            function buildUserTR(row) {
                var LastUpdatedByUserName;
                var LastUpdated;
                var tdLastUpdatedUserName;
                var tdLastUpdatedDate;
                if (row.LastUpdatedByUser == null) {
                    LastUpdatedByUserName = '';


                }
                else {
                    LastUpdatedByUserName = row.LastUpdatedByUser.UserName;
                }
                tdLastUpdatedUserName = $('<td><span id="lblLastUpdatedUserName_' + row.UserId + '">' + LastUpdatedByUserName + '</span></td> ');
                if (row.LastUpdated == null) {
                    tdLastUpdatedDate = $('<td><span id="lblLastUpdatedDate_' + row.UserId + '"></span>' + '' + '</td> ');
                }
                else {
                    tdLastUpdatedDate = $('<td><span id="lblLastUpdatedDate_' + row.UserId + '">' + displayTime(row.LastUpdated) + '</span></td> ');
                }
                var tr = $('<tr id="TR_User_' + row.UserId + '"></tr>');
                var tdUserId = $('<td><span id="lblUserId_' + row.UserId + '">' + row.UserId + '</span></td> ');
                var tdUserName = $('<td><span id="lblUserName_' + row.UserId + '">' + row.UserName + '</span></td> ');
                var tdPassword = $('<td><span id="lblPassword_' + row.UserId + '">' + row.Password + '</span></td> ');
                var tdUserTypeDescription = $('<td><span id="lblUserType_' + row.UserId + '">' + row.KtumaUserType.TypeDescription + '</span></td> ');

                tr.append(tdUserId).append(tdUserName).append(tdPassword).append(tdUserTypeDescription).append(tdLastUpdatedUserName).append(tdLastUpdatedDate);;
                var tdButtons = $('<td style="text-align:center;" id="TD_Buttons_' + row.UserId + '"></td> ');

                if (row.IsActive == true) {
                    var tdIsActive = $('<td style="text-align:center;"><span id="SPN_IsActive_' + row.UserId + '" class="ui-icon  ui-icon-check"></span></td> ');
                    var inpEdit = '<input type="button" id="INP_EditUser_' + row.UserId + '" value="ערוך" onclick="EditInsertModal(true, $(this))" class="btnEditKtumaSemi" />'
                    var inpDel = '<input type="button" id="INP_DeleteUser_' + row.UserId + '"  value="מחק" onclick="deleteUser($(this))"  class="btnEditKtumaSemi" />'
                }
                else {
                    var tdIsActive = $('<td style="text-align:center;"></td> ');
                    var inpEdit = '<input style="display:none;" type="button" id="INP_EditUser_' + row.UserId + '" value="ערוך" onclick="EditInsertModal(true, $(this))" class="btnEditKtumaSemi" />'
                    var inpDel = '<input style="display:none;" type="button" id="INP_DeleteUser_' + row.UserId + '"  value="מחק" onclick="deleteUser($(this))"  class="btnEditKtumaSemi" />'
                }
                tdButtons.append(inpEdit).append(inpDel);
                tr.append(tdIsActive);

                tr.append(tdButtons);
                return tr;
            }

            function addUserToGrid(data) {
                tr = buildUserTR(data.List[0]);
                $('#tblData').append(tr);
            }

            function updateUserInGrid(row) {
                var currentRow = $('#TR_User_' + row.List[0].UserId);
                currentRow.find('td > span[id*=lblUserName]').text(row.List[0].UserName);
                currentRow.find('td > span[id*=lblPassword]').text(row.List[0].Password);
                currentRow.find('td > span[id*=lblUserType]').text(row.List[0].KtumaUserType.TypeDescription);
                currentRow.find('span[id*=lblLastUpdatedUserName]').text(row.List[0].LastUpdatedByUser.UserName);
                currentRow.find('td > span[id*=lblLastUpdatedDate]').text(displayTime(row.List[0].LastUpdated));
            }

            function deleteUser(obj) {
                var objId = obj.attr('id')

                var userIdToDeleteId = objId.replace('INP_DeleteUser_', '');
                if (confirm("למחוק את המשתמש מהמערכת?")) {
                    var pageName = 'Users';
                    var jsonObj =
                     { page: pageName, op: 'deleteUser', 'userIdToDeleteId': userIdToDeleteId };

                    $.ajax({
                        type: "POST",
                        url: "../Handler.ashx",
                        data: jsonObj,
                        dataType: "json",
                        failure: function (response) {
                            alert(response.d);
                        },
                        success: function (data) {
                            //console.log(data);
                            if (data == true) {
                                alert('המשתמש נמחק בהצלחה')
                                var currentRow = $('#TR_User_' + userIdToDeleteId);
                                var userImage = currentRow.find('td > span[id*=SPN_IsActive]').removeClass('ui-icon  ui-icon-check')
                                var actionButtons = currentRow.find('td[id*=TD_Buttons]').empty();

                            }
                            else {
                                alert('ארעה שגיאה, לא ניתן למחוק את המשתמש')
                            }

                        }
                    });
                }
                else {

                }
            }
    </script>
</asp:Content>

