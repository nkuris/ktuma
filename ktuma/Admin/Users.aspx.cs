﻿using PikudBL;
using pikudEntities;
using pikudGui;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Admin_Users : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
        //RPT_Users.ItemDataBound += RPT_Users_ItemDataBound;
        if(!IsPostBack)
        {
           // loadUsers();
        }
    }

    private void loadUsers()
    {
      
    }

    void RPT_Users_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblUserType = (Label)e.Item.FindControl("lblUserType");
            Label lblLastUpdatedUserName = (Label)e.Item.FindControl("lblLastUpdatedUserName");
            Label lblLastUpdatedDate = (Label)e.Item.FindControl("lblLastUpdatedDate");
            KtumaUser curUser = e.Item.DataItem as KtumaUser;
            if (curUser != null)
            {
                lblUserType.Text = curUser.KtumaUserType.TypeDescription;
                if (curUser.LastUpdatedByUser != null)
                {
                    lblLastUpdatedUserName.Text = curUser.LastUpdatedByUser.UserName;
                }
                if (curUser.LastUpdated.HasValue)
                {
                    lblLastUpdatedDate.Text = curUser.LastUpdated.Value.ToString("dd/MM/yyyy HH:mm:ss");
                }
                HtmlGenericControl SPN_IsActive = (HtmlGenericControl)e.Item.FindControl("SPN_IsActive");
                if (curUser.IsActive)
                {
                    SPN_IsActive.Attributes["class"] = "ui-icon  ui-icon-check";
                }
                else
                {
                    HtmlControl TD_Buttons = e.Item.FindControl("TD_Buttons") as HtmlControl;
                    foreach (Control x in TD_Buttons.Controls)
                    {
                        x.Visible = false;
                    }

                    //tc.Controls = null;
                }
            }



        }
    }


   
    protected void OnEdit(object sender, EventArgs e)
    {
        //Find the reference of the Repeater Item.
        RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        this.ToggleElements(item, true);
    }

    private void ToggleElements(RepeaterItem item, bool isEdit)
    {
        //Toggle Buttons.
        item.FindControl("lnkEdit").Visible = !isEdit;
        item.FindControl("lnkUpdate").Visible = isEdit;
        item.FindControl("lnkCancel").Visible = isEdit;
        item.FindControl("lnkDelete").Visible = !isEdit;

        //Toggle Labels.
        item.FindControl("lblUserName").Visible = !isEdit;
        item.FindControl("lblPassword").Visible = !isEdit;

        //Toggle TextBoxes.
        item.FindControl("txtUserName").Visible = isEdit;
        item.FindControl("txtPassword").Visible = isEdit;
    }

 
 
    private List<KtumaUser> getUsersList()
    {
        UserBL ubl = new UserBL();
        return ubl.loadUsers();
    }
}