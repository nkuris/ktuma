﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingAdmin.Master"    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="pikudGui._Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div style="text-align:center;">
        <table style="margin: 0px auto; border:3px solid black">
            <tr>
            <td class="TDbold"> שם משתמש:</td>
            <td><asp:TextBox runat="server" ID="TXT_UserName" MaxLength="30" CssClass="inputReg"></asp:TextBox></td>
         </tr>
            <tr>         
          <td  class="TDbold">סיסמא: </td>
           <td><asp:TextBox runat="server" ID="TXT_ToPassword"  TextMode="Password" MaxLength="30" CssClass="inputReg"></asp:TextBox></td>
        </tr>
        </table>
            </div>
         <div style="text-align:center; margin:20px;">
           <asp:Button runat="server" ID="BTN_Login" style="width:10%;"  OnClientClick="return checkInput()" CssClass="btnEditKtuma" />
           <asp:Button runat="server" ID="BTN_Exits" style="width:10%;" OnClientClick="window.close();"  CssClass="btnEditKtuma" />
         </div>
    <div class="LBLLoginError">
        <asp:Label ID="LBL_Error" runat="server" Text ="שם משתמש או סיסמא שגויים" Visible="false"></asp:Label>
    </div>
    <script type="text/javascript">
      
        $(document).ready(function () {
            $('#menu-outer').remove();
            $('#DivFooter').remove();
        });
        function checkInput(txt_uname, txt_password)
        {
            
            if ($('input[id*="TXT_UserName"').val() == '') {
                alert("הכנס שם משתמש");
                return false;
            }
            if ($('input[id*="TXT_ToPassword"').val() == '') {
                alert("הכנס סיסמא")
                return false;
            }
            return true;
        }
    </script>
</asp:Content>

