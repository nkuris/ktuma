﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingAdmin.master" AutoEventWireup="true" CodeFile="Resources.aspx.cs" Inherits="Admin_Resources" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="text-align:center; margin-top:20px;">
         
        <table style =" width:800px;text-align:center; margin:auto;" class="tblRes">
                <tr>
                        <td class="TDboldNoBorder">
                            עמוד
                        </td>
                        <td class="TDboldNoBorder">
                           <select  id="DDL_Screen" onchange="findScreenCombo()" style="width:200px;"></select>
                        </td>
                 </tr>
            <tr>
                        <td class="TDboldNoBorder">
                            תאור הקומבו
                        </td>
                        <td class="TDboldNoBorder">
                            <select id="DDL_PageCombo" style="width:200px;"  onchange="loadComboResources()"></select>
                        </td>
                 </tr>
            <tr>                 
         </table>
        <div style="width:800px; margin:auto;text-align:right">
        <input type="button" id="INP_AddResource"
             value="הזן תוכן" onclick="EditInsertModal(true, -1)" class="btnEditWidthKtuma" style="margin-bottom:10px;display:none;" />
            </div>
         <table style =" width:800px;text-align:center; margin:auto; display:none;" class="tblRes" id="TBL_resources">
             <tr>
                 <th style="width:10px;text-align:center; display:none;">
                     מיקום ברשימה
                 </th>
                 <th style="text-align:center;">
                     טקסט
                 </th>
                 <th style="width:250px;text-align:center;"></th>
             </tr>
             </table>
        </div>
        <div id="dialog-modal" style="display:none">
            <table class="innerTable" id="tblDataDialog">
            </table>
        </div>
    <script type="text/javascript">
        var curScreens;
        var curCombos;
        var curResources;
        const IMG_Down = 'IMG_Down_';
        const IMG_Up = 'IMG_Up_';        
        var selectedComboId;
        $(document).ready(function () {
            loadPageData();
            $('body').on('click', '.down_button', function () {                
                updateComboInternalId($(this), false);

            });
            $('body').on('click', '.up_button', function () {                
                updateComboInternalId($(this), true);
            });
            

        })

        function updateComboInternalId(obj, Up) {
            var ResourceId;
            var rowToMove;
            var next = {};
            var prev = {};
            rowToMove = obj.parents('tr.MoveableRow:first');
            if(Up == true)
            {
                ResourceId = $(obj).attr('id').replace(IMG_Up, '')
                prev = rowToMove.prev('tr.MoveableRow')
            }
            else
            {
                ResourceId = $(obj).attr('id').replace(IMG_Down, '')
                next = rowToMove.next('tr.MoveableRow')
            }
            
            if ((next.length == 1) || (prev.length == 1)) {
                var pageName = 'Resources';
                var jsonObj =
                        { page: pageName, op: 'UpdateComboIndex', 'ResourceId': ResourceId, 'Up': Up };

                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function () {
                        if (Up == false) {
                            if (next.length == 1) { next.after(rowToMove.fadeIn()); }
                        }
                        else {
                            if (prev.length == 1) { prev.before(rowToMove.fadeIn()); }
                        }
                    }
                });
            }
            else
            {                
            }
        }

        function buildResourceTR(row)
        {
            text = row.ResourceText;
            comboIndex = row.ComboInternalId;
            var tr = $('<tr id="TR_Resource_' + row.ResourceId + '" class="MoveableRow" style="vertical-align:middle;">');
            var tdText = $('<td id="TD_ResourceText_' + row.ResourceId + '">' + text + '</td>');
            var tdComboId = $('<td style="text-align:center;display:none;" id="TD_comboIndex_' + row.ResourceId + '">' + comboIndex + '</td>');
            var imgDown = $('<img src="../Images/icons8-down-arrow-filled-50.png" class="down_button" style="vertical-align:middle;" />');
            imgDown.attr('id', IMG_Down + row.ResourceId);
            var imgUp = $('<img src="../Images/icons8-up-filled-50.png" class="up_button"  style="vertical-align:middle;"/>');
            imgUp.attr('id', IMG_Up + row.ResourceId);
            var tdArrows = $('<td style="text-align:center;vertical-align:middle;"> </td>')

            var inpEdit = $('<input  style="vertical-align:middle;"  type="button" id="INP_EditResource_' + row.ResourceId + '" value="ערוך" onclick="EditInsertModal(true, $(this))" class="btnEditKtumaSmall" />');
            var inpDel = $('<input  style="vertical-align:middle;"  type="button" id="INP_DeleteResource_' + row.ResourceId + '"  value="מחק" onclick="deleteResource($(this))"  class="btnEditKtumaSmall" />');
            tdArrows.append(imgDown).append(imgUp).append(inpEdit).append(inpDel);
            //tdArrows.append(inpEdit).append(inpDel);
            tr.append(tdComboId).append(tdText).append(tdArrows);                
            return tr;
        }

        function deleteResource(obj) {
            var objId = obj.attr('id')

            var resourceIdToDeleteId = objId.replace('INP_DeleteResource_', '');
            if (confirm("למחוק את התוכן מהמערכת?")) {
                var pageName = 'Resources';
                var jsonObj =
                 { page: pageName, op: 'deleteResource', 'resourceIdToDeleteId': resourceIdToDeleteId };

                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data == true) {
                            alert('התוכן נמחק בהצלחה')
                            var currentRow = $('#TR_Resource_' + resourceIdToDeleteId);
                            currentRow.remove();
                        }
                        else {
                            alert('ארעה שגיאה, לא ניתן למחוק את התוכן')
                        }

                    }
                });
            }
            else {

            }
        }

        function  loadPageData()
        {
            var pageName = 'Resources';
            var jsonObj =
                    { page: pageName, op: 'loadScreens' };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    curScreens = data[0];
                    curCombos = data[1];
                    curResources = data[2];
                    fillScreens(curScreens);
                }
            });
        }

        function fillScreens(data) {
            var DDL_Screen = $('#DDL_Screen');
            DDL_Screen.append('<option value="' + '0' + '">' + 'בחר עמוד' + '</option>');
            $(curScreens).each(function () {
                DDL_Screen.append('<option value="' + (this).ScreenName + '">' + (this).ScreenNameHeb + '</option>');
            });
        }

        function findScreenCombo() {
            
            var cbo = $('#DDL_PageCombo');
            cbo.empty();
            cbo.append('<option value="' + '0' + '">' + 'בחר רשימת גלילה' + '</option>');
            filterField = $('#DDL_Screen option:selected').val();            
            filteredDataTable = jQuery.grep(curCombos, function (n, i) {
                return n['ComboScreen'] == filterField;
            });
            if (filterField != null && filterField != 0) {
                $(filteredDataTable).each(function () {
                    text = this.ComboDescription;
                    val = this.ComboId;
                    cbo.append($("<option>").attr('value', val).text(text));
                });
            }
            else
            {
                if ($('#INP_AddResource').css('display') == 'inline-block') {
                    $('#INP_AddResource').fadeOut()
                    $('#TBL_resources').fadeOut();
                    selectedComboId = 0;
                    $('#DDL_PageCombo').val(selectedComboId);
                }
            }
        }
        function loadComboResources()
        {
            $("#TBL_resources").find("tr:gt(0)").remove();            
            filterField = $('#DDL_PageCombo option:selected').val();
            selectedComboId = filterField;
            if (selectedComboId != null && selectedComboId != 0) {
                $('#INP_AddResource').fadeIn()
                $('#TBL_resources').fadeIn();
            }
            else {
                $('#INP_AddResource').fadeOut()
                $('#TBL_resources').fadeOut();
            }
            filteredDataTable = jQuery.grep(curResources, function (n, i) {
                return n['ComboId'] == filterField;
            });
            
            $(filteredDataTable).each(function () {
                var tr = buildResourceTR(this);
                $('#TBL_resources').append(tr);
            });
            

        }

        function EditInsertModal(show, obj) {
            var resourceIdToEditId;
            if (obj != -1) {
                var objId = obj.attr('id')
                resourceIdToEditId = objId.replace('INP_EditResource_', '');
            }
            else {
                resourceIdToEditId = -1;
            }
            if (show) {
                $("#dialog-modal").dialog({
                    height: 250,
                    width: 350,
                    closeOnEscape: false,
                    modal: true,
                    title: resourceIdToEditId == -1 ? "הזנת תוכן חדש" : "עדכון תוכן מספר " + resourceIdToEditId,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    },
                    buttons: [
                                        {
                                            id: "addRowConfirm",
                                            text: "אשר",
                                            click: function () {
                                                saveResource(resourceIdToEditId);

                                                $(this).dialog("close");
                                                resetButtons();

                                            }
                                        },
                                          {
                                              id: "addRowCancel",
                                              text: "בטל",
                                              click: function () {
                                                  $(this).dialog("close");
                                                  resetButtons();

                                              }
                                          }
                    ]
                })
                buildEditTable(resourceIdToEditId);


                //disable
                $(".btnEditKtumaSmall").attr('class', 'btnEditDisabledKtumaSmall').attr('disabled', 'disabled');
                //$(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');


            }
            else {

            }
        }

        function saveResource(resourceId) {
            var resourceText = $('#INP_Resource_Edit').val();
            var comboId = selectedComboId;
            var pageName = 'Resources';            
            var jsonObj =
                    { page: pageName, op: 'saveResource', 'resourceText': resourceText, 'resourceId': resourceId, 'comboId': comboId };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    var res = data[0].actionResult;
                    if (res == 1) {
                        alert('התוכן הוזן בהצלחה')
                        addResourceToGrid(data[0]);
                    }
                    else if (res == 2) {
                        alert('התוכן עודכן בהצלחה')
                        updateResourceInGrid(data[0])
                    }
                    else if (res == -1) {
                        alert('התוכן לא מוכר במערכת')
                    }
                    else {
                        alert('ארעה שגיאה, נא לפנות למהל המערכת');
                    }



                }
            });
        }

        function addResourceToGrid(data) {
            tr = buildResourceTR(data);
            $('#TBL_resources').append(tr);
        }
        
        function updateResourceInGrid(row)
        {            
            $('#TD_ResourceText_' +row.ResourceId).text(row.ResourceText);
        }

        function resetButtons() {
            //reset
            //$(".btnEditDisabledKtumaSemi").attr('class', 'btnEditKtumaSemi').removeAttr('disabled');
            $(".btnEditDisabledKtumaSmall").attr('class', 'btnEditKtumaSmall').removeAttr('disabled');
        }

        function buildEditTable(resourceId) {
            tblData = $('#tblDataDialog');
            tblData.empty();
            
            var resourceText = $('#TD_ResourceText_' + resourceId).text();
            var inputResource = $('<textarea rows="5"   id="INP_Resource_Edit"' + '" maxlength="250" style="width:200px;"   ></textarea>');            
            inputResource.val(resourceText);
            var tdLabel = $('<td class="searchTdCenterBold">טקסט</td>');
            var tdText = $('<td class="searchTdCenterBold"></td>');
            tdText.append(inputResource);
            trFirstRow = $('<tr class="dialogTR">');
            trFirstRow.append(tdLabel).append(tdText);
            tblData.append(trFirstRow);
            setDialogStyle('addRowConfirm', 'addRowCancel');

        }


       
    </script>
</asp:Content>

