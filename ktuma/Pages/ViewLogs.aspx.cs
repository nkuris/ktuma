﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using pikudGui;



public partial class ViewLogs : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        checkSession();
        drpOrder.dropDown.Width = widthExtraWide * 3;
        drpOrder.LabelWidth = widthWide;
        drpOrder.inParamsValues = Session["stat"].ToString();
        drpOrder.dropDown.SelectedIndexChanged
                += new EventHandler(drpOrder_SelectedIndexChanged);
    }


    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        drpOrder.dropDown.SelectedIndex = 1;
    }


    protected void drpOrder_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkLog.Visible = false;
        lnkDownload.Visible = false;
        grid.Visible = false;
    }

    protected void btnDo_Click(object sender, EventArgs e)
    {
        grid.Visible = true;
        grid.noCommandField = true;
        grid.strInParamsNames = "taskOrder_id,taskType";
        grid.strInParamsValues = drpOrder.dropDown.SelectedValue + "," + Session["stat"].ToString();
        grid.strSpName = "getTaskOrdersAndLogs";
        grid.PageNum = 1;
        //grid.strSpName = "searchEvalCenterForEmployeeUpdate";
        grid.columnsTableNum = 15;  //visible colums
        grid.hiddenColumnsCount = 1;
        grid.blnPnlMainVisible = true;
        //grid.strInParamsValues = sb.ToString();
        grid.bind();
        string downloadFileName;

        if (grid.strField1 != "")
        {

            string exportFolder = getParamValueFromParamsTableByKey("ExportFileFolderRelative");
            downloadFileName = getFileNameFromGrid(grid.strField1);
            lnkDownload.HRef = Request.ApplicationPath +"/" + exportFolder +"/" + downloadFileName; //file name
            
            //lnkDownload.HRef = grid.strField1; //file name
            lnkDownload.Visible = true;
        }
       
    }

    private string getFileNameFromGrid(string strField1)
    {
        string[] fileSections = strField1.Split('\\');
        return fileSections[fileSections.Length - 1];
    }

    protected void btnGoToUpload_Click(object sender, EventArgs e)
    {
        Server.Transfer("Settings.aspx");
    }
}
