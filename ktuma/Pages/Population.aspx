﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Heading.Master" AutoEventWireup="true"
     CodeFile="Population.aspx.cs" Inherits="Pages_Population" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;

        var glbEditedLine_Recommendation_BeforeRefresh = {};
        var glbEditedLine_Explanation_BeforeRefresh= '';
        var glbEditedLine_Exception_Explanation_BeforeRefresh = ''
        var glbEditedLine_Exception_BeforeRefresh = {};

        $(document).ready(function () {
            loadCombosPopulation();
            var uid = ('<%=Session["userID"]%>');
            checkSession(uid);
            loadParams(uid, true, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });


        function loadCombosPopulation() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Population';
            var jsonObj =
                { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


        }

        function refresh(isFullRefresh) //
        {
           
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var jsonObj =
                    { op: 'loadPopulation', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data, isFullRefresh);

                }
            });
        }

        function fillTable(data, isFullRefresh) //
        {

           var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                            //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                            //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                            //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                        }
                        else {
                            setTDValTextAndColor($('#TD_Recommendation_' + data[i].Itgonenut_Id), data[i].RecommendationValue, data[i].RecommendationText, true);
                            $('#TD_Explanation_' + data[i].Itgonenut_Id).html(data[i].Explanation);
                            $('#TD_Exception_' + data[i].Itgonenut_Id).html(data[i].Exception);
                            $('#TD_Exception_Explanation_' + data[i].Itgonenut_Id).html(data[i].Exception_Explanation);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
                }
        }
        

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValuePop(data[i].RecommendationValue);            
            row += createOrigTD(bgClass, 'Recommendation', data[i].Itgonenut_Id, data[i].RecommendationText, data[i].RecommendationValue);
            if (data[i].Exception_Explanation == null || data[i].Exception_Explanation == undefined) {
                data[i].Exception_Explanation = '';
            }
            if (data[i].Exception == null || data[i].Exception == undefined) {
                data[i].Exception = '';
            }

            if (data[i].Explanation == null || data[i].Explanation == undefined) {
                data[i].Explanation = '';
            }
            if (data[i].Exceptions == null || data[i].Exceptions == undefined) {
                data[i].Exceptions = '';
            }
            else {
                data[i].Exceptions = data[i].Exceptions.replace('\\n', '<br>');
            }
            if (data[i].ExplanationToException == null || data[i].ExplanationToException == undefined) {
                data[i].ExplanationToException = '';
            }
            else {
                data[i].ExplanationToException = data[i].ExplanationToException.replace('\\n', '<br>');
            }
            
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explanation', data[i].Explanation);
            //row += createTDWithWrap(data[i].Itgonenut_Id, 'Exception', data[i].Exception);            
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Exception', data[i].Exception);            
            //row += createTDWithWrap(data[i].Itgonenut_Id, 'Exception_Explanation', data[i].Exception_Explanation);            
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Exception_Explanation', data[i].Exception_Explanation);            


            //var strClick = createSTRClick(data[i].Itgonenut_Id);
            //var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            //var disableAllEditButtons = '';
            //if (data[0].isUserEdited == 1)//
            //{
                //disableAllEditButtons = 'disabled="disabled"';
            //}

            //var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);
            //if (userTypeId != "-1") {
              //  if (data[i].user_name == null) // no user took this button yet, for reguler users
                //{
                    //row += strButton;
                //}
                //else if (data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                //{
                  //  strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    //strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    //row += strButton;
                    //first time
                    //if (timer_gear == -1)//
                    //{
                        //glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        //startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    //}
                //}
                //else //
                //{
                    //row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                //}
          //  }

          //  if (data[i].user_name == null)//
          //  {
          //      row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
          //  }
          //  else //
         //   {
        ////        row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
        //    }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
        //    if (data[i].user_name == '<%=Session["UserName"]%>') {
          //      createEditDDL(glbEditedLine_ItgonenutId);
//            }
        }

        function createEditDDL(ItgonenutId) {
            var tdRecommendation = $('#TD_Recommendation_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Recommendation_BeforeRefresh, tdRecommendation.html())
            glbEditedLine_Explanation_BeforeRefresh = $("#TD_Explanation_" + ItgonenutId).html();
            var tdExplanation = $("#TD_Explanation_" + ItgonenutId);
            glbEditedLine_Exception_Explanation_BeforeRefresh = $("#TD_Exception_Explanation_" + ItgonenutId).html();
            var tdExceptionExplanation = $("#TD_Exception_Explanation_" + ItgonenutId);
            glbEditedLine_Exception_BeforeRefresh = $("#TD_Exception_" + ItgonenutId).html();
            var tdException = $("#TD_Exception_" + ItgonenutId);
            tdRecommendation.empty();
            tdRecommendation.attr('class', 'tdEditClass');
            tdExceptionExplanation.empty();
            tdExceptionExplanation.attr('class', 'tdEditClass');
            tdException.empty();
            tdException.attr('class', 'tdEditClass');
            tdExplanation.empty();
            tdExplanation.attr('class', 'tdEditClass');
            cboRecommendation = createCombo(curCombosDT, 'Recommendation', tdRecommendation, ItgonenutId, false, glbEditedLine_Recommendation_BeforeRefresh.val, true);
            createTextArea(tdException, 'Exception', ItgonenutId, glbEditedLine_Exception_BeforeRefresh, 220);
            createTextArea(tdExplanation, 'Explanation', ItgonenutId, glbEditedLine_Explanation_BeforeRefresh,200 );
            createTextArea(tdExceptionExplanation, 'Exception_Explanation', ItgonenutId, glbEditedLine_Exception_Explanation_BeforeRefresh, 200);
        }

        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Population'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Explanation_BeforeRefresh, glbEditedLine_Exception_BeforeRefresh,
                glbEditedLine_Exception_Explanation_BeforeRefresh, glbEditedLine_Recommendation_BeforeRefresh, Itgonenut_Id);
        }


        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }


        function checkLower100(obj)//
        {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }


        function checkOutOrSave(ItgonenutId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = "Population";
            if ($("#btnEdit" + ItgonenutId).val()== 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                        }
                    }
                });
            }
            else//
            {
                var explanation = $("#INP_Explanation_" + ItgonenutId).val();
                var exception = $("#INP_Exception_" + ItgonenutId).val();
                var exception_explanation = $("#INP_Exception_Explanation_" + ItgonenutId).val();
                var recommendation = {}
                setValuesForUndoCheckoutFromTextOnly(recommendation, "Recommendation", ItgonenutId)
                var jsonObj =
                        {
                            page: pageName, "op": "save", "op": "save", "ItgonenutId": ItgonenutId,
                            "userId": '<%=Session["userID"]%>', "explanation": explanation,
                            "exception": exception, "exception_explanation": exception_explanation,
                            "recommendation": recommendation.val
                        };
                //alert(JSON.stringify(jsonObj));


                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
    
                            resetEdit(explanation, exception, exception_explanation, recommendation, ItgonenutId);

                            // enableDisbaleUserLine(userType, 'disable');
                            //$("#cph_region" + userType + "_EditedBy").val("");//after save clear my user from "active user cell"
                        }
                    }
                });
            }
        }



        function resetEdit(Explanation, exception, exception_explanation, recommendation, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            $('#TD_Explanation_' + glbEditedLine_ItgonenutId).html(Explanation);
            $('#TD_Exception_' + glbEditedLine_ItgonenutId).html(exception);
            $('#TD_Exception_Explanation_' + glbEditedLine_ItgonenutId).html(exception_explanation);
            setTDValTextAndColorObj($('#TD_Recommendation_' + glbEditedLine_ItgonenutId), recommendation, true);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>
      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/אוכלוסייה-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; אוכלוסייה</span>
       </div>
      <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 18%;" /><!-- 30%-->
                    <col style="width: 18%;" />
                    <col style="width: 18%;" /><!-- 66%-->

                    <col style="width: 18%;" />
                    <!--<col style="width: 8%;" />
                    <col style="width: 8%;" /> -->

                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>                  
                    <th class="TBLHeader">הסבר להמלצה למדיניות
                    </th>
                    <th class="TBLHeader">יישובים מוחרגים
                    </>
                    <th class="TBLHeader">הסבר לצורך בהחרגה
                    <!--th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th> -->
                </tr>
            </table>
        </div>
    </div>
</asp:Content>