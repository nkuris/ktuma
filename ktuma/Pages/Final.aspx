﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master" CodeFile="Final.aspx.cs" Inherits="Pages_Final" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {
            loadParams('<%=Session["userID"]%>', true, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });

        function refresh(isFullRefresh) {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)
            var jsonObj =
                    { op: 'loadFinal_Grade', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data, isFullRefresh);

                }
            });
        }

        function fillTable(data, isFullRefresh) {

            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i)
                }
                setRangesCol('tblData');
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {


                        setTDValTextAndColor($('#TD_Gila_' + data[i].Itgonenut_Id), data[i].GilaValue, data[i].GilaText, false);
                        setTDValTextAndColor($('#TD_Hagna_' + data[i].Itgonenut_Id), data[i].HagnaValue, data[i].HagnaText, false);
                        setTDValTextAndColor($('#TD_Migun_' + data[i].Itgonenut_Id), data[i].MigunValue, data[i].MigunText, false);
                        setTDValTextAndColor($('#TD_Mod_' + data[i].Itgonenut_Id), data[i].ModValue, data[i].ModText, false);
                        setTDValTextAndColor($('#TD_Haatra_' + data[i].Itgonenut_Id), data[i].HaatraValue, data[i].HaatraText, false);
                        setTDValTextAndColor($('#TD_Population_' + data[i].Itgonenut_Id), data[i].PopulationValue, data[i].PopulationText, true);
                        setTDValTextAndColor($('#TD_FinalPopMod_' + data[i].Itgonenut_Id), data[i].FinalPopModValue, data[i].FinalPopModText, false);
                        setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText, false);

                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValue(data[i].ModValue);
            row += createOrigTD(bgClass, 'Mod', data[i].Itgonenut_Id, data[i].ModText, data[i].ModValue);
            bgClass = getClassByExactValue(data[i].HagnaValue);
            row += createOrigTD(bgClass, 'Hagna', data[i].Itgonenut_Id, data[i].HagnaText, data[i].HagnaValue);
            bgClass = getClassByExactValue(data[i].FinalPopModValue);
            row += createOrigTD(bgClass, 'FinalPopMod', data[i].Itgonenut_Id, data[i].FinalPopModText, data[i].FinalPopModValue);
            bgClass = getClassByExactValue(data[i].GilaValue);
            row += createOrigTD(bgClass, 'Gila', data[i].Itgonenut_Id, data[i].GilaText, data[i].GilaValue);
            bgClass = getClassByExactValue(data[i].HaatraValue);
            row += createOrigTD(bgClass, 'Haatra', data[i].Itgonenut_Id, data[i].HaatraText, data[i].HaatraValue);
            bgClass = getClassByExactValue(data[i].MigunValue);
            row += createOrigTD(bgClass, 'Migun', data[i].Itgonenut_Id, data[i].MigunText, data[i].MigunValue);

            bgClass = getClassByExactValuePop(data[i].PopulationValue);
            row += createOrigTD(bgClass, 'Population', data[i].Itgonenut_Id, data[i].PopulationText, data[i].PopulationValue);
 
            bgClass = getClassByExactValue(data[i].FinalGradeValue);
            row += createOrigTD(bgClass, 'FinalGrade', data[i].Itgonenut_Id, data[i].FinalGradeText, data[i].FinalGradeValue);

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
        }
        
        function createTdColoredData(dataRow, fieldId) {
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id']) {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }
        </script>
        <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/שקלול המרכיבים-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; שקלול המרכיבים</span>
          </div>
      <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" /><!-- 12%-->
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" /><!-- 54%-->
                    <col style="width: 10%;" />
                    <col style="width: 16%;" /><!-- 80%-->
                    <col style="width: 10%;" />
                    <col style="width: 10%;" />                 
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים</th>
                    <th class="TBLHeader">אזור התגוננות</th>
                     <th class="TBLHeader">הערכת מודיעין</th>
                    <th class="TBLHeader">הגנ"א </th>
                     <th class="TBLHeader">
                       הגנ"א+הערכת מודיעין
                    </th>
                 <th class="TBLHeader">גיל"ה והפצה</th>   
                          <th class="TBLHeader">הפרעה לשגרת החיים
                    </th>
               
                    <th class="TBLHeader">מיגון-כשירות
                    </th>

                     <th class="TBLHeader">אוכלוסייה - המלצת המחוזות והפקמ"רים
                    </th>
           
                    <th class="TBLHeader">
                     מדרג משוקלל
                    </th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
