﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master" CodeFile="Gila.aspx.cs" Inherits="Pages_Gila" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;

        var glbEditedLine_Amida_BeforeRefresh = {};
        var glbEditedLine_Gilui_BeforeRefresh = {};
        var glbEditedLine_Hatraa_BeforeRefresh = {};
        var glbEditedLine_Harigim_BeforeRefresh;
        var glbEditedLine_Explantion_BeforeRefresh;
        var glbEditedLine_Comander_Affect_BeforeRefresh = {};
        var glbEditedLine_FinalGrade_BeforeRefresh = {};

     $(document).ready(function ()//
     {
         loadCombosGila();
         var uid = '<%=Session["userID"]%>';
         checkSession(uid);
         loadParams(uid, true, false);
         loadAdminSviva('<%=Session["SvivaId"]%>');
     })
    
     function loadCombosGila() {
         var userName = '<%=Session["UserName"]%>';
         checkSession(userName)
         var pageName = 'Gila';
         var jsonObj =
                 { page: pageName, op: 'loadCombosByPage', 'userName': userName };

         $.ajax({
             type: "POST",
             url: "../Handler.ashx",
             data: jsonObj,
             dataType: "json",
             failure: function (response) {
                 alert(response.d);
             },
             success: function (data) {
                 //console.log(data);

                 curCombosDT = data;
             }
         });


     }

       
     function refresh(isFullRefresh) //
     {
       
         var userName = '<%=Session["UserName"]%>';
         checkSession(userName)
             var jsonObj =
                     { op: 'loadGila', 'userName': userName };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data, isFullRefresh, userName);
                    
                 }
             });
         }
        
        
        function fillTable(data, isFullRefresh, userName) //
        {
            var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           

            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
                
                
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                           
                        }
                        else {
                            setTDValTextAndColor($('#TD_Amida_' + data[i].Itgonenut_Id), data[i].AmidaValue, data[i].AmidaText);
                            setTDValTextAndColor($('#TD_Gilui_' + data[i].Itgonenut_Id), data[i].GiluiValue, data[i].GiluiText);
                            setTDValTextAndColor($('#TD_Hatraa_' + data[i].Itgonenut_Id), data[i].HatraaValue, data[i].HatraaText);
                            setTDValTextAndColor($('#TD_Comander_Affect_' + data[i].Itgonenut_Id), data[i].ComanderAffectValue, data[i].ComanderAffectText);
                            $('#TD_Explantion_' + data[i].Itgonenut_Id).html(data[i].Explantion);
                            $('#TD_Harigim_' + data[i].Itgonenut_Id).html(data[i].Harigim);
                            setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValue(data[i].AmidaValue);
            row+= createOrigTD(bgClass, 'Amida', data[i].Itgonenut_Id, data[i].AmidaText, data[i].AmidaValue);
            bgClass = getClassByExactValue(data[i].GiluiValue);            
            row += createOrigTD(bgClass, 'Gilui', data[i].Itgonenut_Id, data[i].GiluiText, data[i].GiluiValue);
            bgClass = getClassByExactValue(data[i].HatraaValue);
            row += createOrigTD(bgClass, 'Hatraa', data[i].Itgonenut_Id, data[i].HatraaText, data[i].HatraaValue);


            if (data[i].ComanderAffectText == null || data[i].ComanderAffectText == undefined) {
                data[i].ComanderAffectText = '';
            }
            if (data[i].ComanderAffectValue == null || data[i].ComanderAffectValue == undefined) {
                data[i].ComanderAffectValue = '';
            }

            if (data[i].Explantion == null || data[i].Explantion == undefined) {
                data[i].Explantion = '';
            }
            if (data[i].Harigim == null || data[i].Harigim == undefined) {
                data[i].Harigim = '';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Harigim', data[i].Harigim);            
            if (data[i].ComanderAffectValue == '') {
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" ></td>';
            }
            else {
                bgClass = getClassByExactValue(data[i].ComanderAffectValue);
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].ComanderAffectValue + ' - ' + data[i].ComanderAffectText + '</td>';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explantion', data[i].Explantion);
            bgClass = getClassByExactValue(data[i].FinalGradeValue);
            row += createOrigTD(bgClass, 'Final_Grade', data[i].Itgonenut_Id, data[i].FinalGradeText, data[i].FinalGradeValue);            


            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);
            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if (data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }


        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Gila'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Amida_BeforeRefresh, glbEditedLine_Gilui_BeforeRefresh,
                glbEditedLine_Hatraa_BeforeRefresh, glbEditedLine_Harigim_BeforeRefresh,
                 glbEditedLine_Comander_Affect_BeforeRefresh, glbEditedLine_Explantion_BeforeRefresh,
                 glbEditedLine_FinalGrade_BeforeRefresh, Itgonenut_Id);
        }
        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }


        function checkLower100(obj)//
        {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }


        function checkOutOrSave(ItgonenutId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)
            var pageName = "Gila";
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName,  op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                            
                        }
                    }
                });
            }
            else//
            {
                var amida = {};
                var gilui = {};
                var hatraa = {};
                var comander_Affect = {};
                var harigim = $("#INP_Harigim_" + ItgonenutId).val();
                var explantion = $("#INP_Explantion_" + ItgonenutId).val();

                setValuesForUndoCheckoutFromTextOnly(amida, "Amida", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(gilui, "Gilui", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(hatraa, "Hatraa", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(comander_Affect, "Comander_Affect", ItgonenutId);

                if ((comander_Affect.val != '0' && comander_Affect.val != '') && explantion == '')
                {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }
                var comander_AffectText = $("#SELECT_Comander_Affect_" + ItgonenutId + ' option:selected').text();

                var jsonObj =
                        { page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>',
                            "amida": amida.val, "gilui": gilui.val, "hatraa": hatraa.val, "harigim": harigim,
                            "explantion": explantion, "comander_Affect": comander_Affect.val };
         
                //console.log('1: ' + Date.now())
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);
                    if (data[0].result == "false") //
                    {
                        alert("השמירה נכשלה");
                    }
                    else//
                    {
                   //     console.log('2: ' + Date.now())
                        var finalGrade = {val: data[0].Final_Grade, text:data[0].ResourceText};
                        resetEdit(amida, gilui, hatraa, harigim, comander_Affect, explantion , finalGrade);
                         
                     
                    }
                }
            });
        }
    }

        function createEditDDL(ItgonenutId)
        {
            var tdAmida = $('#TD_Amida_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Amida_BeforeRefresh, tdAmida.html())
            var tdGilui = $('#TD_Gilui_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Gilui_BeforeRefresh, tdGilui.html())
            var tdHatraa = $('#TD_Hatraa_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Hatraa_BeforeRefresh, tdHatraa.html())
            var tdComanderAffect = $("#TD_Comander_Affect_" + ItgonenutId)
            setValuesForUndoCheckout(glbEditedLine_Comander_Affect_BeforeRefresh, tdComanderAffect.html())
            glbEditedLine_Harigim_BeforeRefresh = $("#TD_Harigim_" + ItgonenutId).html();
            var tdHarigim = $("#TD_Harigim_" + ItgonenutId);
            glbEditedLine_Explantion_BeforeRefresh = $("#TD_Explantion_" + ItgonenutId).html();
            var tdExplantion = $("#TD_Explantion_" + ItgonenutId);
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())
            tdAmida.empty();
            tdAmida.attr('class', 'tdEditClass');
            tdGilui.empty();
            tdGilui.attr('class', 'tdEditClass');
            tdHatraa.empty();
            tdHatraa.attr('class', 'tdEditClass');
            tdComanderAffect.empty();
            tdComanderAffect.attr('class', 'tdEditClass');
            tdHarigim.empty();
            tdExplantion.empty();
            cboAmida = createCombo(curCombosDT, 'Amida', tdAmida, ItgonenutId, false, glbEditedLine_Amida_BeforeRefresh.val, true,false);
            cboGilui = createCombo(curCombosDT, 'Gilui', tdGilui, ItgonenutId, false, glbEditedLine_Gilui_BeforeRefresh.val, true, false);
            cboHatraa = createCombo(curCombosDT, 'Hatraa', tdHatraa, ItgonenutId, false,glbEditedLine_Hatraa_BeforeRefresh.val, true, false);
            cboComander_Affect = createCombo(curCombosDT, 'Comander_Affect', tdComanderAffect, ItgonenutId, true, glbEditedLine_Comander_Affect_BeforeRefresh.val, true, false);
            createTextArea(tdHarigim, 'Harigim', ItgonenutId, glbEditedLine_Harigim_BeforeRefresh, 120);
            createTextArea(tdExplantion, 'Explantion', ItgonenutId, glbEditedLine_Explantion_BeforeRefresh,120);
        }

        


        function resetEdit(amida, gilui, hatraa, harigim, comander_Affect, exlanation, finalGrade, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            setTDValTextAndColorObj($('#TD_Amida_' + glbEditedLine_ItgonenutId), amida, false);
            setTDValTextAndColorObj($('#TD_Gilui_' + glbEditedLine_ItgonenutId), gilui, false);
            setTDValTextAndColorObj($('#TD_Hatraa_' + glbEditedLine_ItgonenutId), hatraa, false);
            setTDValTextAndColorObj($('#TD_Comander_Affect_' + glbEditedLine_ItgonenutId), comander_Affect, false);
            $('#TD_Harigim_' + glbEditedLine_ItgonenutId).html(harigim);
            $('#TD_Explantion_' + glbEditedLine_ItgonenutId).html(exlanation);
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }

    






        </script>
      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/גילה והפצה-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; גיל"ה והפצה</span>
          </div>
      <div style="width: 99%; margin: 0 auto;">
        
      
        <div style="height: 700px; overflow-y: auto;">
           
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 9%;" /><!-- 21%-->
                    <col style="width: 11%;" />
                    <col style="width: 11%;" /> <!-- 43%-->
                    <col style="width: 13%;" />
                    <col style="width: 7%;" /> <!-- 63%-->

                    <col style="width: 13%;" /><!-- 76%-->

                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th  class="TBLHeader">עמידה בהישג נדרש
                    </th>                  
                    <th  class="TBLHeader">קיום וכשירות מערכות הגילוי
                    </th >
                    <th class="TBLHeader">קיום וכשירות מערכות ההתרעה
                    </th>
                    <th class="TBLHeader">אירועים חריגים
                    </th>
                     <th  class="TBLHeader">הערכת מפקד
                    </th>
                    <th  class="TBLHeader">
                        הסבר (מלל חופשי)
                    </th>
                    <th  class="TBLHeader">
                        ציון סופי
                    </th>
                    <th  class="TBLHeader" id="TH_Actions"></th>
                    <th  class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>            
        </div>
    </div>
</asp:Content>