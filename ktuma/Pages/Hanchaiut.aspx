﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master"
     CodeFile="Hanchaiut.aspx.cs" Inherits="Pages_Hanchaiut" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <script type="text/javascript">
         var urlItgonenutId = -1
         var chosenMidrag;
         $(document).ready(function ()//
         {
             if ('<%=urlItgonenutId%>' != '') {
                 //05.03.2018 - remove return to hompage button
                 $('#BTN_ReturnHome').css('display', 'none');
                 $('#BTN_ReturnMainPortal').css('display', 'none');
                 $('#DIV_ClearAll').css('display', 'none');
             }
             var uid = '<%=Session["userID"]%>';
             checkSession(uid);
             loadParams(uid, true, false);
             loadAdminSviva('<%=Session["SvivaId"]%>');


         });
    
         function refresh(isFullRefresh) {
             if ('<%=urlItgonenutId%>' != '') {
                 urlItgonenutId = '<%=urlItgonenutId%>';
             }
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var jsonObj =
                     { op: 'loadHanchaiut', 'userName': userName, 'Itgonenut_Id': urlItgonenutId };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     chosenMidrag = data[0].currentMidrag;
                     fillTable(data, isFullRefresh);


                 }
             });
         }
                
         
         function fillTable(data, isFullRefresh) {
             var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
             if (isFullRefresh) //********************* full redraw table
             {
                 //remove all rows except first one
                 $("#tblData").find("tr:gt(0)").remove();
                 for (var i = 0; i < data.length; i++)//
                 {
                     buildTableRow(data, i, userTypeId);
                 }
                 if (urlItgonenutId != -1) {
                     //05.03.2018 - remove added buttons
                     //addButtons(urlItgonenutId);

                 }
                 else {                     
                     setColorsCol('tblData');
                 }
                 if (chosenMidrag != '') {
                     chooseRow(chosenMidrag);
                 }


             }
             else //only refresh values !!!
             {

             }

         }

         function buildTableRow(data, i, userTypeId) {
             var row = '';
             row = '';
             if (data[i].Champ_Affect == null || data[i].Champ_Affect == undefined) {
                 data[i].Champ_Affect = '';
             }
             if (data[i].Explantion_Champ_Affect == null || data[i].Explantion_Champ_Affect == undefined) {
                 data[i].Explantion_Champ_Affect = '';
             }

             if (data[i].Midrag == null || data[i].Midrag == undefined) {
                 data[i].Midrag = '';
             }

             bgClass = getClassByExactValue(data[i].Color);
             if (data[i].Sherootim == null || data[i].Sherootim == undefined) {
                 data[i].Sherootim = '';
             }
             if (data[i].Dilemot == null || data[i].Dilemot == undefined) {
                 data[i].Dilemot = '';
             }
             if (data[i].Itcansoot == null || data[i].Itcansoot == undefined) {
                 data[i].Mosach = '';
             }
             if (data[i].Itcansoot == null || data[i].Mosach == undefined) {
                 data[i].Mosach = '';
             }
             if (data[i].Sector == null || data[i].Sector == undefined) {
                 data[i].Sector = '';
             }
             if (data[i].Job == null || data[i].Job == undefined) {
                 data[i].Job = '';
             }
             row += '<td id="TD_Midrag_' + data[i].Midrag + '" class="' + bgClass + '" style-"height:30px;" >' + data[i].Midrag + '</td>';
             row += '<td id="TD_Itcansoot_' + data[i].Midrag + '" class="' + bgClass + '" >' + data[i].Itcansoot + '</td>';
             row += '<td id="TD_Mosach_' + data[i].Midrag + '" class="' + bgClass + '" >' + data[i].Mosach + '</td>';
             row += '<td id="TD_Job_' + data[i].Midrag + '" class="' + bgClass + '" >' + data[i].Job + '</td>';
            //row += '<td id="TD_Sherootim_' + data[i].Midrag + '" class="' + bgClass + '" style="display:none;" >' + data[i].Sherootim + '</td>';
             row += '<td id="TD_Sector_' + data[i].Midrag + '" class="' + bgClass + '" >' + data[i].Sector + '</td>';
             row += '<td id="TD_Dilemot_' + data[i].Midrag + '" class="' + bgClass + '" >' + data[i].Dilemot + '</td>';

             var style = ''
             if (data.length == 8) {
                 style = 'style="display:none;vertical-align:bottom;"';
             }
             else {
                 style = 'style="display:block;vertical-align:bottom;"';
             }
             if (userTypeId != "-1") {
                 var inputChoose = '<input id="INP_Choose_' + data[i].Color + '" type="button" onclick="chooseRowNew(' + data[i].Midrag + ')" value="בחר הנחיה"  class="btnEdit" ' + style + ' />';
                 row += '<td id="TD_Choose_' + data[i].Color + '" ' + style + ' >' + inputChoose + '</td>';
             }
             else {
                 $('#DIV_ClearAll').css('display', 'none');
             }
             $('#tblData').append('<tr class="TBLRow" id="tr_midrag_' + data[i].Midrag + '">' + row + '</tr>')
         }

         function setColorsCol(tblID) {
             var id = $('#' + tblID + ' tr');
             var curRow = 1;
             var myform1 = $(id).eq(curRow);
             var rowSpan = 2;             
             var curText = 4
             bgClass = getClassByExactValue(curText);
             myform1.append('<td style="border:1px solid black;" class="' + bgClass + '"  rowspan=' + rowSpan + '>' + curText + '</td>');
             for (var i = 2; i <= 4; i++) {
                 curRow = curRow + rowSpan;
                 myform1 = $(id).eq(curRow)
                 curText = 4 - i + 1;
                 bgClass = getClassByExactValue(curText);
                 myform1.append('<td style="border:1px solid black;" class="' + bgClass + '"  rowspan=' + rowSpan + '>' + curText + '</td>');
             }
         }
    

         function chooseRow(midrag) {
             chosenMidrag = midrag;
             $("tr[id^='tr_midrag']").css('border', 'none');
             $('#tr_midrag_' + midrag).css('border', '8px solid black')
             //05.03.2018 - cancel approve button, set the choice on chooseRow             
         }

         function chooseRowNew(midrag)
         {
             chooseRow(midrag);
             ApproveChoise();
         }

         function addButtons(Itgonenut_Id) {
             var div2 = $('<table align="center" style="text-align:center;direction:rtl; width:300px;">');
             var spanCancel = $('<td style="width:100px;"><input id="INP_Choose_' + Itgonenut_Id + '" type="button" onclick="CancelChoise()" value="אפס בחירה בעמוד"   class="btnEditWidth"/></td>');
             var spanReset = $('<td style="width:100px;"><input id="INP_Reset_' + Itgonenut_Id + '" type="button" onclick="ResetChoise()" value="הורד הנחיה עבור אזור ההתגוננות"   class="btnEditWidth"/></td>');
             var spanApprove = $('<td style="width:100px;"><input id="INP_Approve_' + Itgonenut_Id + '" type="button" onclick="ApproveChoise()" value="אשר בחירה"  class="btnEditWidth"  /></td>');
             div2.append(spanCancel).append(spanReset).append(spanApprove);
             $('#TR_Header').append('<th></th>');
             $('#mainDiv').append(div2);
         }

         function CancelChoise() { //unselect
             //unselect all rows
             $("tr[id^='tr_midrag']").css('border', 'none');
             chosenMidrag = -1;
         }

         function ClearAll() {//update null in the db
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var pageName = 'Hanchaiut'
             var jsonObj =
                 {
                     page: pageName, "op": "clearAllHanchiut", "ItgonenutId": urlItgonenutId, "userId": '<%=Session["userID"]%>',
                     "midrag": -1
                 }
             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     CancelChoise();
                     alert('הפעולה הסתיימה בהצלחה')

                 }
             });

         }

         function ResetChoise() {//update null in the db
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var pageName = 'Hanchaiut'
             var jsonObj =
                 {
                     page: pageName, "op": "save", "ItgonenutId": urlItgonenutId, "userId": '<%=Session["userID"]%>',
                     "midrag": -1
                 }
             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     CancelChoise();
                     alert('הפעולה הסתיימה בהצלחה')

                 }
             });

         }
         function ApproveChoise() {
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var pageName = 'Hanchaiut'
             var jsonObj =
                 {
                     page: pageName, "op": "save", "ItgonenutId": urlItgonenutId, "userId": '<%=Session["userID"]%>',
                     "midrag": chosenMidrag
                 }
             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     alert('הפעולה הסתיימה בהצלחה')


                 }
             });
         }
         </script>

       <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/הנחיות-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; הנחיות</span>
       </div>
        
    <div id="DIV_ClearAll" style="margin-bottom:10px; text-align:right;">
        <input type="button" id="INP_ClearAll" value="אפס הנחיות" onclick="ResetChoise()" class="btnEdit" style="width:200px; margin-right:20px;" />
    </div>
        <div style="height: 700px; overflow-y: auto;" id="mainDiv">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 5%;" /><!--מדרג-!-->
                    <col style="width: 15%;" /><!--התכנסויות והתקהלויות-!-->
                    <col style="width: 15%;" /><!--פעילות חינוכית-!-->
                    <col style="width: 15%;" /><!--50%--><!--מקומות עבודה ומתחמי קניות שאינם חיוניים-!-->
                    <!--col style="width: 15%;" /><!--שירותים לקבל הרחב-!-->
                    <col style="width: 15%;" /><!--סקטור חיוני-!-->                                                            
                    <col style="width: 15%;" /><!--דילמות/ דגשים-!-->                    
                    <col style="width: 5%;" /><!--שקלול המרכיבים-!-->
                </colgroup>
                <tr class="TBLHeader" id="TR_Header">
                    <th class="TBLHeader">מדרג</th>
                    <th class="TBLHeader">התכנסויות מופעים וארועים</th>
                    <th class="TBLHeader">פעילות חינוכית</th>
                    <th class="TBLHeader">מקומות עבודה ומתחמי קניות שאינם חיוניים</th>                  
                    <!--th class="TBLHeaderDispNone">שירותים לקבל הרחב</th-->
                    <th class="TBLHeader">סקטור חיוני</th>                                                              
                     <th class="TBLHeader">דילמות/ דגשים</th>                     
                     <th class="TBLHeader" id="TH_Actions">שקלול המרכיבים</th>                    
                </tr>
            </table>
        </div>
  
</asp:Content>