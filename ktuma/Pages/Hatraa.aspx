﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Heading.master" AutoEventWireup="true" CodeFile="Hatraa.aspx.cs" Inherits="Pages_Hatraa" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_mostcommon_BeforeRefresh
        var glbEditedLine_minCountPerRegion_BeforeRefresh;
        var glbEditedLine_maxCountPerRegion_BeforeRefresh;
        var glbEditedLine_Explantion_Commander_Affect_BeforeRefresh;
        var glbEditedLine_Comander_Affect_BeforeRefresh;
        var glbEditedLine_FinalGrade_BeforeRefresh = {};

     $(document).ready(function ()//
     {
         loadCombosHaatra();
         var uid = '<%=Session["userID"]%>'
         checkSession(uid);
         loadParams('<%=Session["userID"]%>', true, false);
         loadAdminSviva('<%=Session["SvivaId"]%>');
     })
    
        function loadCombosHaatra() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Haatra';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;
                }
            });


        }

       
     function refresh(isFullRefresh) //
     {
       
         var userName = '<%=Session["UserName"]%>';
         checkSession(userName);
             var jsonObj =
                     { op: 'loadHaatra', 'userName': userName };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data, isFullRefresh, userName);
                    
                 }
             });
         }
        
        
        function fillTable(data, isFullRefresh, userName) //
        {

            var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
                
                
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                           
                        }
                        else {
                            $('#TD_mostcommon_' + data[i].Itgonenut_Id).html(data[i].mostcommon);
                            $('#TD_minCountPerRegion_' + data[i].Itgonenut_Id).html(data[i].minCountPerRegion);
                            $('#TD_maxCountPerRegion_' + data[i].Itgonenut_Id).html(data[i].maxCountPerRegion);
                            $('#TD_Comander_Affect_' + data[i].Itgonenut_Id).html(data[i].Comander_Affect);
                            $('#TD_Explantion_Commander_Affect_' + data[i].Itgonenut_Id).html(data[i].Explantion_Commander_Affect);
                            setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            if (data[i].mostcommon == null || data[i].mostcommon == undefined) {
                data[i].mostcommon = '';
            }
            if (data[i].minCountPerRegion == null || data[i].minCountPerRegion == undefined) {
                data[i].minCountPerRegion = '';
            }

            if (data[i].maxCountPerRegion == null || data[i].maxCountPerRegion == undefined) {
                data[i].maxCountPerRegion = '';
            }

            if (data[i].Comander_Affect == null || data[i].Comander_Affect == undefined) {
                data[i].Comander_Affect = '';
            }          
            if (data[i].Explantion_Commander_Affect == null || data[i].Explantion_Commander_Affect == undefined) {
                data[i].Explantion_Commander_Affect = '';
            }           
            row += createTDWithWrap(data[i].Itgonenut_Id, 'mostcommon', data[i].mostcommon);
            row += createTDWithWrap(data[i].Itgonenut_Id, 'minCountPerRegion', data[i].minCountPerRegion);
            row += createTDWithWrap(data[i].Itgonenut_Id, 'maxCountPerRegion', data[i].maxCountPerRegion);
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Comander_Affect', data[i].Comander_Affect);
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explantion_Commander_Affect', data[i].Explantion_Commander_Affect);
            bgClass = getClassByExactValue(data[i].FinalGradeValue);
            row += createOrigTD(bgClass, 'Final_Grade', data[i].Itgonenut_Id, data[i].FinalGradeText, data[i].FinalGradeValue);            


            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);
            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if ('<%=Session["UserName"]%>'  != '' && data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row +=  '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }


        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Hatraa_Comander_Affect'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Comander_Affect_BeforeRefresh, glbEditedLine_Explantion_Commander_Affect_BeforeRefresh, glbEditedLine_FinalGrade_BeforeRefresh, Itgonenut_Id);
        }
        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }


        function checkLower100(obj)//
        {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }


        function checkOutOrSave(ItgonenutId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = "Haatra";
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)

                        }
                    }
                });
            }
            else//
            {

                var Comander_Affect = $("#INP_Comander_Affect_" + ItgonenutId).val();
                var Explantion_Commander_Affect = $("#INP_Explantion_Commander_Affect_" + ItgonenutId).val();

                if ((Comander_Affect != null && Comander_Affect != '0' && Comander_Affect != '') && Explantion_Commander_Affect == '') {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }

                var jsonObj =
                        {
                            page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>',
                            "Explantion_Commander_Affect": Explantion_Commander_Affect, "Comander_Affect": Comander_Affect
                        };

                //console.log('1: ' + Date.now())
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
                            //     console.log('2: ' + Date.now())
                            var finalGrade = { val: data[0].FinalGrade, text: data[0].FinalGradeText };
                            resetEdit(Comander_Affect, Explantion_Commander_Affect, finalGrade);


                        }
                    }
                });
            }
        }

        function createEditDDL(ItgonenutId)
        {
            var tdmostcommon = $('#TD_mostcommon_' + ItgonenutId);
            glbEditedLine_mostcommon_BeforeRefresh = tdmostcommon.html();            
            var tdminCountPerRegion = $('#TD_minCountPerRegion_' + ItgonenutId);
            glbEditedLine_minCountPerRegion_BeforeRefresh = tdminCountPerRegion.html();            
            var tdmaxCountPerRegion = $('#TD_maxCountPerRegion_' + ItgonenutId);
            glbEditedLine_maxCountPerRegion_BeforeRefresh = tdmaxCountPerRegion.html();
            var tdComander_Affect = $("#TD_Comander_Affect_" + ItgonenutId)
            glbEditedLine_Comander_Affect_BeforeRefresh = tdComander_Affect.html();
            var tdExplantion_Commander_Affect = $("#TD_Explantion_Commander_Affect_" + ItgonenutId);
            glbEditedLine_Explantion_Commander_Affect_BeforeRefresh = tdExplantion_Commander_Affect.html();
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())
            tdExplantion_Commander_Affect.empty();
            tdExplantion_Commander_Affect.attr('class', 'tdEditClass');
            tdComander_Affect.empty();
            tdComander_Affect.attr('class', 'tdEditClass');
            createInput(tdComander_Affect, 'Comander_Affect', ItgonenutId, glbEditedLine_Comander_Affect_BeforeRefresh);
            setInputOnlyPositiveOrZero('INP_Comander_Affect_' + ItgonenutId);
            createTextArea(tdExplantion_Commander_Affect, 'Explantion_Commander_Affect', ItgonenutId, glbEditedLine_Explantion_Commander_Affect_BeforeRefresh, 120);
        }

        


        function resetEdit(Comander_Affect, Explantion_Commander_Affect, finalGrade, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')                       
            $('#TD_Comander_Affect_' + glbEditedLine_ItgonenutId).html(Comander_Affect);
            $('#TD_Explantion_Commander_Affect_' + glbEditedLine_ItgonenutId).html(Explantion_Commander_Affect);
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }

    






        </script>
      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
            <img src="../Images/התרעה-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; התרעה</span>
          </div>
      <div style="width: 99%; margin: 0 auto;">
        
      
        <div style="height: 700px; overflow-y: auto;">
           
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 9%;" /><!-- 21%-->
                    <col style="width: 11%;" />
                    <col style="width: 13%;" />
                    <col style="width: 7%;" /> <!-- 63%-->

                    <col style="width: 13%;" /><!-- 76%-->

                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th  class="TBLHeader">שכיח
                    </th>                  
                    <th  class="TBLHeader">ערך מינימלי
                    </th >
                    <th class="TBLHeader">ערך מקסימלי
                    </th>
                     <th  class="TBLHeader">הערכת מפקד
                    </th>
                    <th  class="TBLHeader">
                        הסבר (מלל חופשי)
                    </th>
                    <th  class="TBLHeader">
                        ציון סופי
                    </th>
                    <th  class="TBLHeader" id="TH_Actions"></th>
                    <th  class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>            
        </div>
    </div>
</asp:Content>
