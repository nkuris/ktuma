﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master"
     CodeFile="Migun.aspx.cs" Inherits="Pages_Migun" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        
        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_ValPrati_BeforeRefresh;
        var glbEditedLine_ValTziboori_BeforeRefresh;
        var glbEditedLine_ValMosach_BeforeRefresh;
        var glbEditedLine_FinalGrade_BeforeRefresh = {};

        $(document).ready(function ()//
        {
            var uid = '<%=Session["userID"]%>'
            checkSession(uid);
            loadParams(uid,true, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });


        function fillFirstComboAndParams() {
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                dataType: 'json',
                data: { op: 'loadCombos' },
                //async: false,
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    
                    allOrigData = data;
                }

            });
        }



        function fillCombo(dataTable, cboId, colValue, colText, blnAddEmpty, filterFieldNameOnDest, filterFieldByParentValue) //
        {
            fillComboChild(dataTable, cboId, colValue, colText, blnAddEmpty, filterFieldNameOnDest, filterFieldByParentValue) //
            //on every change refresh the table
            refresh(true);
        }



   
        function refresh(isFullRefresh) //
        {
            //  dist_id, resp_id, prim_id, sec_id, third_id
            //var distVal = cboGetVal('cboDist');
            // var respVal = cboGetVal('cboResp');
            // var primVal = cboGetVal('cboPrim');
            // var secVal = cboGetVal('cboSec');
            //  var thirdVal = cboGetVal('cboThird');

            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
         var jsonObj =
                 { op: 'loadMigun', 'userName': userName };

         $.ajax({
             type: "POST",
             url: "../Handler.ashx",
             data: jsonObj,
             dataType: "json",
             failure: function (response) {
                 alert(response.d);
             },
             success: function (data) {
                 //console.log(data);

                 fillTable(data, isFullRefresh);

             }
         });
     }

     function fillTable(data, isFullRefresh) //
     {
         var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
         if (isFullRefresh) //********************* full redraw table
         {
             //remove all rows except first one
             $("#tblData").find("tr:gt(0)").remove();
             for (var i = 0; i < data.length; i++)//
             {
                 buildTableRow(data, i, userTypeId)
             }
             setRangesCol('tblData');
             
         }
         else //only refresh values !!!
         {
             //only refresh don't redraw table
             var i = 0;
             $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
             (//
                 function ()//
                 {
                     if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                     {
                         //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                         //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                         //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                     }
                     else {
                         $('#TD_Prati_' + data[i].Itgonenut_Id).html(data[i].Prati);
                         $('#TD_Tziboori_' + data[i].Itgonenut_Id).html(data[i].Tziboori);
                         $('#TD_Mosach_' + data[i].Itgonenut_Id).html(data[i].Mosach);
                         setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                         setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                         var disableAllEditButtons = '';
                         if (data[0].isUserEdited == 1)//
                         {
                             disableAllEditButtons = 'disabled="disabled"';
                         }
                         if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                             var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                             var strClick = createSTRClick(data[i].Itgonenut_Id);
                             var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                             //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                             edit.html(strButton);
                         }
                         else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                             var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                             edit.html('');
                         }
                     }
                     //update user checkout on row
                     $(this).find('.data_user').html(data[i].user_name);
                     i++;
                 }//
             ); //END .each
         }
     }

     function buildTableRow(data, i, userTypeId) {
         var row = '';
         row = '';

         if (data[i].Prati == null || data[i].Prati == undefined) {
             data[i].Prati = '';
         }
         if (data[i].Tziboori == null || data[i].Tziboori == undefined) {
             data[i].Tziboori = '';
         }

         if (data[i].Mosach == null || data[i].Mosach == undefined) {
             data[i].Mosach = '';
         }
         row += '<td style="text-align:center">' + data[i].Name + '</td>';
         bgClass = getClassByValue(data[i].Prati);
         row += '<td id="TD_Prati_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].Prati + '</td>';
         bgClass = getClassByValue(data[i].Tziboori);
         row += '<td id="TD_Tziboori_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].Tziboori + '</td>';
         bgClass = getClassByValue(data[i].Mosach);
         row += '<td id="TD_Mosach_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].Mosach + '</td>';
         bgClass = getClassByExactValue(data[i].Final_Grade);
         row += '<td id="TD_Final_Grade_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].FinalGradeValue + ' - ' + data[i].FinalGradeText + '</td>';


         var strClick = createSTRClick(data[i].Itgonenut_Id);
         var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
         var disableAllEditButtons = '';
         if (data[0].isUserEdited == 1)//
         {
             disableAllEditButtons = 'disabled="disabled"';
         }

         var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);

         if (userTypeId != "-1") {
             if (data[i].user_name == null) // no user took this button yet, for reguler users
             {
                 row += strButton;
             }
             else if ('<%=Session["UserName"]%>' != '' && data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
             {
                 strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                 strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                 row += strButton;
                 //first time
                 if (timer_gear == -1)//
                 {
                     glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                     startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                 }
             }
             else //
             {
                 row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
             }

         }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }


        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }


        function checkLower100(obj)//
        {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }

        function checkOutOrSave(ItgonenutId) {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Migun';
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName)
                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                        }
                    }
                });
            }
            else//
            {

                var prati = $("#INP_Prati_" + ItgonenutId).val();
                var tziboori = $("#INP_Tziboori_" + ItgonenutId).val();
                var mosach = $("#INP_Mosach_" + ItgonenutId).val();
                if (!(($.isNumeric(prati) && numberInRange(prati, 1, 100)) &&
                    ($.isNumeric(tziboori) && numberInRange(tziboori, 1, 100)) &&
                    ($.isNumeric(mosach) && numberInRange(mosach, 1, 100))
                    )) {
                    alert('הכנס רק ערכים מספריים בין 1 ל-100')
                    return;
                }

                var jsonObj =
                        {
                            page: pageName, "op": "save", "ItgonenutId": ItgonenutId,
                            "userId": '<%=Session["userID"]%>', "prati": prati, "tziboori": tziboori, "mosach": mosach
                        };
                //alert(JSON.stringify(jsonObj));


                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
                            var finalGrade = { val: data[0].Final_Grade, text: data[0].ResourceText };
                            resetEdit(prati, tziboori, mosach, finalGrade, ItgonenutId);

                            // enableDisbaleUserLine(userType, 'disable');
                            //$("#cph_region" + userType + "_EditedBy").val("");//after save clear my user from "active user cell"
                        }
                    }
                });
            }
        }

        function createEditDDL(ItgonenutId) {
            glbEditedLine_ValPrati_BeforeRefresh = $("#TD_Prati_" + ItgonenutId).html();
            var tdPrati = $('#TD_Prati_' + ItgonenutId);
            glbEditedLine_ValTziboori_BeforeRefresh = $("#TD_Tziboori_" + ItgonenutId).html();
            var tdTziboori = $('#TD_Tziboori_' + ItgonenutId);
            glbEditedLine_ValMosach_BeforeRefresh = $("#TD_Mosach_" + ItgonenutId).html();
            var tdMosach = $('#TD_Mosach_' + ItgonenutId);
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())
            tdPrati.empty();
            tdPrati.attr('class', 'tdEditClass');
            tdTziboori.empty();
            tdTziboori.attr('class', 'tdEditClass');
            tdMosach.empty();
            tdMosach.attr('class', 'tdEditClass');
            createInput(tdPrati, 'Prati', ItgonenutId, glbEditedLine_ValPrati_BeforeRefresh);
            createInput(tdTziboori, 'Tziboori', ItgonenutId, glbEditedLine_ValTziboori_BeforeRefresh);
            createInput(tdMosach, 'Mosach', ItgonenutId, glbEditedLine_ValMosach_BeforeRefresh);
           



            //tdAmida.empty();



        }

        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Migun'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_ValPrati_BeforeRefresh, glbEditedLine_ValTziboori_BeforeRefresh,
                glbEditedLine_ValMosach_BeforeRefresh,
                 glbEditedLine_FinalGrade_BeforeRefresh, Itgonenut_Id);
        }



        function resetEdit(prati, tziboori, mosach, finalGrade, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            bgClass = getClassByValue(prati)
            $('#TD_Prati_' + glbEditedLine_ItgonenutId).html(prati).attr('class', bgClass);
            bgClass = getClassByValue(tziboori)
            $('#TD_Tziboori_' + glbEditedLine_ItgonenutId).html(tziboori).attr('class', bgClass);
            bgClass = getClassByValue(mosach)
            $('#TD_Mosach_' + glbEditedLine_ItgonenutId).html(mosach).attr('class', bgClass);
            bgClass = getClassByExactValue(finalGrade)
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade, false);
            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>
     <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/מיגון-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; מיגון</span>
       </div>
      <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 6%;" /> <!--12%-->
                    <col style="width: 18%;" />
                    <col style="width: 18%;" />
                    <col style="width: 18%;" /><!--66%-->
                    <col style="width: 10%;" />
                    <col style="width: 7%;" />
                    <col style="width:7%;" /> <!--90% -->
                </colgroup>
                <tr class="TBLHeader">
                      <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">מיגון פרטי
                    </th>                  
                    <th class="TBLHeader">מיגון ציבורי
                    </th>
                    <th class="TBLHeader">מיגון מוסדות חינוך
                    </th>
                    
                     <th class="TBLHeader">דירוג סופי
                    </th>
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>