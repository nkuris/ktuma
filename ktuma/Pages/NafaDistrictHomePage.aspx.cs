﻿using pikudEntities;
using System;
using System.Configuration;


public partial class Pages_NafaDistrictHomePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            {//readonly

                //string strFullUser = Request.LogonUserIdentity.Name;
                ////            strMachine = "\n" + "machine=" + Request.UserHostAddress;
                //if (strFullUser.IndexOf('\\') == -1)
                //{
                //    Session["userID"] = "guest";
                //}
                //else
                //{
                //    int indName = strFullUser.IndexOf('\\');
                //    Session["userID"] = strFullUser.Substring(indName + 1);
                //}
            }

        }
        initContorls();
    }

    private void initContorls()
    {        
        BTN_NafaData.Click += BTN_NafaData_Click;
        BTN_DistrictData.Click += BTN_DistrictData_Click;
        BTN_Exceptions.Click += BTN_Exceptions_Click;
        BTN_NafaData.Text = "מדיניות התגוננות ברמת נפה - הסטוריה";
        BTN_DistrictData.Text = "מדיניות התגוננות ברמת מחוז - הסטוריה";
        BTN_Exceptions.Text = "מילוי החרגות ברמת מחוז - הסטוריה";
        if (Session["userID"] != null && Session["UserGroupId"]!= null)
        {
            if (Session["UserGroupId"].ToString() == "3")
            {
                BTN_NafaData.Text = "מדיניות התגוננות ברמת נפה";
            }
            else if (Session["UserGroupId"].ToString() == "2")
            {
                BTN_DistrictData.Text = "מדיניות התגוננות ברמת מחוז";
                BTN_Exceptions.Text = "מילוי החרגות ברמת מחוז";
            }
        }
    }

    private void BTN_Exceptions_Click(object sender, EventArgs e)
    {
        KtumaUser curUser = ((KtumaUser)Session["User"]);
        if (curUser != null && curUser.GroupId == 2)
        {
            transferToPageFromMain("ExceptionFill.aspx");
        }        
        else
        {
            transferToPageFromMain("ExceptionFillHistory.aspx");
        }
    }

    private void BTN_DistrictData_Click(object sender, EventArgs e)
    {
        KtumaUser curUser = ((KtumaUser)Session["User"]);
        if (curUser != null && curUser.GroupId == 2)
        {
            transferToPageFromMain("DistrictFill.aspx");
        }        
        else
        {
            transferToPageFromMain("DistrictFillHistory.aspx");
        }
    }

    private void BTN_NafaData_Click(object sender, EventArgs e)
    {
        KtumaUser curUser = ((KtumaUser)Session["User"]);
        if (curUser != null && curUser.GroupId == 3)
        {
            transferToPageFromMain("NafaFill.aspx");
        }        
        else
        {
            transferToPageFromMain("NafaFillHistory.aspx");
        }
    }
   
    private void transferToPageFromMain(string pageName)
    {
        Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        //Server.Transfer(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName);
    }

}

