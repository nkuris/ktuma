﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true" CodeFile="DistrictFill.aspx.cs" Inherits="Pages_DistrictFill" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
       var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh = {};
        var glbEditedLine_Adequacy_BeforeRefresh = '';
        var glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh = '';        

    $(document).ready(function ()//
    {
        var uId = '<%=Session["userID"]%>';
        checkSession(uId)
        loadCombosDistrict();
        loadParams(uId, true, false)
        loadAdminSviva('<%=Session["SvivaId"]%>');
        
    });

        function loadCombosDistrict() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)

            var pageName = 'DistrictFill';
            var jsonObj =
                { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


        }

    function refresh(isFullRefresh) //
     {
        var userName = '<%=Session["UserName"]%>';
        var userId = '<%=Session["userID"]%>';
         checkSession(userName)
             var jsonObj =
                     { op: 'loadDistrictFill', 'userName': userName, 'userId' : userId };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data, isFullRefresh, userName);
                    
                 }
             });
    }

     function fillTable(data, isFullRefresh) //
     {
         var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 2) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
         if (isFullRefresh) //********************* full redraw table
         {
             //remove all rows except first one
             $("#tblData").find("tr:gt(0)").remove();
             for (var i = 0; i < data.length; i++)//
             {
                 buildTableRow(data, i, userTypeId)
             }           
             
         }
         else //only refresh values !!!
         {
             //only refresh don't redraw table
             var i = 0;
             $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
             (//
                 function ()//
                 {
                     if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                     {
                         //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                         //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                         //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                     }
                     else {
                         $('#TD_CurrentItgonenutPolicy_' + data[i].Itgonenut_Id).html(data[i].CurrentItgonenutPolicyText);
                         setTDValTextAndColorShowTextForNull($('#TD_RecommendedItgonenutPolicy_' + data[i].Itgonenut_Id), data[i].RecommendedItgonenutPolicy, data[i].RecommendedItgonenutPolicyText, true);
                         $('#TD_ExplanationRecommendedPolicy_' + data[i].Itgonenut_Id).html(data[i].ExplanationRecommendedPolicy);
                         $('#TD_Adequacy_' + data[i].Itgonenut_Id).html(data[i].Adequacy);                         
                         var disableAllEditButtons = '';
                         if (data[0].isUserEdited == 1)//
                         {
                             disableAllEditButtons = 'disabled="disabled"';
                         }
                         if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                             var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                             var strClick = createSTRClick(data[i].Itgonenut_Id);
                             var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                             //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                             edit.html(strButton);
                         }
                         else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                             var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                             edit.html('');
                         }
                     }
                     //update user checkout on row
                     $(this).find('.data_user').html(data[i].user_name);
                     i++;
                 }//
             ); //END .each
         }
     }

     function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';

            if (data[i].CurrentItgonenutPolicyText == null || data[i].CurrentItgonenutPolicyText == undefined) {
                data[i].CurrentItgonenutPolicyText = '';
            }
            if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
                data[i].RecommendedItgonenutPolicyText = '';
            }

            if (data[i].ExplanationRecommendedPolicy == null || data[i].ExplanationRecommendedPolicy == undefined) {
                data[i].ExplanationRecommendedPolicy = '';
            }
            if (data[i].ExceptionPolicy == null || data[i].ExceptionPolicy == undefined) {
                data[i].ExceptionPolicy = '';
            }
            if (data[i].Adequacy == null || data[i].Adequacy == undefined) {
                data[i].Adequacy = '';
            }
            row += '<td style="text-align:center">' + data[i].Name + '</td>';
            bgClass = getClassByExactValuePop(data[i].CurrentItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'CurrentItgonenutPolicy', data[i].Itgonenut_Id, data[i].CurrentItgonenutPolicyText, data[i].CurrentItgonenutPolicy);
            bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].Itgonenut_Id, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);
            row += '<td id="TD_Adequacy_' + data[i].Itgonenut_Id + '" class="">' + data[i].Adequacy + '</td>';
            row += '<td id="TD_ExplanationRecommendedPolicy_' + data[i].Itgonenut_Id + '" class="">' + data[i].ExplanationRecommendedPolicy + '</td>';



            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);

            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if ('<%=Session["UserName"]%>' != '' && data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
     }

      function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'DistrictFill'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
          resetEdit(glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh, glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh,
              glbEditedLine_Adequacy_BeforeRefresh, Itgonenut_Id);
        }

        function checkOutOrSave(Itgonenut_Id)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)
            var pageName = "DistrictFill";
            if ($("#btnEdit" + Itgonenut_Id).val() == 'ערוך')//
            {
                var jsonObj =
                    { page: pageName, op: 'checkout', 'Itgonenut_Id': Itgonenut_Id, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(Itgonenut_Id, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                            createEditDDL(Itgonenut_Id);
                            glbEditedLine_ItgonenutId = Itgonenut_Id;
                            startTimerForUndoCheckout(Itgonenut_Id, data_undoCheckoutTimeoutInMS)

                        }
                    }
                });
            }
            else//
            {
                var recommendedPolicy = {};
                var ExceptionPolicy = $("#INP_ExceptionPolicy_" + Itgonenut_Id).val();
                var ExplanationRecommendedPolicy = $("#INP_ExplanationRecommendedPolicy_" + Itgonenut_Id).val();
                var Adequacy = $("#INP_Adequacy_" + Itgonenut_Id).val();

                setValuesForUndoCheckoutFromTextOnly(recommendedPolicy, "RecommendedItgonenutPolicy", Itgonenut_Id)

                if ((recommendedPolicy.val != '0' && recommendedPolicy.val != '') && ExplanationRecommendedPolicy == '') {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }

                var jsonObj =
                {
                    page: pageName, "op": "save", "ItgonenutId": Itgonenut_Id, "userId": '<%=Session["userID"]%>',
                    "recommendedPolicy": recommendedPolicy.val, "ExplanationRecommendedPolicy": ExplanationRecommendedPolicy
                    , "adequacy": Adequacy
                };

                //console.log('1: ' + Date.now())
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {

                            resetEdit(recommendedPolicy, ExplanationRecommendedPolicy, Adequacy, Itgonenut_Id);


                        }
                    }
                });
            }
        }

        function createEditDDL(Itgonenut_Id)
        {
            var tdRecommendedItgonenutPolicy = $('#TD_RecommendedItgonenutPolicy_' + Itgonenut_Id);
            setValuesForUndoCheckout(glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh, tdRecommendedItgonenutPolicy.html())
            var tdExplanationRecommendedPolicy = $("#TD_ExplanationRecommendedPolicy_" + Itgonenut_Id);
            glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh = tdExplanationRecommendedPolicy.html();
            var tdAdequacy = $("#TD_Adequacy_" + Itgonenut_Id);
            glbEditedLine_Adequacy_BeforeRefresh = tdAdequacy.html();            
            tdRecommendedItgonenutPolicy.empty();
            tdRecommendedItgonenutPolicy.attr('class', 'tdEditClass');
            tdExplanationRecommendedPolicy.empty();
            tdExplanationRecommendedPolicy.attr('class', 'tdEditClass');
            tdAdequacy.empty();
            tdAdequacy.attr('class', 'tdEditClass');
            cboRecommendedItgonenutPolicy = createCombo(curCombosDT, 'RecommendedItgonenutPolicy', tdRecommendedItgonenutPolicy, Itgonenut_Id, false, glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh.val, true, false);            
            createTextArea(tdExplanationRecommendedPolicy, 'ExplanationRecommendedPolicy', Itgonenut_Id, glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh, 120);
            createTextArea(tdAdequacy, 'Adequacy', Itgonenut_Id, glbEditedLine_Adequacy_BeforeRefresh,120);
        }

        


        function resetEdit(recommendedPolicy, ExplanationRecommendedPolicy, Adequacy, Itgonenut_Id) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            setTDValTextAndColorObj($('#TD_RecommendedItgonenutPolicy_' + glbEditedLine_ItgonenutId), recommendedPolicy, true);
            $('#TD_ExplanationRecommendedPolicy_' + glbEditedLine_ItgonenutId).html(ExplanationRecommendedPolicy);
            $('#TD_Adequacy_' + glbEditedLine_ItgonenutId).html(Adequacy);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }



</script>
       <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 18%;" /> <!--18%-->
                    <col style="width: 18%;" />
                    <col style="width: 12%;" /><!--54%-->
                    <col style="width: 12%;" /><!--66%-->
                    <col style="width: 10%;" />
                    <col style="width: 7%;" />
                    <col style="width:7%;" /> <!--90% -->
                </colgroup>
                <tr class="TBLHeader">
                      <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות נוכחית
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>                  
                    <th class="TBLHeader">הלימה במדיניות התגוננות
                    </th>
                    <th class="TBLHeader">הסבר להמלצה
                    </th>                                         
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

