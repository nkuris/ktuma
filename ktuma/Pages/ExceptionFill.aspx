﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true" CodeFile="ExceptionFill.aspx.cs" Inherits="Pages_ExceptionFill" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        var curCombosDT;
        var ItgonenutRegionsDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_ItgonenutIdName = '';
        var glbEditedLine_rashutId = -1;
        var glbEditedLine_rashutIdName = '';
        var glbEditedLine_ExceptionFillRowId = -1;//table row id for updating and deleting
        var glbEditedLine_Parameter_BeforeRefresh = '';
        var glbEditedLine_ParameterText_BeforeRefresh = '';        
        var glbEditedLine_InstitutionName_BeforeRefresh = '';
        var glbEditedLine_SecurityTour_BeforeRefresh = '';
        var glbEditedLine_SecurityTourText_BeforeRefresh = '';
        var glbEditedLine_ExplanationToException_BeforeRefresh = '';  
        var glbEditedLine_ItgonenutRegionExceptTo_BeforeRefresh = ''; 
        var glbEditedLine_ItgonenutRegionExceptToName_BeforeRefresh = ''; 
        var glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh = {};
        var glbEditedLine_Exception_BeforeRefresh = '';
        
        $(document).ready(function ()//
        {
            var uId = '<%=Session["userID"]%>';
            checkSession(uId)
            loadExceptionPageDDL(uId);
            
        });

        function loadExceptionPageDDL(uId) {

            loadCombosException();
            loadAdminSviva('<%=Session["SvivaId"]%>');
            ;
            let promiseRashuyot = loadRashuyotDT();
            promiseRashuyot.then(function (result) {
                loadItgonenutRegionsDT()
            }).then(function (result) {
                loadParams(uId, true, true);
            });
        }
     

        function loadCombosException() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)

            var pageName = 'ExceptionFill';
            var jsonObj =
                { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


        }

        function refresh(isFullRefresh) //
        {
            var userName = '<%=Session["UserName"]%>';
            var userId = '<%=Session["userID"]%>';
            checkSession(userName)
            var jsonObj =
                { op: 'loadExceptionFill', 'userName': userName, 'userId': userId };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data, isFullRefresh, userName);

                }
            });
        }

        function checkException() {
            if ($('#INP_Exception_Edit').val() == null || $('#INP_Exception_Edit').val() == '') {
                alert('אנא הכנס בקשה')
                return false;
            }
            if ($('#INP_ExceptionExplanation_Edit').val() == null || $('#INP_ExceptionExplanation_Edit').val() == '') {
                alert('אנא הכנס הסבר לבקשה')
                return false;
            }
            return true;
        }

        function saveNewException(ExceptionFillRowId) {
            var pageName = "ExceptionFill";
            var rashutId = $('#SELECT_rashutId_' + ExceptionFillRowId)
            var parameter = $('#SELECT_Parameter_' + ExceptionFillRowId)
            var instituteName = $('#INP_Institute_Edit')
            var explanationToException = $("#INP_ExceptionExplanation_Edit")
            var securityTour = $("#SELECT_TourSecurity_" + ExceptionFillRowId)
            var exception = $("#INP_Exception_Edit")
            var itgonenutRegionExceptTo = $('#SELECT_ItgonenutRegionExceptTo_' + ExceptionFillRowId)

            if ((rashutId.val() == '0')) {
                alert('הכנס רשות');
                return;
            }
            if ((exception.val() == '')) {
                alert('הכנס החרגה');
                return;
            }
            if ((explanationToException.val() == '')) {
                alert('הכנס הסבר להחרגה');
                return;
            }

            var jsonObj =
            {
                page: pageName, "op": "save", "rashutId": rashutId.val(), "userId": '<%=Session["userID"]%>',
                "parameter": parameter.val(), "instituteName": instituteName.val(), "explanationToException": explanationToException.val()
                , "securityTour": securityTour.val(), "exception": exception.val(), 'itgonenutRegionExceptTo': itgonenutRegionExceptTo.val()
                , 'exceptionFillRowId': ExceptionFillRowId
            };

            //console.log('1: ' + Date.now())
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);
                    if (data[0].result == "false") //
                    {
                        alert("השמירה נכשלה");
                    }
                    else if (data[0].result == 'alreadyRashut')
                    {
                        alert("לא ניתן לשמור - קיימת החרגה לרשות זו במערכת.");
                    }
                    else//
                    {
                        glbEditedLine_ExceptionFillRowId = -1;
                        window.location = window.location;


                    }
                }
            });
        }
        

        function addExceptionModal() {
            $("#dialog-Exceptionmodal").dialog({
                height: 550,
                width: 500,
                closeOnEscape: false,
                modal: true,
                title: "הזן החרגה",
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                buttons: [
                    {
                        id: "addRowConfirm",
                        text: "אשר",
                        click: function () {
                            if (checkException()) {
                                if (saveNewException(-1)) {
                                    $(this).dialog("close");
                                }
                            }


                        }
                    },
                    {
                        id: "addRowCancel",
                        text: "בטל",
                        click: function () {
                            $(this).dialog("close");


                        }
                    }
                ]
            })
            buildNewExceptionTable();


            //disable
            $(".btnEditKtumaSmall").attr('class', 'btnEditDisabledKtumaSmall').attr('disabled', 'disabled');
            //$(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');

        }

        function buildNewExceptionTable() {
            var unitId = '<%=((pikudEntities.KtumaUser)Session["User"]).UnitId %>';
            tblData = $('#tblExceptionDataDialog');
            tblData.empty();

            trFirstRow = $('<tr class="dialogTR">');

            var tdRashutLabel = $('<td class="searchTdAlignRight">רשות</td>');
            var tdRashut = $('<td class="searchTdAlignRight"></td>');
            cboRashuyot = createRashuyotCombo(curRashuyotDT, "rashutId", tdRashut, -1, false, null, unitId, 250);
            
            tdRashut.append(cboRashuyot);
            trFirstRow.append(tdRashutLabel).append(tdRashut);
            tblData.append(trFirstRow)

            trSecondRow = $('<tr class="dialogTR">');

            var tdRParameterLabel = $('<td class="searchTdAlignRight">פרמטר</td>');
            var tdParameter = $('<td class="searchTdAlignRight"></td>');
            cboParameter = createCombo(curCombosDT, 'Parameter', tdParameter, -1, true, null, true, false, 250);
            tdParameter.append(cboParameter);
            trSecondRow.append(tdRParameterLabel).append(tdParameter);
            tblData.append(trSecondRow)

            trThirdRow = $('<tr class="dialogTR">');

            var tdInstituteLabel = $('<td class="searchTdAlignRight">שם מוסד</td>');
            var tdInstitute = $('<td class="searchTdAlignRight"></td>');
            var inputInstitute = $('<input type="textbox" id="INP_Institute_Edit" style="width:246px;">')
            tdInstitute.append(inputInstitute);
            trThirdRow.append(tdInstituteLabel).append(tdInstitute);
            tblData.append(trThirdRow)

            trFourthRow = $('<tr class="dialogTR">');
            var tdExceptionExplanationLabel = $('<td class="searchTdAlignRight">הסבר לבקשה</td>');
            var tdExceptionExplanation = $('<td class="searchTdAlignRight"></td>');
            var inputExceptionExplanation = $('<textarea rows="5"   id="INP_ExceptionExplanation_Edit"' + '" maxlength="250" style="width:243px;"   ></textarea>');
            tdExceptionExplanation.append(inputExceptionExplanation)
            trFourthRow.append(tdExceptionExplanationLabel).append(tdExceptionExplanation);
            tblData.append(trFourthRow);

            trFifthhRow = $('<tr class="dialogTR">');
            var tdSecurityTourLabel = $('<td class="searchTdAlignRight">האם בוצע סיור למקום</td>');
            var tdSecurityTour = $('<td class="searchTdAlignRight"></td>');
            cboSecurityTour = createCombo(curCombosDT, 'TourSecurity', tdSecurityTour, -1, true, null, false, false);
            tdSecurityTour.append(cboSecurityTour)
            trFifthhRow.append(tdSecurityTourLabel).append(tdSecurityTour);
            tblData.append(trFifthhRow);

            trSixthRow = $('<tr class="dialogTR">');
            var tdExceptionLabel = $('<td class="searchTdAlignRight">הבקשה</td>');
            var tdException = $('<td class="searchTdAlignRight"></td>');
            var inputException = $('<textarea rows="5"   id="INP_Exception_Edit"' + '" maxlength="250" style="width:243px;"   ></textarea>');
            tdException.append(inputException)
            trSixthRow.append(tdExceptionLabel).append(tdException);
            tblData.append(trSixthRow);

            trSeventhRow = $('<tr class="dialogTR">');
            var tdItgoenenutRegionToLabel = $('<td class="searchTdAlignRight">החרגה לאזור התגוננות</td>');
            var tdItgoenenutRegionTo = $('<td class="searchTdAlignRight"></td>');
            cboItgonenutRegionToId = createItgonenutRegionsCombo(curItgonenutRegionsDT, 'ItgonenutRegionExceptTo', tdItgoenenutRegionTo, -1, true, null, 250);
            tdItgoenenutRegionTo.append(cboItgonenutRegionToId)
            trSeventhRow.append(tdItgoenenutRegionToLabel).append(tdItgoenenutRegionTo);
            tblData.append(trSeventhRow);


            setDialogStyle('addRowConfirm', 'addRowCancel');

        }

        function fillTable(data,isFullRefresh) //
        {
            var userTypeId = $('#HID_UserTypeId').val();
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                    (//
                    function ()//
                    {
                        if (glbEditedLine_ExceptionFillRowId != -1 && glbEditedLine_ExceptionFillRowId == data[i].ExceptionFillRowId)//we have edited line so skip it
                        {
                            //  $(this).find('input#data_24_' + data[i].rashutid).val(glbEditedLine_Val24_BeforeRefresh);
                            //  $(this).find('input#data_48_' + data[i].rashutid).val(glbEditedLine_Val48_BeforeRefresh);
                            //  $(this).find('input#data_72_' + data[i].rashutid).val(glbEditedLine_Val72_BeforeRefresh);
                        }
                        else {
                            $('#TD_RashutName_' + data[i].ExceptionFillRowId).html(data[i].rashutName);
                            $('#TD_Itgonenut_Region_Name_' + data[i].ExceptionFillRowId).html(data[i].Name);
                            $('#TD_RecommendedItgonenutPolicy_' + data[i].ExceptionFillRowId).html(data[i].RecommendedItgonenutPolicyText);
                            setTDValTextAndColorShowTextForNull($('#TD_RecommendedItgonenutPolicy_' + data[i].ExceptionFillRowId), data[i].RecommendedItgonenutPolicy, data[i].RecommendedItgonenutPolicyText, true);
                            $('#TD_ExplanationToException_' + data[i].ExceptionFillRowId).html(data[i].ExplanationToException);
                            $('#TD_Exception_' + data[i].ExceptionFillRowId).html(data[i].Exception);
                            $('#TD_TourSecurity_' + data[i].ExceptionFillRowId).html(data[i].TourSecurityText);
                            $('#TD_InstitutionName_' + data[i].ExceptionFillRowId).html(data[i].InstitutionName);
                            $('#TD_Parameter_' + + data[i].ExceptionFillRowId).html(data[i].ParameterText);
                            $('#Inp_ItgonenutRegion_Id_' + data[i].ExceptionFillRowId).val(data[i].Itgonenut_Id);
                            $('#Inp_RashutId_' + data[i].ExceptionFillRowId).val(data[i].rashutId);
                            $('#Inp_Parameter_' + data[i].ExceptionFillRowId).val(data[i].Parameter);
                            $('#Inp_TourSecurity_' + data[i].ExceptionFillRowId).val(data[i].TourSecurity);
                            $('#Inp_ItgonenutRegionExceptTo_' + data[i].ExceptionFillRowId).val(data[i].ItgonenutRegionExceptTo);
                            $('#TD_ItgonenutRegionExceptTo_' + data[i].ExceptionFillRowId).text(data[i].regionsToName);
          

                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].ExceptionFillRowId).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].ExceptionFillRowId);
                                var strClick = createSTRClick(data[i].ExceptionFillRowId);
                                var strButton = createEditButtonNoExternal(data[i].ExceptionFillRowId, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].rashutid + strClick; //edit button (checkOut)
                                edit.html(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].rashutid).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].rashutid);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        if (data[i].user_name == null || data[i].user_name == undefined) {
                            data[i].user_name = '';
                        }
                        $(this).find('.data_user').html(data[i].user_name + setHiddenFieldsInRowByRow(data[i]));
                        i++;
                    }//
                    ); //END .each
            }

        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';

            if (data[i].ParameterText == null || data[i].ParameterText == undefined) {
                data[i].ParameterText = '';
            }
            if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
                data[i].RecommendedItgonenutPolicyText = '';
            }

            if (data[i].TourSecurityText == null || data[i].TourSecurityText == undefined) {
                data[i].TourSecurityText = '';
            }

            if (data[i].ParameterText == null || data[i].ParameterText == undefined) {
                data[i].ParameterText = '';
            }

            if (data[i].InstitutionName == null || data[i].InstitutionName == undefined) {
                data[i].InstitutionName = '';
            }
            if (data[i].ExplanationToException == null || data[i].ExplanationToException == undefined) {
                data[i].ExplanationToException = '';
            }
            if (data[i].regionsToName == null || data[i].regionsToName == undefined) {
                data[i].regionsToName = ''
            }
            if (data[i].Exception == null || data[i].Exception == undefined) {
                data[i].Exception = '';
            }
            if (data[i].SecurityTourText == null || data[i].SecurityTourText == undefined) {
                data[i].SecurityTourText = '';
            }
            row += '<td id="TD_Itgonenut_Region_Name_' + data[i].ExceptionFillRowId + '" class="">' + data[i].Name +'</td>';
            row += '<td id="TD_RashutName_' + data[i].ExceptionFillRowId + '" class="">' + data[i].rashutName  + '</td>';    
            bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].ExceptionFillRowId, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);
            row += '<td id="TD_Parameter_' + data[i].ExceptionFillRowId + '" class="">' + data[i].ParameterText +  '</td>';
            row += '<td id="TD_InstitutionName_' + data[i].ExceptionFillRowId + '" class="tdEditClass">' + data[i].InstitutionName  +'</td>';
            row += '<td id="TD_ExplanationToException_' + data[i].ExceptionFillRowId + '" class="tdEditClass">' + data[i].ExplanationToException + '</td>';
            row += '<td id="TD_TourSecurity_' + data[i].ExceptionFillRowId + '" class="">' + data[i].TourSecurityText + '</td>';
            row += '<td id="TD_Exception_' + data[i].ExceptionFillRowId + '" class="">' + data[i].Exception + '</td>';    
            row += '<td id="TD_ItgonenutRegionExceptTo_' + data[i].ExceptionFillRowId + '" class="">' + data[i].regionsToName + '</td>';    
            var strClick = createSTRClick(data[i].ExceptionFillRowId);
            var strClickUndo = createSTRUndoClick(data[i].ExceptionFillRowId);
            var strDelete = createSTRDeleteClick(data[i].ExceptionFillRowId);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton =  createDelAndEditButton(data[i].ExceptionFillRowId, disableAllEditButtons, strClick, strClickUndo, strDelete);            

            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if ('<%=Session["UserName"]%>' != '' && data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].ExceptionFillRowId)
                    strButton = createSaveButton(data[i].ExceptionFillRowId, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ExceptionFillRowId = data[i].ExceptionFillRowId;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].ExceptionFillRowId + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].ExceptionFillRowId + '">' + setHiddenFieldsInRowByRow(data[i]) + '</td > ';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].ExceptionFillRowId + '">' + data[i].user_name + setHiddenFieldsInRowByRow(data[i]) + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ExceptionFillRowId);
            }
        }

        function setHiddenFieldsInRow(ExceptionFillRowId, Itgonenut_Id, TourSecurity, ItgonenutRegionExceptTo, Parameter, rashutId) {
            return '<input id="Inp_ItgonenutRegion_Id_' + ExceptionFillRowId + '" type="hidden" value="' + Itgonenut_Id + '" />'
                + '<input id="Inp_TourSecurity_' + ExceptionFillRowId + '" type="hidden" value="' + TourSecurity + '" />'
                + '<input id="Inp_ItgonenutRegionExceptTo_' + ExceptionFillRowId + '" type="hidden" value="' + ItgonenutRegionExceptTo + '" />'
                + '<input id="Inp_Parameter_' + ExceptionFillRowId + '" type="hidden" value="' + Parameter + '" />'
                + '<input id="Inp_RashutId_' + ExceptionFillRowId + '" type="hidden" value="' + rashutId + '" />'
        }

        function setHiddenFieldsInRowByRow(row) {
            return setHiddenFieldsInRow(row.ExceptionFillRowId, row.Itgonenut_Id, row.TourSecurity, row.ItgonenutRegionExceptTo
                , row.Parameter, row.rashutId);
        }

        function deleteRow(rowId) {
             pageName = 'DistrictFill'
            var jsonObj =
                { page: pageName, op: 'deleteException', 'ExceptionFillRowId': rowId };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    alert('ההחרגה נמחקה בהצלחה')
                    window.location = window.location
                }
            });
        }

        function UndoCheckout(ExceptionFillRowId) //
        {

            pageName = 'ExceptionFill'
            var jsonObj =
                { page: pageName, op: 'undoCheckout', 'ExceptionFillRowId': ExceptionFillRowId };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_rashutIdName, glbEditedLine_InstitutionName_BeforeRefresh, glbEditedLine_ExplanationToException_BeforeRefresh
                , ExceptionFillRowId, glbEditedLine_SecurityTourText_BeforeRefresh, glbEditedLine_Exception_BeforeRefresh
                , glbEditedLine_ItgonenutRegionExceptToName_BeforeRefresh, glbEditedLine_ItgonenutIdName
                , glbEditedLine_ParameterText_BeforeRefresh, glbEditedLine_ItgonenutId,
                glbEditedLine_ItgonenutRegionExceptTo_BeforeRefresh, glbEditedLine_rashutId
                , glbEditedLine_Parameter_BeforeRefresh, glbEditedLine_SecurityTour_BeforeRefresh, 'btnEdit', 'btnEdit', '', 'TD_USERNAME_'
                , glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh.val, glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh.text);
        }

        function checkOutOrSave(ExceptionFillRowId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)
            var pageName = "ExceptionFill";
            if ($("#btnEdit" + ExceptionFillRowId).val() == 'ערוך')//
            {
                var jsonObj =
                    { page: pageName, op: 'checkout', 'exceptionFillRowId': ExceptionFillRowId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            var htmlINP = $('#TD_USERNAME_' + ExceptionFillRowId).html();
                            ShowSaveButtonsConfiguration(ExceptionFillRowId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName + htmlINP);

                            createEditDDL(ExceptionFillRowId);
                            glbEditedLine_ExceptionFillRowId = ExceptionFillRowId;
                            startTimerForUndoCheckout(ExceptionFillRowId, data_undoCheckoutTimeoutInMS)

                        }
                    }
                });
            }
            else//
            {
                var rashutId = $('#SELECT_rashutId_' + ExceptionFillRowId)
                var parameter = $('#SELECT_Parameter_' + ExceptionFillRowId)
                var instituteName = $('#INP_InstitutionName_' + ExceptionFillRowId)
                var explanationToException = $("#INP_ExplanationToException_" + ExceptionFillRowId)
                var securityTour = $("#SELECT_TourSecurity_" + ExceptionFillRowId)
                var exception = $("#INP_Exception_" + ExceptionFillRowId)
                var itgonenutRegionExceptTo = $('#SELECT_ItgonenutRegionExceptTo_' + ExceptionFillRowId)

                if ((rashutId.val() == '0')) {
                    alert('הכנס רשות');
                    return;
                }
                if ((exception.val() == '')) {
                    alert('הכנס החרגה');
                    return;
                }
                if ((explanationToException.val() == '')) {
                    alert('הכנס הסבר להחרגה');
                    return;
                }

                var jsonObj =
                {
                    page: pageName, "op": "save", "rashutId": rashutId.val(), "userId": '<%=Session["userID"]%>',
                    "parameter": parameter.val(), "instituteName": instituteName.val(), "explanationToException": explanationToException.val()
                    , "securityTour": securityTour.val(), "exception": exception.val(), 'itgonenutRegionExceptTo': itgonenutRegionExceptTo.val()
                    , 'exceptionFillRowId': ExceptionFillRowId
                };

                //console.log('1: ' + Date.now())
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else if (data[0].result == 'alreadyRashut') {
                            alert("לא ניתן לשמור - קיימת החרגה לרשות זו במערכת.");
                        }
                        else//
                        {
                            glbEditedLine_ExceptionFillRowId = -1;
                            resetEdit(data[0].rashutName, data[0].InstitutionName, data[0].ExplanationToException,
                                ExceptionFillRowId, data[0].TourSecurityText, data[0].Exception,
                                data[0].ItgonenutRegionToIdName, data[0].regionName, data[0].ParameterText,
                                data[0].ItgonenutId, data[0].ItgonenutRegionToId, data[0].rashutId
                                , data[0].Parameter, data[0].TourSecurity, 'btnEdit', 'btnEditDisabled', '', 'TD_USERNAME'
                                , data[0].RecommendedItgonenutPolicy, data[0].RecommendedItgonenutPolicyText);


                        }
                    }
                });
            }
        }

        function createEditDDL(ExceptionFillRowId)
        {
            
            
            var tdInstitution = $('#TD_InstitutionName_'+ ExceptionFillRowId);            
            
            
            var tdItgonenutRegionTo = $('#TD_ItgonenutRegionExceptTo_' + ExceptionFillRowId);
            var tdItgonenutId = $('#TD_Itgonenut_Region_Name_' + ExceptionFillRowId);

            var InpItgonenutRegion = $('#Inp_ItgonenutRegion_Id_' + ExceptionFillRowId);
            var InpItgonenutRegionExceptTo = $("#Inp_ItgonenutRegionExceptTo_" + ExceptionFillRowId);
            glbEditedLine_ItgonenutId = InpItgonenutRegion.val();
            glbEditedLine_ItgonenutIdName = tdItgonenutId.text();
            glbEditedLine_ItgonenutRegionExceptTo_BeforeRefresh = InpItgonenutRegionExceptTo.val();
            glbEditedLine_ItgonenutRegionExceptToName_BeforeRefresh = tdItgonenutRegionTo.text();

            var tdRashutId = $('#TD_RashutName_'+ ExceptionFillRowId);
            var InpRashutId = $('#Inp_RashutId_' + ExceptionFillRowId);
            glbEditedLine_rashutId = InpRashutId.val();
            glbEditedLine_rashutIdName = tdRashutId.text();

            var tdRecommendedItgonenutPolicy = $('#TD_RecommendedItgonenutPolicy_' + ExceptionFillRowId);
            setValuesForUndoCheckout(glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh, tdRecommendedItgonenutPolicy.html())
            var tdParameter = $('#TD_Parameter_' + ExceptionFillRowId);
            var InpParameter = $('#Inp_Parameter_' + ExceptionFillRowId);            
            
            glbEditedLine_Parameter_BeforeRefresh = InpParameter.val();
            glbEditedLine_ParameterText_BeforeRefresh = tdParameter.text();

            var tdExplanationRecommendedPolicy = $("#TD_ExplanationToException_" + ExceptionFillRowId);
            glbEditedLine_ExplanationToException_BeforeRefresh = tdExplanationRecommendedPolicy.html();
            glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh = tdExplanationRecommendedPolicy.html();

            var tdSecurityTour = $('#TD_TourSecurity_' + ExceptionFillRowId);
            var InpTourSecurity = $("#Inp_TourSecurity_" + ExceptionFillRowId);
            glbEditedLine_SecurityTour_BeforeRefresh = InpTourSecurity.val();
            glbEditedLine_SecurityTourText_BeforeRefresh = tdSecurityTour.text();
            glbEditedLine_InstitutionName_BeforeRefresh = tdInstitution.html();

            var tdException = $("#TD_Exception_" + ExceptionFillRowId);
            glbEditedLine_Exception_BeforeRefresh = tdException.html();
            //tdRecommendedItgonenutPolicy.empty();
            //tdRecommendedItgonenutPolicy.attr('class', 'tdEditClass');
            //tdItgonenutId.empty();
            //tdItgonenutId.attr('class', 'tdEditClass');
            tdSecurityTour.empty();
            tdSecurityTour.attr('class', 'tdEditClass');
            tdItgonenutRegionTo.empty();
            tdItgonenutRegionTo.attr('class', 'tdEditClass');
            

            tdRashutId.empty();
            tdRashutId.attr('class', 'tdEditClass');
            tdParameter.empty();
            tdParameter.attr('class', 'tdEditClass');
            tdInstitution.empty();
            tdInstitution.attr('class', 'tdEditClass');
            
            tdExplanationRecommendedPolicy.empty();
            tdExplanationRecommendedPolicy.attr('class', 'tdEditClass');
            tdException.empty();
            tdException.attr('class', 'tdEditClass');
            
            glbEditedLine_ItgonenutRegionExceptTo_BeforeRefresh = InpItgonenutRegionExceptTo.val();
            var unitId = '<%=((pikudEntities.KtumaUser)Session["User"]).UnitId %>';
            //cboItgonenutRegionId = createItgonenutRegionsCombo(curItgonenutRegionsDT, 'Itgonenut_Id', tdItgonenutId, ExceptionFillRowId, false, glbEditedLine_ItgonenutId);
            cboRashuyot = createRashuyotCombo(curRashuyotDT, "rashutId", tdRashutId, ExceptionFillRowId, false, glbEditedLine_rashutId, unitId, 150);
            createTextArea(tdExplanationRecommendedPolicy, 'ExplanationToException', ExceptionFillRowId, glbEditedLine_ExplanationToException_BeforeRefresh, 120);
            cboSecurityTour = createCombo(curCombosDT, 'TourSecurity', tdSecurityTour, ExceptionFillRowId, true, glbEditedLine_SecurityTour_BeforeRefresh, false, false);
            cboItgonenutRegionToId = createItgonenutRegionsCombo(curItgonenutRegionsDT, 'ItgonenutRegionExceptTo', tdItgonenutRegionTo, ExceptionFillRowId, true, glbEditedLine_ItgonenutRegionExceptTo_BeforeRefresh, 150);
            
            createTextArea(tdException, 'Exception', ExceptionFillRowId, glbEditedLine_Exception_BeforeRefresh, 120);
            createTextArea(tdInstitution, 'InstitutionName', ExceptionFillRowId, glbEditedLine_InstitutionName_BeforeRefresh, 120);
            //cboRecommendedItgonenutPolicy = createCombo(curCombosDT, 'RecommendedItgonenutPolicy', tdRecommendedItgonenutPolicy, ExceptionFillRowId, false, glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh.val, true, false);
            cboParameter = createCombo(curCombosDT, 'Parameter', tdParameter, ExceptionFillRowId, true, glbEditedLine_Parameter_BeforeRefresh, true, false);
            //createTextArea(tdInstitution, 'InstitutionName', ExceptionFillRowId, glbEditedLine_InstitutionName_BeforeRefresh, 120);
        }

        
        

        function resetEdit(rashutName, instituteName, explanationToException,
            ExceptionFillRowId, securityTourText, exception, itgonenutRegionExceptToName, itgonenutRegionName, parameterText
            , itgonenutRegionId, itgonenutRegionToId, rashutId, parameter, securityTour, btnEditId, editButtonsClass
            , btnUndoId, TdUserNameId,RecommendedItgonenutPolicy, RecommendedItgonenutPolicyText) {

            $("#" + btnEditId + ExceptionFillRowId).attr('class', 'btnEdit');
            $("#" + btnEditId + ExceptionFillRowId).val("ערוך");
            $("." + editButtonsClass).removeAttr('disabled').attr('class', 'btnEdit');
            $("#" + btnUndoId + ExceptionFillRowId).attr('class', 'btnSaveDispNone');
            
            bgClass = getClassByExactValuePop(RecommendedItgonenutPolicy);
            if (parameterText == null) {
                parameterText = '';
            }
            if (securityTourText == null) {
                securityTourText = '';
            }
            //$('#TD_Itgonenut_Region_Name_' + ExceptionFillRowId).text(itgonenutRegionName);
            $('#TD_RashutName_' + ExceptionFillRowId).text(rashutName);
            $('#TD_Parameter_' + ExceptionFillRowId).text(parameterText);
            $('#TD_InstitutionName_' + ExceptionFillRowId).text(instituteName);
            $('#TD_ItgonenutRegionExceptTo_' + ExceptionFillRowId).text(itgonenutRegionExceptToName);
            $('#TD_ExplanationToException_' + ExceptionFillRowId).text(explanationToException);
            $('#TD_TourSecurity_' + ExceptionFillRowId).text(securityTourText);
            $('#TD_Exception_' + ExceptionFillRowId).text(exception);
            $('#TD_ItgonenutRegionExceptTo_' + ExceptionFillRowId).text(itgonenutRegionExceptToName);
            $('#Inp_ItgonenutRegion_Id_' + ExceptionFillRowId).val(itgonenutRegionId);
            $('#Inp_RashutId_' + ExceptionFillRowId).val(rashutId);
            $('#Inp_Parameter_' + ExceptionFillRowId).val(parameter);
            $('#Inp_TourSecurity_' + ExceptionFillRowId).val(securityTour);
            $('#Inp_ItgonenutRegionExceptTo_' + ExceptionFillRowId).val(itgonenutRegionToId);            
            setTDValTextAndColor($('#TD_RecommendedItgonenutPolicy_' + ExceptionFillRowId), RecommendedItgonenutPolicy, RecommendedItgonenutPolicyText, true);
            $('#SELECT_ItgonenutRegionExceptTo_' + ExceptionFillRowId).val(itgonenutRegionToId);
            $('#' + TdUserNameId + '_' + ExceptionFillRowId).html(setHiddenFieldsInRow(ExceptionFillRowId, itgonenutRegionId, securityTour, itgonenutRegionToId, parameter, rashutId));
            

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }

        </script>  
       <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 8%;" />
                    <col style="width: 8%;" /> <!--16%-->
                    <col style="width: 15%;" /><!--31%-->
                    <col style="width: 10%;" /><!--41%-->
                    <col style="width: 10%;" /><!--51%-->
                    <col style="width: 10%;" /><!--61%-->
                    <col style="width: 7%;" /><!--68%-->
                    <col style="width:8%;" /> <!--76% -->
                    <col style="width:8%;" /> <!--84% -->
                    <col style="width:8%;" /> <!--92% -->
                    <col style="width:8%;" /> <!--100% -->
                </colgroup>
                <tr class="TBLHeader">
                      <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">שם הרשות
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>                  
                    <th class="TBLHeader">הפרמטר
                    </th>
                    <th class="TBLHeader">שם המוסד
                    </th>
                    <th class="TBLHeader">הסבר לבקשה
                    </th> 
                    <th class="TBLHeader">האם בוצע סיור איש<br /> מקצוע במקום
                    </th>
                    <th class="TBLHeader">הבקשה
                    </th>
                    <th class="TBLHeader">החרגה לאזור התגוננות
                    </th>
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>

                </tr>
            </table>
             <button type="button" id="BTN_AddException" onclick="addExceptionModal()"   value="הוסף החרגה" title="הוסף החרגה" style=" cursor:pointer; color:white; background-color:#FF9900; font-weight:bold;font-size:12px;height:24px; width:126px; margin: 20px 0px 20px 0px;" >הוסף החרגה</button>
        </div>
           <div id="dialog-Exceptionmodal" style="display:none">
            <table class="innerTable" id="tblExceptionDataDialog">
            </table>
        </div>
    </div>
</asp:Content>


