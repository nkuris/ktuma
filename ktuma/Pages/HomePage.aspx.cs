﻿using pikudEntities;
using pikudGui;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Pages_HomePage : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            {//readonly
                
                //string strFullUser = Request.LogonUserIdentity.Name;
                ////            strMachine = "\n" + "machine=" + Request.UserHostAddress;
                //if (strFullUser.IndexOf('\\') == -1)
                //{
                //    Session["userID"] = "guest";
                //}
                //else
                //{
                //    int indName = strFullUser.IndexOf('\\');
                //    Session["userID"] = strFullUser.Substring(indName + 1);
                //}
            }

        }
        initContorls();
    }

    private void initContorls()
    {
        BTN_Gila.Click += BTN_Gila_Click;
        BTN_Gila.AlternateText = "גילה והפצה";
        BTN_Aluf.Click += BTN_Aluf_Click;
        BTN_Aluf.AlternateText = "הנחיית אלוף";
        BTN_Final.Click += BTN_Final_Click;
        BTN_Final.AlternateText = "שקלול המרכיבים";
        BTN_Hagna.Click += BTN_Hagna_Click;
        BTN_Hagna.AlternateText = "הגנ\"א";
        BTN_Hanchaiut.Click += BTN_Hanchaiut_Click;
        BTN_Hanchaiut.AlternateText = "הנחיות";
        BTN_Markiv.Click += BTN_Markiv_Click;
        BTN_Markiv.AlternateText = "מרכיב האוכלוסיה";
        BTN_Migun.Click += BTN_Migun_Click;
        BTN_Migun.AlternateText = "מיגון";
        BTN_Mod.Click += BTN_Mod_Click;
        BTN_Mod.AlternateText = "מודיעין";
        BTN_ItgonenutByMidrag.Click += BTN_ItgonenutByMidrag_Click;
        BTN_ItgonenutByMidrag.AlternateText = "אזורי התגוננות ע\"פי מדרג";
        //BTN_Shigra.Click += BTN_Shigra_Click;
        //BTN_Shigra.AlternateText = "התרעה";
        BTN_Haatra.Click += BTN_Haatra_Click;
        BTN_Haatra.AlternateText = "התרעה";
        

    }

    void BTN_Haatra_Click(object sender, ImageClickEventArgs e)
    {
        transferToPageFromMain("Hatraa.aspx");
    }

    void BTN_Shigra_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Shigra.aspx");
    }

    void BTN_ItgonenutByMidrag_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("MidragFinal.aspx");
    }

    void BTN_Mod_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Mod.aspx");
    }

    void BTN_Migun_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Migun.aspx");
    }

    void BTN_Markiv_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Population.aspx");
    }

    void BTN_Hanchaiut_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Hanchaiut.aspx");
    }

    void BTN_Hagna_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Hagna.aspx");
    }

    void BTN_Final_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Final.aspx");
    }

    void BTN_Aluf_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Aluf.aspx");
    }

    void BTN_Gila_Click(object sender, EventArgs e)
    {
        transferToPageFromMain("Gila.aspx");
    }


    private void transferToPageFromMain(string pageName)
    {
        Response.Redirect(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName, true);
        //Server.Transfer(ConfigurationManager.AppSettings["pagesFolderPath"] + pageName);
    }

}
