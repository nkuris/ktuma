﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Heading.Master"
     AutoEventWireup="true" CodeFile="Mod.aspx.cs" Inherits="Pages_Mod" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_Haka_BeforeRefresh = {};
        var glbEditedLine_Dapa_BeforeRefresh = {};
        var glbEditedLine_Teshen_BeforeRefresh = {};
        var glbEditedLine_Region_Factor_BeforeRefresh = {};
        var glbEditedLine_Explantion_BeforeRefresh = '';
        var glbEditedLine_Harigim_BeforeRefresh = '';
        var glbEditedLine_Comander_Affect_BeforeRefresh = {};
        var glbEditedLine_FinalGrade_BeforeRefresh = {};
        $(document).ready(function ()//
        {
            loadCombosMod();
            var uid = '<%=Session["userID"]%>'
            checkSession(uid);
            loadParams(uid,true, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });

        function loadCombosMod() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Mod';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;
                }
            });


        }



        function refresh(isFullRefresh) //
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var jsonObj =
                    { op: 'loadMod', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data, isFullRefresh);

                }
            });
        }

        function fillTable(data, isFullRefresh) //
        {
           var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           

            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                            //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                            //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                            //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                        }
                        else {
                            setTDValTextAndColor($('#TD_Haka_' + data[i].Itgonenut_Id), data[i].HakaValue, data[i].HakaText);
                            setTDValTextAndColor($('#TD_Dapa_' + data[i].Itgonenut_Id), data[i].DapaValue, data[i].DapaText);
                            setTDValTextAndColor($('#TD_Teshen_' + data[i].Itgonenut_Id), data[i].TeshenValue, data[i].TeshenText);
                            setTDValTextAndColor($('#TD_Region_Factor_' + data[i].Itgonenut_Id), data[i].Region_FactorValue, data[i].Region_FactorText);
                            setTDValTextAndColor($('#TD_Comander_Affect_' + data[i].Itgonenut_Id), data[i].ComanderAffectValue, data[i].ComanderAffectText);
                            setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                            $('#TD_Explantion_' + data[i].Itgonenut_Id).html(data[i].Explantion);
                            $('#TD_Harigim_' + data[i].Itgonenut_Id).html(data[i].Harigim); var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);

                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValue(data[i].HakaValue);            
            row += createOrigTD(bgClass, 'Haka', data[i].Itgonenut_Id, data[i].HakaText, data[i].HakaValue);
            bgClass = getClassByExactValue(data[i].DapaValue);            
            row += createOrigTD(bgClass, 'Dapa', data[i].Itgonenut_Id, data[i].DapaText, data[i].DapaValue);
            bgClass = getClassByExactValue(data[i].TeshenValue);            
            row += createOrigTD(bgClass, 'Teshen', data[i].Itgonenut_Id, data[i].TeshenText, data[i].TeshenValue);
            bgClass = getClassByExactValue(data[i].Region_FactorValue);            
            row += createOrigTD(bgClass, 'Region_Factor', data[i].Itgonenut_Id, data[i].Region_FactorText, data[i].Region_FactorValue);
            
            if (data[i].ComanderAffectText == null || data[i].ComanderAffectText == undefined) {
                data[i].ComanderAffectText = '';
            }
            if (data[i].ComanderAffectValue == null || data[i].ComanderAffectValue == undefined) {
                data[i].ComanderAffectValue = '';
            }

            if (data[i].Explantion == null || data[i].Explantion == undefined) {
                data[i].Explantion = '';
            }
            if (data[i].Harigim == null || data[i].Harigim == undefined) {
                data[i].Harigim = '';
            }

            if (data[i].Explantion == null || data[i].Explantion == undefined) {
                data[i].Explantion = '';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Harigim', data[i].Harigim);
            if (data[i].ComanderAffectValue == '') {
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" ></td>';
            }
            else {
                bgClass = getClassByExactValue(data[i].ComanderAffectValue);
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].ComanderAffectValue + ' - ' + data[i].ComanderAffectText + '</td>';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explantion', data[i].Explantion);            
            bgClass = getClassByExactValue(data[i].FinalGradeValue);            
            row += createOrigTD(bgClass, 'Final_Grade', data[i].Itgonenut_Id, data[i].FinalGradeText, data[i].FinalGradeValue);


            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);
            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if (data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }


        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }

        function createEditDDL(ItgonenutId) {

            var tdHaka = $('#TD_Haka_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Haka_BeforeRefresh, tdHaka.html())
            var tdDapa = $('#TD_Dapa_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Dapa_BeforeRefresh, tdDapa.html())
            var tdTeshen = $('#TD_Teshen_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Teshen_BeforeRefresh, tdTeshen.html())
            var tdComanderAffect = $("#TD_Comander_Affect_" + ItgonenutId)
            setValuesForUndoCheckout(glbEditedLine_Comander_Affect_BeforeRefresh, tdComanderAffect.html())
            var tdRegion_Factor = $("#TD_Region_Factor_" + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Region_Factor_BeforeRefresh, tdRegion_Factor.html())
            var tdExplantion = $("#TD_Explantion_" + ItgonenutId);
            glbEditedLine_Explantion_BeforeRefresh = tdExplantion.html();
            var tdHarigim= $("#TD_Harigim_" + ItgonenutId);
            glbEditedLine_Harigim_BeforeRefresh = tdHarigim.html();
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())
            tdHaka.empty();
            tdHaka.attr('class', 'tdEditClass');
            tdDapa.empty();
            tdDapa.attr('class', 'tdEditClass');
            tdTeshen.empty();
            tdTeshen.attr('class', 'tdEditClass');
            tdComanderAffect.empty();
            tdComanderAffect.attr('class', 'tdEditClass');
            tdRegion_Factor.empty();
            tdRegion_Factor.attr('class', 'tdEditClass');
            tdExplantion.empty();
            tdExplantion.attr('class', 'tdEditClass');
            tdHarigim.empty();
            tdHarigim.attr('class', 'tdEditClass');
            cboKHaka = createCombo(curCombosDT, 'Haka', tdHaka, ItgonenutId, false, glbEditedLine_Haka_BeforeRefresh.val, true, false);
            cboDapa = createCombo(curCombosDT, 'Dapa', tdDapa, ItgonenutId, false, glbEditedLine_Dapa_BeforeRefresh.val, true, false);
            cboTeshen = createCombo(curCombosDT, 'Teshen', tdTeshen, ItgonenutId, false, glbEditedLine_Teshen_BeforeRefresh.val, true, false);
            cboRegion_Factor = createCombo(curCombosDT, 'Region_Factor', tdRegion_Factor, ItgonenutId, false, glbEditedLine_Region_Factor_BeforeRefresh.val, true, false);
            cboComander_Affect = createCombo(curCombosDT, 'Comander_Affect', tdComanderAffect, ItgonenutId, true, glbEditedLine_Comander_Affect_BeforeRefresh.val, true, false);
            createTextArea(tdExplantion, 'Explantion', ItgonenutId, glbEditedLine_Explantion_BeforeRefresh, 130);
            createTextArea(tdHarigim, 'Harigim', ItgonenutId, glbEditedLine_Harigim_BeforeRefresh, 130);
        }

        function checkLower100(obj) {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }


        function checkOutOrSave(ItgonenutId) {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Mod';
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName)
                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                        }
                    }
                });
            }
            else//
            {
                var haka = {};
                var dapa = {};
                var teshen = {};
                var region_factor = {};
                var comander_Affect = {};
                setValuesForUndoCheckoutFromTextOnly(haka, "Haka", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(dapa, "Dapa", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(teshen, "Teshen", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(region_factor, "Region_Factor", ItgonenutId)
                var explantion = $("#INP_Explantion_" + ItgonenutId).val();
                var harigim = $("#INP_Harigim_" + ItgonenutId).val();

                setValuesForUndoCheckoutFromTextOnly(comander_Affect, "Comander_Affect", ItgonenutId);
                if ((comander_Affect.val != '0' && comander_Affect.val != '') && explantion == '') {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }
                var jsonObj =
                        {
                            page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>',
                            "haka": haka.val, "dapa": dapa.val, "teshen": teshen.val, "region_factor": region_factor.val,
                            "explantion": explantion, "comander_Affect": comander_Affect.val, "harigim": harigim
                        };
                //alert(JSON.stringify(jsonObj));

                //console.log('1: ' +Date.now())
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
                            //console.log('2: ' + Date.now())
                            var finalGrade = { val: data[0].Final_Grade, text: data[0].ResourceText };
                            resetEdit(haka, dapa, teshen, region_factor, explantion, comander_Affect, finalGrade, harigim, ItgonenutId);


                            // enableDisbaleUserLine(userType, 'disable');
                            //$("#cph_region" + userType + "_EditedBy").val("");//after save clear my user from "active user cell"
                        }
                    }
                });
            }
        }

        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Mod_Edit'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Haka_BeforeRefresh, glbEditedLine_Dapa_BeforeRefresh,
                glbEditedLine_Teshen_BeforeRefresh, glbEditedLine_Region_Factor_BeforeRefresh,
                 glbEditedLine_Explantion_BeforeRefresh, glbEditedLine_Comander_Affect_BeforeRefresh,
                 glbEditedLine_FinalGrade_BeforeRefresh, glbEditedLine_Harigim_BeforeRefresh, Itgonenut_Id);
        }




        function resetEdit(haka, dapa, teshen, region_factor, explantion, comander_Affect,
                 finalGrade, harigim, Itgonenut_Id) {
            //console.log('3: ' + Date.now())
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            setTDValTextAndColorObj($('#TD_Haka_' + glbEditedLine_ItgonenutId), haka, false);
            setTDValTextAndColorObj($('#TD_Dapa_' + glbEditedLine_ItgonenutId), dapa, false);
            setTDValTextAndColorObj($('#TD_Teshen_' + glbEditedLine_ItgonenutId), teshen, false);
            setTDValTextAndColorObj($('#TD_Comander_Affect_' + glbEditedLine_ItgonenutId), comander_Affect, false);
            setTDValTextAndColorObj($('#TD_Region_Factor_' + glbEditedLine_ItgonenutId), region_factor, false);
            $('#TD_Explantion_' + glbEditedLine_ItgonenutId).html(explantion);
            $('#TD_Harigim_' + glbEditedLine_ItgonenutId).html(harigim);
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade, false);
           // console.log('4: ' + Date.now())
            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>
      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/מודיעין-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; מודיעין</span>
       </div>
      <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 8%;" /><!-- 20%-->
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" /><!-- 44% פקטור אזורי-->
                    <col style="width: 13%;" /><!-- 59%-->
                    <col style="width: 6%;" /><!-- 65%-->

                    <col style="width: 15%;" /><!-- 80%-->

                    <col style="width: 6%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">הכ"א
                    </th>  
                     <th class="TBLHeader">דפ"א
                    </th>
                     <th class="TBLHeader">תש"ן
                    </th>                
                    <th class="TBLHeader">פקטור אזורי
                    </th>
                    <th class="TBLHeader">"ידיעת זהב"
                    </th>
                    <th class="TBLHeader">הערכת מפקד
                    </th>
                    <th class="TBLHeader">הסבר (מלל חופשי)</th>
                    <th class="TBLHeader">ציון סופי</th>
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
