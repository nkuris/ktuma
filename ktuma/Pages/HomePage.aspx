﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master"
     CodeFile="HomePage.aspx.cs" Inherits="Pages_HomePage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="text-align:center;margin-top:40px;"> 
        <asp:ImageButton ID="BTN_Migun" runat="server"   CssClass="mainBTN"  ImageUrl="~/Images/מיגון-01.png"/>                
        <asp:ImageButton ID="BTN_Haatra" runat="server" CssClass="mainBTN"  ImageUrl="~/Images/התרעה-01.png"/>
        <asp:ImageButton ID="BTN_Gila" runat="server"  CssClass="mainBTN"  ImageUrl="~/Images/גילה והפצה-01.png"/>        
        <asp:ImageButton ID="BTN_Hagna" runat="server" CssClass="mainBTN"  ImageUrl="~/Images/הגנא-01.png"/>        
        <asp:ImageButton ID="BTN_Mod" runat="server"  CssClass="mainBTN"  ImageUrl="~/Images/מודיעין-01.png"/>         
        </div>
    <div style="text-align:center; margin-top:40px;">
        <asp:ImageButton ID="BTN_ItgonenutByMidrag" CssClass="mainBTN" runat="server"  ImageUrl="~/Images/מדיניות התגוננות מומלצת-01.png"/>
        <asp:ImageButton ID="BTN_Aluf" runat="server" CssClass="mainBTN"   ImageUrl="~/Images/החלטת אלוף-01.png" />
        <asp:ImageButton ID="BTN_Final" runat="server" CssClass="mainBTN"   ImageUrl="~/Images/שקלול המרכיבים-01.png"/>        
        <asp:ImageButton ID="BTN_Hanchaiut" runat="server" CssClass="mainBTN"   ImageUrl="~/Images/הנחיות-01.png" />
        <asp:ImageButton ID="BTN_Markiv" runat="server" CssClass="mainBTN" ImageUrl="~/Images/אוכלוסייה-01.png" />
        
    </div>   
       <div id="dialog-modal" style="display:none">
            <table class="innerTable" id="tblDataDialog">
            </table>
        </div>
    <script type="text/javascript">

        function buildLoginModal() {
            $("#dialog-modal").dialog({
                height: 250,
                width: 350,
                closeOnEscape: false,
                modal: true,
                title:"הזן פרטי משתמש",
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                },
                buttons: [
                                    {
                                        id: "addRowConfirm",
                                        text: "אשר",
                                        click: function () {
                                            if (checkLoginInput('INP_UserName_Edit','INP_Password_Edit'))
                                            {
                                                if (Login()) {
                                                    $(this).dialog("close");
                                                }
                                               
                                            }
                                           

                                        }
                                    },
                                      {
                                          id: "addRowCancel",
                                          text: "בטל",
                                          click: function () {
                                              $(this).dialog("close");
                                          

                                          }
                                      }
                ]
            })
           buildEditTable();


            //disable
            $(".btnEditKtumaSmall").attr('class', 'btnEditDisabledKtumaSmall').attr('disabled', 'disabled');
            //$(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');


        }
       

        function Logout() {
            var jsonObj =
                   {
                       "op": "Logout"
                   };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);

                    window.location = window.location;
                    return true;

                }
            });
        }

        function Login()
        {
            var username = $('#INP_UserName_Edit').val();
            var password = $('#INP_Password_Edit').val();
            var jsonObj =
            {
                "op": "Login", "username": username,
                "password": password, "ktumaPikudBoolean": "1"
            };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);
                    if (data == false) //
                    {
                        alert("הנתונים שהוזנו לא נכונים, אנא הזן שוב");
                        return false;
                    }
                    else if(data.IsActive == false)
                    {
                        alert("המשתמש איננו פעיל");
                        return false;
                    }
                    else//
                    {
                        window.location = window.location;
                        return true;
                    }
                }
            });
        }

        function buildEditTable() {
            tblData = $('#tblDataDialog');
            tblData.empty();

            trFirstRow = $('<tr class="dialogTR">');

            var tdNameLabel = $('<td class="searchTdCenterBold">שם משתמש</td>');
            var inputName = $('<input type="textbox" id="INP_UserName_Edit" style=width:150px;">')
            var tdNameInp = $('<td class="searchTdCenterBold">');
            tdNameInp.append(inputName);

            trSecondRow = $('<tr class="dialogTR">');
            var tdPasswordLabel = $('<td class="searchTdCenterBold">סיסמא</td>');
            var inputPassword = $('<input type="textbox" id="INP_Password_Edit" style=width:150px;">')
            var tdPasswordInp = $('<td class="searchTdCenterBold">');
            tdPasswordInp.append(inputPassword);

            trFirstRow.append(tdNameLabel).append(tdNameInp);
            trSecondRow.append(tdPasswordLabel).append(tdPasswordInp);
            tblData.append(trFirstRow).append(trSecondRow).append;
            setDialogStyle('addRowConfirm', 'addRowCancel');

        }
        $(document).ready(function () {


            var userTypeId = $('#HID_UserTypeId').val();

           var userGroupId = '<%=Session["UserGroupId"]%>';
            $('#BTN_GoToLogin').css('display', 'inline-block');
            if (userTypeId != "-1") {
                if (userTypeId == "1" && userGroupId == "1") {
                    $('#BTN_GoToAdmin').css('display', 'inline-block');
                }
                $('#BTN_GoToLogin').val("התנתק");
                $('#BTN_GoToLogin').text("התנתק");
                $('#BTN_GoToLogin').click(function () {
                    Logout();
                });
                $('#BTN_GoToAdmin').click(function () {
                    window.location = "settings.aspx";
                })
            }
            else {
                $('#BTN_GoToAdmin').css('display', 'none');

                $('#BTN_GoToLogin').click(function () {
                    buildLoginModal();
                });
            }


            $('#BTN_ReturnHome').css('display', 'none');

        });

    

    </script>
</asp:Content>