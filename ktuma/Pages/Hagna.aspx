﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Hagna.aspx.cs" 
    MasterPageFile="~/MasterPages/Heading.Master"  Inherits="Pages_Hagna" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_Kium_BeforeRefresh = {};
        var glbEditedLine_Mediniut_BeforeRefresh = {};
        var glbEditedLine_Efectiviut_BeforeRefresh = {};
        var glbEditedLine_Prisa_BeforeRefresh = {};
        var glbEditedLine_Explantion_BeforeRefresh = '';
        var glbEditedLine_Comander_Affect_BeforeRefresh = {};
        var glbEditedLine_FinalGrade_BeforeRefresh = {};

        $(document).ready(function ()//
        {
            loadCombosHagna();
            var uid = '<%=Session["userID"]%>';
            checkSession(uid);
            loadParams('<%=Session["userID"]%>', true, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });

        function loadCombosHagna() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Hagna';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


        }

        function refresh(isFullRefresh) //
        {
         
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var jsonObj =
                    { op: 'loadHagna', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data, isFullRefresh);

                }
            });
        }

        function fillTable(data, isFullRefresh) //
        {

            var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                            //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                            //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                            //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                        }
                        else {
                            setTDValTextAndColor($('#TD_Kium_' + data[i].Itgonenut_Id), data[i].KiumValue, data[i].KiumText);
                            setTDValTextAndColor($('#TD_Mediniut_' + data[i].Itgonenut_Id), data[i].MediniutValue, data[i].MediniutText);
                            setTDValTextAndColor($('#TD_Efectiviut_' + data[i].Itgonenut_Id), data[i].EfectiviutValue, data[i].EfectiviutText);
                            setTDValTextAndColor($('#TD_Comander_Affect_' + data[i].Itgonenut_Id), data[i].ComanderAffectValue, data[i].ComanderAffectText);
                            setTDValTextAndColor($('#TD_Prisa_' + data[i].Itgonenut_Id), data[i].PrisaValue, data[i].PrisaText);
                            $('#TD_Explantion_' + data[i].Itgonenut_Id).html(data[i].Explantion);
                            setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                           if((data[i].user_name == '' || data[i].user_name == null) &&  $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '')
                           {//add edit button
                               var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                               var strClick = createSTRClick(data[i].Itgonenut_Id);
                               var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);

                               //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                               edit.append(strButton);
                           }
                            else if((data[i].user_name != '' && data[i].user_name != null) &&  $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '')
                            {//remove edit button
                                 var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValue(data[i].KiumValue);
            row += createOrigTD(bgClass, 'Kium', data[i].Itgonenut_Id, data[i].KiumText, data[i].KiumValue);
            bgClass = getClassByExactValue(data[i].MediniutValue);
            row += createOrigTD(bgClass, 'Mediniut', data[i].Itgonenut_Id, data[i].MediniutText, data[i].MediniutValue);
            bgClass = getClassByExactValue(data[i].EfectiviutValue);
            row += createOrigTD(bgClass, 'Efectiviut', data[i].Itgonenut_Id, data[i].EfectiviutText, data[i].EfectiviutValue);
            bgClass = getClassByExactValue(data[i].PrisaValue);
            row += createOrigTD(bgClass, 'Prisa', data[i].Itgonenut_Id, data[i].PrisaText, data[i].PrisaValue);

            if (data[i].Commander_Affect == null || data[i].Commander_Affect == undefined) {
                data[i].Commander_Affect = '';
            }

            if (data[i].Explantion == null || data[i].Explantion == undefined) {
                data[i].Explantion = '';
            }

            if (data[i].ComanderAffectValue == '' || data[i].ComanderAffectValue == null) {
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" ></td>';
            }
            else {
                bgClass = getClassByExactValue(data[i].ComanderAffectValue);
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].ComanderAffectValue + ' - ' + data[i].ComanderAffectText + '</td>';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explantion', data[i].Explantion);

            bgClass = getClassByExactValue(data[i].Final_Grade);
            row += createOrigTD(bgClass, 'Final_Grade', data[i].Itgonenut_Id, data[i].FinalGradeText, data[i].FinalGradeValue);

            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);
            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if (data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }

            }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }

     //make 3 values textboxes with color ranges
     function createTdColoredData(dataRow, fieldId) //
     {
         //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
         editValue = dataRow[fieldId]; //original value from db

         var disabled = 'disabled="disabled"';
         //(time_gear=-1) = disabled     (time_gear=1) = enabled  
         if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
         {
             disabled = "";
         }

         //value textBox by fieldId with colors
         if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
         {
             return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
         }
         else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
         {
             return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
         }
         else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
         {
             return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
         }
     }

     function UndoCheckout(Itgonenut_Id) //
     {
         
         pageName = 'Hagna'
         var jsonObj =
                 { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
         $.ajax({
             type: "POST",
             url: "../Handler.ashx",
             data: jsonObj,
             dataType: "json",
             failure: function (response) {
                 alert(response.d);
             },
             success: function (data) {
                 //console.log(data);
                 //fillTable(data);
             }
         });
         resetEdit(glbEditedLine_Kium_BeforeRefresh, glbEditedLine_Mediniut_BeforeRefresh,
             glbEditedLine_Efectiviut_BeforeRefresh, glbEditedLine_Prisa_BeforeRefresh,
              glbEditedLine_Explantion_BeforeRefresh, glbEditedLine_Comander_Affect_BeforeRefresh,
              glbEditedLine_FinalGrade_BeforeRefresh,Itgonenut_Id);
     }


     function checkLower100(obj)//
     {
         if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
         {
             alert('יש להזין מספר בין 0 ל 100');
         }
     }


     function checkOutOrSave(ItgonenutId)//
     {
         var userName = '<%=Session["UserName"]%>';
         checkSession(userName);
         var pageName = 'Hagna';
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                           
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName)
                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                        }
                    }
                });
            }
            else//
            {
                var kium = {};
                var mediniut = {};
                var efectiviut = {};
                var prisa = {};
                var comander_Affect = {};
                setValuesForUndoCheckoutFromTextOnly(kium, "Kium", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(mediniut, "Mediniut", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(efectiviut, "Efectiviut", ItgonenutId)
                setValuesForUndoCheckoutFromTextOnly(prisa, "Prisa", ItgonenutId)
                var explantion = $("#INP_Explantion_" + ItgonenutId).val();
                setValuesForUndoCheckoutFromTextOnly(comander_Affect, "Comander_Affect", ItgonenutId);
                if ((comander_Affect.val != '0' && comander_Affect.val != '') && explantion == '') {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }
              
                var jsonObj =
                        {
                            page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>',
                            "kium": kium.val, "mediniut": mediniut.val, "efectiviut": efectiviut.val, "prisa": prisa.val,
                            "explantion": explantion, "comander_Affect": comander_Affect.val
                        };
                //alert(JSON.stringify(jsonObj));
               


                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
                            var finalGrade = {val: data[0].Final_Grade, text:data[0].ResourceText};
                            resetEdit(kium, efectiviut, mediniut, prisa, explantion, comander_Affect, finalGrade, ItgonenutId);

                       
                        }
                    }
                });
            }
        }

        function createEditDDL(ItgonenutId) {   
           
            var tdKium = $('#TD_Kium_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Kium_BeforeRefresh, tdKium.html())        
            var tdMediniut = $('#TD_Mediniut_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Mediniut_BeforeRefresh, tdMediniut.html())
            var tdEfectiviut = $('#TD_Efectiviut_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Efectiviut_BeforeRefresh, tdEfectiviut.html())   
            var tdComanderAffect = $("#TD_Comander_Affect_" + ItgonenutId)
            setValuesForUndoCheckout(glbEditedLine_Comander_Affect_BeforeRefresh,tdComanderAffect.html()) 
            var tdPrisa = $("#TD_Prisa_" + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Prisa_BeforeRefresh, tdPrisa.html())     
            var tdExplantion = $("#TD_Explantion_" + ItgonenutId);
            glbEditedLine_Explantion_BeforeRefresh = tdExplantion.html();
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())     
            tdKium.empty();
            tdKium.attr('class', 'tdEditClass');
            tdMediniut.empty();
            tdMediniut.attr('class', 'tdEditClass');
            tdEfectiviut.empty();
            tdEfectiviut.attr('class', 'tdEditClass');
            tdComanderAffect.empty();
            tdComanderAffect.attr('class', 'tdEditClass');
            tdPrisa.empty();
            tdPrisa.attr('class', 'tdEditClass');
            tdExplantion.empty();
            tdExplantion.attr('class', 'tdEditClass');
            cboKium = createCombo(curCombosDT, 'Kium', tdKium, ItgonenutId, false, glbEditedLine_Kium_BeforeRefresh.val, true);
            cboMediniut = createCombo(curCombosDT, 'Mediniut', tdMediniut, ItgonenutId, false,glbEditedLine_Mediniut_BeforeRefresh.val, true);
            cboEfectiviut = createCombo(curCombosDT, 'Efectiviut', tdEfectiviut, ItgonenutId, false, glbEditedLine_Efectiviut_BeforeRefresh.val, true);
            cboPrisa = createCombo(curCombosDT, 'Prisa', tdPrisa, ItgonenutId, false, glbEditedLine_Prisa_BeforeRefresh.val, true);
            cboComander_Affect = createCombo(curCombosDT, 'Comander_Affect', tdComanderAffect, ItgonenutId, true, glbEditedLine_Comander_Affect_BeforeRefresh.val, true);
            createTextArea(tdExplantion, 'Explantion', ItgonenutId, glbEditedLine_Explantion_BeforeRefresh, 100);
        }


        function resetEdit(kium, efectiviut, mediniut, prisa, explantion,
            comander_Affect, finalGrade,ItgonenutId)
        {
            
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')

            setTDValTextAndColorObj($('#TD_Kium_' + glbEditedLine_ItgonenutId), kium, false);
            setTDValTextAndColorObj($('#TD_Mediniut_' + glbEditedLine_ItgonenutId), mediniut, false);
            setTDValTextAndColorObj($('#TD_Efectiviut_' + glbEditedLine_ItgonenutId), efectiviut, false);
            setTDValTextAndColorObj($('#TD_Comander_Affect_' + glbEditedLine_ItgonenutId), comander_Affect, false);
            setTDValTextAndColorObj($('#TD_Prisa_' + glbEditedLine_ItgonenutId), prisa, false);
            $('#TD_Explantion_' + glbEditedLine_ItgonenutId).html(explantion);
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade, false);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>
     <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/הגנא-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; הגנ"א</span>
       </div>
      <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 8%;" /><!-- 20%-->
                    <col style="width: 10%;" />
                    <col style="width: 10%;" /><!-- 40%-->
                    <col style="width: 10%;" />

                    <col style="width: 8%;" /><!-- 58%-->

                    <col style="width:100px;" />
                   <col style="width: 8%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">קיום וכשירות מערכות היירוט
                    </th>                  
                    <th class="TBLHeader">מדיניות היירוט
                    </th>
                    <th class="TBLHeader">אפקטיביות היירוט יממה אחרונה
                    </th>
                    <th class="TBLHeader">שינויי פריסה צפויים
                    </th>
                    <th class="TBLHeader">הערכת מפקד</th>
                    <th class="TBLHeader">הסבר (מלל חופשי)</th>
                    <th class="TBLHeader">ציון סופי</th>
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
