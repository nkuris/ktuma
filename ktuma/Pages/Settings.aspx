﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Heading.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Pages_Settings" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="direction:rtl;">
      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->              
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; ניהול</span>
          </div>
     <div style="margin:20px; font-size:18px; ">
        <span class="ui-icon  ui-icon-plusthick" id="SPN_UsersAdmin" style="cursor:pointer;"></span>ניהול משתמשים
        </div>
      <fieldset id="FLD_UsersAdmin" style =" direction:rtl; display:none; width:90%; margin:auto; height:500px;overflow-y:auto;">
        <legend>ניהול משתמשים</legend>   
         <input type="button" id="INP_AddUser"
             value="הזן משתמש" onclick="EditInsertModalUser(true, -1)" class="btnEditWidthKtuma" style="margin-bottom:10px;" />
        <input type="text" id="INP_SearchUser" style="margin-bottom:10px;width:200px;" class="inputReg" />
        <span class="ui-icon ui-icon-search" id="BTN_SearchUser" onclick="SearchUser()" style="cursor:pointer;"></span>
         <table style="width: 100%" class="tblRes" id="tblDataUsers" >
                    <tr class="TBLHeader">
                        <th class="TBLHeader">מזהה משתמש</th>
                        <th class="TBLHeader">שם משתמש</th>
                        <th class="TBLHeader">סיסמא</th>
                        <th class="TBLHeader">גורם אחראי</th>
                        <th class="TBLHeader">קבוצה</th>
                        <th class="TBLHeader">סוג משתמש</th>
                        <th class="TBLHeader">משתמש מעדכן</th>
                        <th class="TBLHeader">תאריך עדכון</th>
                        <th class="TBLHeader">פעיל</th>
                        <th class="TBLHeader"></th>
                    </tr>
             </table>
   
        <div id="dialog-modalUsers" style="display:none">
            <table class="innerTable" id="tblDataDialogUsers">
            </table>
        </div>
    </fieldset>
     <div style="margin:20px; font-size:18px; ">
        <span class="ui-icon  ui-icon-plusthick" id="SPN_ResourcesAdmin" style="cursor:pointer;"></span>ניהול תכנים
     </div>
     <fieldset id="FLD_ResourcesAdmin" style =" direction:rtl; display:none; width:90%; margin:auto; height:500px;overflow-y:auto;">
        <legend>ניהול תכנים</legend>
         <div style="text-align:center; margin-top:20px;">
         
        <table style =" width:800px;text-align:center; margin:auto;" class="tblRes">
                <tr>
                        <td class="TDboldNoBorder">
                            עמוד
                        </td>
                        <td class="TDboldNoBorder">
                           <select  id="DDL_Screen" onchange="findScreenCombo()" style="width:200px;"></select>
                        </td>
                 </tr>
            <tr>
                        <td class="TDboldNoBorder">
                            תאור הקומבו
                        </td>
                        <td class="TDboldNoBorder">
                            <select id="DDL_PageCombo" style="width:200px;"  onchange="loadComboResources()"></select>
                        </td>
                 </tr>
            <tr>                 
         </table>
        <div style="width:800px; margin:auto;text-align:right">
        <input type="button" id="INP_AddResource"
             value="הזן תוכן" onclick="EditInsertModalResource(true, -1)" class="btnEditWidthKtuma" style="margin-bottom:10px;display:none;" />
            </div>
         <table style =" width:800px;text-align:center; margin:auto; display:none;" class="tblRes" id="TBL_resources">
             <tr>
                 <th style="width:10px;text-align:center; display:none;">
                     מיקום ברשימה
                 </th>
                 <th style="text-align:center;">
                     טקסט
                 </th>
                 <th style="width:250px;text-align:center;"></th>
             </tr>
             </table>
        </div>
        <div id="dialog-modalResource" style="display:none">
            <table class="innerTable" id="tblDataDialogResource">
            </table>
        </div>
     </fieldset>
<div style="margin:20px; font-size:18px; ">
        <span class="ui-icon  ui-icon-plusthick" id="SPN_ArchiveAdmin" style="cursor:pointer;"></span>ניהול ארכיון
     </div>
         <fieldset id="FLD_ArchiveAdmin" class="FLD_ArchiveAdmin">
           <legend>ניהול ארכיון</legend>
              <div runat="server" ID="MaintananceDiv" class="MaintananceDiv">
        <div class="MainDiv">
            <span style="width:15%;margin:3px;">מתאריך</span>&nbsp;
            <span style="width:15%;margin:3px;"><input type="text" ID="TXT_FromDate" style="width:30%"/></span>&nbsp;
            <span style="width:15%;margin:3px;">משעה</span>&nbsp;
            <span style="width:15%;margin:3px;"><select id="Select_FromDate"></select></span>
        </div>
        <div class="MainDiv">
            <span style="width:15%;margin:3px;">לתאריך</span>&nbsp;
            <span style="width:15%;margin:4px;"><input type="text" ID="TXT_ToDate" style="width:30%" /></span>&nbsp;
            <span style="width:15%;margin:3px;">לשעה</span>&nbsp;
            <span style="width:15%;margin:3px;"><select id="Select_ToDate"></select></span>
        </div>  
        <div  class="MainDivInline">
            <input type="button" style="width:220px; margin:10px;" onclick="archiveTables()" value="שמור נתונים בארכיון" />
        </div>
        <div class="MainDivInline" style="display:none;">
            <input type="button" style="width:220px; margin:10px;" onclick="resetTables()" value="אפס נתונים" />
       </div>
           <div class="MainDivInline">
            <input type="button" style="width:220px; margin:10px;" onclick="gotoArchivePage()" value="צפייה בארכיון" />
       </div>
    </div>
     </fieldset>
  
        <div style="margin:20px; font-size:18px; ">
        <span class="ui-icon  ui-icon-plusthick" id="SPN_SvivotAdmin" style="cursor:pointer;"></span>ניהול סביבות
     </div>
     <fieldset id="FLD_SvivotAdmin" class="FLD_ArchiveAdmin">
        <legend>ניהול סביבות</legend>
         <div style="text-align:center; margin-top:20px; direction:rtl; vertical-align:middle;" id="DIV_Svivot">         
   </div>
     </fieldset>
        </div>

     <script type="text/javascript">
         var userTypesDT;
         $(document).ready(function () {
             loadAdminSviva('<%=Session["SvivaId"]%>');
             loadUsers();
             loadUserTypes();
             loadNafotMehozot();
             var unit = '<%=((pikudEntities.KtumaUser)Session["User"]).UnitId %>';
             var group = '<%=((pikudEntities.KtumaUser)Session["User"]).GroupId %>';
             $("#SPN_UsersAdmin").click(function () {
                 togSearchSection('FLD_UsersAdmin', 'SPN_UsersAdmin');
             });
             $("#SPN_ResourcesAdmin").click(function () {
                 togSearchSection('FLD_ResourcesAdmin', 'SPN_ResourcesAdmin');
             });
             $("#SPN_ArchiveAdmin").click(function () {
                 togSearchSection('FLD_ArchiveAdmin', 'SPN_ArchiveAdmin');
             });
             $("#SPN_SvivotAdmin").click(function () {
                 togSearchSection('FLD_SvivotAdmin', 'SPN_SvivotAdmin');
             });
             $('#TXT_FromDate').datepicker(
               {
                   //appendText: " (dd/mm/yyyy)",
                   showOn: 'both',
                   buttonText: "בחר תאריך",
                   dateFormat: 'dd/mm/yy',
                   changeMonth: true,
                   changeYear: true,
                   onSelect: function () {
                       var fromDateObject = $(this).datepicker('getDate');
                   }
               });
             $('#TXT_ToDate').datepicker(
                 {
                     //appendText: " (dd/mm/yyyy)",
                     showOn: 'both',
                     buttonText: "בחר תאריך",
                     dateFormat: 'dd/mm/yy',
                     changeMonth: true,
                     changeYear: true,
                     onSelect: function () {
                         var toDateObject = $(this).datepicker('getDate');
                     }
                 });

             populate('Select_FromDate')
             populate('Select_ToDate')
             populateSvivot();
         });
        

         function gotoArchivePage() {
             window.location = 'viewLogs.aspx';
         }


         function populateSvivot()
         {
             var pageName = 'Settings';
             var jsonObj =
                     { page: pageName, op: 'loadSvivot' };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     var svivaId = '<%= Session["SvivaId"]%>';
                     console.log(svivaId)
                     fillRadioButtons(data, svivaId);
                 }
             });
         }

         function createRadioButtonsFromData(text, value, contaiingDivID) {
             var label = $('<label for="INP_Radio_' + value + '">' + text + '</label>');
             var input = $('<input type="radio" id="INP_Radio_' + value + '" class="radio" name="SvivaRadio" value="'+value+'" />');
             input.click(function () {
                 var cnfrm = confirm("האם אתה בטוח שברצונך לשנות סביבת עבודה?");
                 if (cnfrm == true) {
                     changeSviva(value);
                 }
                 else {
                     return false;
                 }
             });
             $('#' + contaiingDivID).append(input).append(label);
         }

         function changeSviva(val) {

             var radioValue = $("input[name='SvivaRadio']:checked").val();
             var pageName = 'Settings';
             var jsonObj =
                     { page: pageName, op: 'changeSviva', 'svivaId': radioValue };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     $('#h2Sviva').text('סביבה: ' + data.SvivaName)
                     alert('שינוי הסביבה הסתיים בהצלחה')
                 }
             });
         }
             
         

         function fillRadioButtons(data, selectedSvivaId)
         {
           
             $(data).each(function () {
                 createRadioButtonsFromData((this).SvivaName, (this).SvivaId, 'DIV_Svivot');
             });
             $("#INP_Radio_" + selectedSvivaId).prop("checked", true);


                          
         }

         function populate(selector) {
             var select = $('#' + selector);
             var hours, minutes, ampm;
             for (var i = 0; i <= 1430; i += 15) {
                 hours = Math.floor(i / 60);
                 minutes = i % 60;
                 if (minutes < 10) {
                     minutes = '0' + minutes; // adding leading zero
                 }
                 ampm = hours % 24 < 12 ? 'AM' : 'PM';
                 hours = hours % 12;
                 //if (hours === 0) {
                 //  hours = 12;
                 //}
                 select.append($('<option></option>')
                     .attr('value', i)
                     .text(hours + ':' + minutes + ' ' + ampm));
             }
         }
         
         function togSearchSection(fieldSetId, spanId) {

             $('fieldSet[id^="FLD_"]').each(function () {
                 if ($(this).css('display') == 'block' && $(this).attr('id') != fieldSetId) {
                     $(this).toggle("slow", function () {
                         var spnId = ($(this).attr('id')).replace('FLD','SPN')
                         $('#' + spnId).removeClass('ui-icon-minusthick').addClass(' ui-icon-plusthick');
                     })
                 }
             });
             $("#"+fieldSetId).toggle("slow", function () {
                 if ($('#'+spanId).attr('class') == 'ui-icon  ui-icon-plusthick' ||
                     $('#' + spanId).attr('class') == 'ui-icon ui-icon-plusthick') {
                     $('#' + spanId).removeClass('ui-icon-plusthick').addClass('ui-icon-minusthick')
                 }
                 else {
                     $('#' + spanId).removeClass('ui-icon-minusthick').addClass(' ui-icon-plusthick')
                 }

             });
         }
        
         function showSnackBar() {
             // Get the snackbar DIV
             var x = document.getElementById("snackbar")

             // Add the "show" class to DIV
             x.className = "show";

             // After 3 seconds, remove the show class from DIV
             setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
         }

         function loadUsers() {
             var pageName = 'Settings';
             var jsonObj =
                     { page: pageName, op: 'loadUsers', 'searchName': null };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     
                     fillTable(data);
                 }
             });
         }

         function fillTable(data) {
             $("#tblDataUsers").find("tr:gt(0)").remove();
             for (var i = 0; i < data.List.length; i++)//
             {
                 var tr = buildUserTR(data.List[i]);
                 $("#tblDataUsers").append(tr);
             }
         }



         function SearchUser() {
             var searchName = $('#INP_SearchUser').val();
             {
                 if (searchName == null || searchName.trim() == '') {
                     alert('הזן שם משתמש');
                 }
                 else {
                     var pageName = 'Settings';
                     var jsonObj =
                             { page: pageName, op: 'loadUsers', 'searchName': searchName };

                     $.ajax({
                         type: "POST",
                         url: "../Handler.ashx",
                         data: jsonObj,
                         dataType: "json",
                         failure: function (response) {
                             alert(response.d);
                         },
                         success: function (data) {
                             //console.log(data);
                             if (data.List == null) {
                                 alert('שם המשתמש לא מוכר במערכת')
                                 return false;
                             }
                             fillTable(data);
                         }
                     });
                 }
             }
             

         }

          function loadNafotMehozot() {
             var pageName = 'Settings'
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var jsonObj =
                     { page: pageName, op: 'loadNafotMehozot' };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     unitsDT = data;
                 }
             });
         }

         function loadUserTypes() {
             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
             var pageName = 'Settings';
             var jsonObj =
                     { page: pageName, op: 'loadUserTypes', 'userName': userName };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     userTypesDT = data;
                 }
             });
         }



         function EditInsertModalUser(show, obj) {
             var userIdToEditId;
             if (obj != -1) {
                 var objId = obj.attr('id')
                 userIdToEditId = objId.replace('INP_EditUser_', '');
             }
             else {
                 userIdToEditId = -1;
             }
             if (show) {
                 $("#dialog-modalUsers").dialog({
                     height: 250,
                     width: 350,
                     closeOnEscape: false,
                     modal: true,
                     title: userIdToEditId == -1 ? "הזנת משתמש חדש" : "עדכון משתמש מספר " + userIdToEditId,
                     open: function (event, ui) {
                         $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                     },
                     buttons: [
                                         {
                                             id: "addRowConfirm",
                                             text: "אשר",
                                             click: function () {
                                                 if (checkLoginInput('INP_UserName_Edit', 'INP_Password_Edit')) {
                                                     saveUser(userIdToEditId);

                                                     $(this).dialog("close");
                                                     resetButtons();
                                                 }

                                             }
                                         },
                                           {
                                               id: "addRowCancel",
                                               text: "בטל",
                                               click: function () {
                                                   $(this).dialog("close");
                                                   resetButtons();

                                               }
                                           }
                     ]
                 })
                 buildEditTableUser(userIdToEditId);


                 //disable
                 $(".btnEditKtumaSemi").attr('class', 'btnEditDisabledKtumaSemi').attr('disabled', 'disabled');
                 $(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');


             }
             else {

             }
         }

         function resetButtons() {
             //reset             
             $(".btnEditDisabledKtumaSemi").attr('class', 'btnEditKtumaSemi').removeAttr('disabled');
             $(".btnEditWidthKtumaDisabled").attr('class', 'btnEditWidthKtuma').removeAttr('disabled');
             $(".btnEditDisabledKtumaSmall").attr('class', 'btnEditKtumaSmall').removeAttr('disabled');
             
         }

         function buildEditTableUser(userId) {
             tblDataUsers = $('#tblDataDialogUsers');
             tblDataUsers.empty();

             var currentRow = $('#TR_User_' + userId);
             var userName = currentRow.find('td > span[id*=lblUserName]').text();
             var userPassword = currentRow.find('td > span[id*=lblPassword]').text();
             var userType = currentRow.find('td > span[id*=lblUserType]').text();
             var userUnit = currentRow.find('td > span[id*=lblUserUnit]').text();

  
             trFirstRow = $('<tr class="dialogTR">');

             var tdNameLabel = $('<td class="searchTdCenterBold">שם משתמש</td>');
             var inputName = $('<input type="textbox" id="INP_UserName_Edit" style=width:150px;">')
             inputName.val(userName);
             var tdNameInp = $('<td class="searchTdCenterBold">');
             tdNameInp.append(inputName);

             trSecondRow = $('<tr class="dialogTR">');
             var tdPasswordLabel = $('<td class="searchTdCenterBold">סיסמא</td>');
             var inputPassword = $('<input type="textbox" id="INP_Password_Edit" style=width:150px;">')
             inputPassword.val(userPassword);
             var tdPasswordInp = $('<td class="searchTdCenterBold">');
             tdPasswordInp.append(inputPassword);

             trThirdRow = $('<tr class="dialogTR">');
             var tdTypeDescriptionLabel = $('<td class="searchTdCenterBold">סוג משתמש</td>');
             var tdTypeDescriptionDDL = $('<td class="searchTdCenterBold">');
             cboUserTypes = createUserTypesDDL(userTypesDT, tdTypeDescriptionDDL, userType, userId);

             trFourthRow = $('<tr class="dialogTR">');
             var tdUnitLabel = $('<td class="searchTdCenterBold">גורם אחראי</td>');
             var tdUnitDDL = $('<td class="searchTdCenterBold">');
             cboUnit = createUnitsDDL(unitsDT, tdUnitDDL, userUnit, userId);
             trFirstRow.append(tdNameLabel).append(tdNameInp);
             trSecondRow.append(tdPasswordLabel).append(tdPasswordInp);
             trThirdRow.append(tdTypeDescriptionLabel).append(tdTypeDescriptionDDL);
             trFourthRow.append(tdUnitLabel).append(tdUnitDDL);
             tblDataUsers.append(trFirstRow).append(trSecondRow).append(trThirdRow).append(trFourthRow);
             setDialogStyle('addRowConfirm', 'addRowCancel');

         }


         function createUserTypesDDL(userTypesDT, td, selectedText, userId) {
             cbo = $("<select id='SELECT_UserTypeEdit_" + userId + "' style='max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;width:100%;'></select>");
             $(userTypesDT).each(function () {
                 if (selectedText == this.TypeDescription) {
                     cbo.append($("<option selected >").attr('value', this.UserTypeId).text(this.TypeDescription));
                 }
                 else {
                     cbo.append($("<option>").attr('value', this.UserTypeId).text(this.TypeDescription));
                 }
             });
             td.append(cbo);
         }



         function saveUser(userId) {

             var uName = $('#INP_UserName_Edit').val();
             var uPassword = $('#INP_Password_Edit').val();
             var uType = $('select[id*=SELECT_UserTypeEdit_]').val();
             if (uName == null || uName == '')
             {
                 alert('אנא הכנס שם משתמש');
                 return false;
             }
             if (uPassword == null || uPassword == '') {
                 alert('אנא הכנס סיסמא');
                 return false;
             }
             var pageName = 'Settings';
             var unit = $('select[id*=SELECT_UnitEdit_]').val();
             var jsonObj =
                     { page: pageName, op: 'saveUser', 'uName': uName, 'uPassword': uPassword, 'uType': uType, 'userId': userId, 'unit' : unit };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     var res = data.ActionResult;
                     if (res == 1) {
                         alert('המשתמש הוזן בהצלחה')
                         addUserToGrid(data);
                     }
                     else if (res == 2) {
                         alert('המשתמש עודכן בהצלחה')
                         updateUserInGrid(data)
                     }
                     else if (res == -1) {
                         alert('קיים משתמש עם שם שכזה במערכת')
                     }
                     else if (res == -2) {
                         window.location = "../default.aspx";
                     }
                     else {
                         alert('ארעה שגיאה, נא לפנות למנהל המערכת');
                     }



                 }
             });
         }

         function buildUserTR(row) {
             var LastUpdatedByUserName;
             var LastUpdated;
             var tdLastUpdatedUserName;
             var tdLastUpdatedDate;
             if (row.LastUpdatedByUser == null) {
                 LastUpdatedByUserName = '';


             }
             else {
                 LastUpdatedByUserName = row.LastUpdatedByUser.UserName;
             }
             tdLastUpdatedUserName = $('<td><span id="lblLastUpdatedUserName_' + row.UserId + '">' + LastUpdatedByUserName + '</span></td> ');
             if (row.LastUpdated == null) {
                 tdLastUpdatedDate = $('<td><span id="lblLastUpdatedDate_' + row.UserId + '"></span>' + '' + '</td> ');
             }
             else {
                 tdLastUpdatedDate = $('<td><span id="lblLastUpdatedDate_' + row.UserId + '">' + displayTime(row.LastUpdated) + '</span></td> ');
             }
             var tr = $('<tr id="TR_User_' + row.UserId + '"></tr>');
             var tdUserId = $('<td><span id="lblUserId_' + row.UserId + '">' + row.UserId + '</span></td> ');
             var tdUserName = $('<td><span id="lblUserName_' + row.UserId + '">' + row.UserName + '</span></td> ');
             var tdPassword = $('<td><span id="lblPassword_' + row.UserId + '">' + row.Password + '</span></td> ');
             var tdUnit = $('<td><span id="lblUserUnit_' + row.UserId + '">' + row.YehidaName + '</span></td> ');
             var tdGroup = $('<td><span id="lblGroup_' + row.UserId + '">' + row.UserGroup.GroupName + '</span></td> ');
             var tdUserTypeDescription = $('<td><span id="lblUserType_' + row.UserId + '">' + row.KtumaUserType.TypeDescription + '</span></td> ');

             tr.append(tdUserId).append(tdUserName).append(tdPassword).append(tdUnit).append(tdGroup).append(tdUserTypeDescription).append(tdLastUpdatedUserName).append(tdLastUpdatedDate);;
             var tdButtons = $('<td style="text-align:center;" id="TD_Buttons_' + row.UserId + '"></td> ');

             if (row.IsActive == true) {
                 var tdIsActive = $('<td style="text-align:center;"><span id="SPN_IsActive_' + row.UserId + '" class="ui-icon  ui-icon-check"></span></td> ');
                 var inpEdit = '<input type="button" id="INP_EditUser_' + row.UserId + '" value="ערוך" onclick="EditInsertModalUser(true, $(this))" class="btnEditKtumaSemi" />'
                 var inpDel = '<input type="button" id="INP_DeleteUser_' + row.UserId + '"  value="מחק" onclick="deleteUser($(this))"  class="btnEditKtumaSemi" />'
             }
             else {
                 var tdIsActive = $('<td style="text-align:center;"></td> ');
                 var inpEdit = '<input style="display:none;" type="button" id="INP_EditUser_' + row.UserId + '" value="ערוך" onclick="EditInsertModalUser(true, $(this))" class="btnEditKtumaSemi" />'
                 var inpDel = '<input style="display:none;" type="button" id="INP_DeleteUser_' + row.UserId + '"  value="מחק" onclick="deleteUser($(this))"  class="btnEditKtumaSemi" />'
             }
             tdButtons.append(inpEdit).append(inpDel);
             tr.append(tdIsActive);

             tr.append(tdButtons);
             return tr;
         }

         function addUserToGrid(data) {
             tr = buildUserTR(data.List[0]);
             $('#tblDataUsers').append(tr);
         }

         function updateUserInGrid(row) {
             var currentRow = $('#TR_User_' + row.List[0].UserId);
             currentRow.find('td > span[id*=lblUserName]').text(row.List[0].UserName);
             currentRow.find('td > span[id*=lblPassword]').text(row.List[0].Password);
             currentRow.find('td > span[id*=lblUserType]').text(row.List[0].KtumaUserType.TypeDescription);
             currentRow.find('td > span[id*=lblUserUnit]').text(row.List[0].YehidaName);
             currentRow.find('td > span[id*=lblGroup]').text(row.List[0].UserGroup.GroupName);
             currentRow.find('span[id*=lblLastUpdatedUserName]').text(row.List[0].LastUpdatedByUser.UserName);
             currentRow.find('td > span[id*=lblLastUpdatedDate]').text(displayTime(row.List[0].LastUpdated));             
             if (row.List[0].IsActive == false) {
                 currentRow.find('td > span[id*=SPN_IsActive_]').attr('class', '');
                 currentRow.find('td[id*=TD_Buttons_]').html('');
             }
         }

         function deleteUser(obj) {
             var objId = obj.attr('id')

             var userIdToDeleteId = objId.replace('INP_DeleteUser_', '');
             if (confirm("למחוק את המשתמש מהמערכת?")) {
                 var pageName = 'Settings';
                 var jsonObj =
                  { page: pageName, op: 'deleteUser', 'userIdToDeleteId': userIdToDeleteId };

                 $.ajax({
                     type: "POST",
                     url: "../Handler.ashx",
                     data: jsonObj,
                     dataType: "json",
                     failure: function (response) {
                         alert(response.d);
                     },
                     success: function (data) {
                         //console.log(data);
                         var res = data.ActionResult;                        
                        if (res == 2) {
                             alert('המשתמש נמחק בהצלחה')
                             updateUserInGrid(data)
                         }
                         else if (res == -1) {
                             alert('המשתמש לא מוכר במערכת')
                         }
                         else if (res == -2) {
                             window.location = "../default.aspx";
                         }
                         else {
                             alert('ארעה שגיאה, נא לפנות למהל המערכת');
                         }
                     }
                 });
             }
             else {

             }
         }
    
         var curScreens;
         var curCombos;
         var curResources;
         var unitsDT;
         const IMG_Down = 'IMG_Down_';
         const IMG_Up = 'IMG_Up_';
         var selectedComboId;
         $(document).ready(function () {
             loadPageData();
             $('body').on('click', '.down_button', function () {
                 updateComboInternalId($(this), false);

             });
             $('body').on('click', '.up_button', function () {
                 updateComboInternalId($(this), true);
             });


         })

         function updateComboInternalId(obj, Up) {
             var ResourceId;
             var rowToMove;
             var next = {};
             var prev = {};
             rowToMove = obj.parents('tr.MoveableRow:first');
             if (Up == true) {
                 ResourceId = $(obj).attr('id').replace(IMG_Up, '')
                 prev = rowToMove.prev('tr.MoveableRow')
             }
             else {
                 ResourceId = $(obj).attr('id').replace(IMG_Down, '')
                 next = rowToMove.next('tr.MoveableRow')
             }

             if ((next.length == 1) || (prev.length == 1)) {
                 var pageName = 'Resources';
                 var jsonObj =
                         { page: pageName, op: 'UpdateComboIndex', 'ResourceId': ResourceId, 'Up': Up };

                 $.ajax({
                     type: "POST",
                     url: "../Handler.ashx",
                     data: jsonObj,
                     dataType: "json",
                     failure: function (response) {
                         alert(response.d);
                     },
                     success: function (data) {
                         if (data != null && data.length>0) {
                             if (Up == false) {
                                 if (next.length == 1) { next.after(rowToMove.fadeIn()); }
                             }
                             else {
                                 if (prev.length == 1) { prev.before(rowToMove.fadeIn()); }
                             }

                         }
                         else
                         {
                             alert('ארעה שגיאה, לא ניתן לעדכן את מיקוםהתוכן ברשימת הגלילה')
                             window.location.href = "../default.aspx";
                         }
                     }
                 });
             }
             else {
             }
         }

         function buildResourceTR(row) {
             text = row.ResourceText;
             comboIndex = row.ComboInternalId;
             var tr = $('<tr id="TR_Resource_' + row.ResourceId + '" class="MoveableRow" style="vertical-align:middle;">');
             var tdText = $('<td id="TD_ResourceText_' + row.ResourceId + '">' + text + '</td>');
             var tdComboId = $('<td style="text-align:center;display:none;" id="TD_comboIndex_' + row.ResourceId + '">' + comboIndex + '</td>');
             var imgDown = $('<img src="../Images/icons8-down-arrow-filled-50.png" class="down_button" style="vertical-align:middle;" />');
             imgDown.attr('id', IMG_Down + row.ResourceId);
             var imgUp = $('<img src="../Images/icons8-up-filled-50.png" class="up_button"  style="vertical-align:middle;"/>');
             imgUp.attr('id', IMG_Up + row.ResourceId);
             var tdArrows = $('<td style="text-align:center;vertical-align:middle;"> </td>')

             var inpEdit = $('<input  style="vertical-align:middle;"  type="button" id="INP_EditResource_' + row.ResourceId + '" value="ערוך" onclick="EditInsertModalResource(true, $(this))" class="btnEditKtumaSmall" />');
             var inpDel = $('<input  style="vertical-align:middle;"  type="button" id="INP_DeleteResource_' + row.ResourceId + '"  value="מחק" onclick="deleteResource($(this))"  class="btnEditKtumaSmall" />');
             tdArrows.append(imgDown).append(imgUp).append(inpEdit).append(inpDel);
             //tdArrows.append(inpEdit).append(inpDel);
             tr.append(tdComboId).append(tdText).append(tdArrows);
             return tr;
         }

         function deleteResource(obj) {
             var objId = obj.attr('id')

             var resourceIdToDeleteId = objId.replace('INP_DeleteResource_', '');
             if (confirm("למחוק את התוכן מהמערכת?")) {
                 var pageName = 'Resources';
                 var jsonObj =
                  { page: pageName, op: 'deleteResource', 'resourceIdToDeleteId': resourceIdToDeleteId };

                 $.ajax({
                     type: "POST",
                     url: "../Handler.ashx",
                     data: jsonObj,
                     dataType: "json",
                     failure: function (response) {
                         alert(response.d);
                     },
                     success: function (data) {
                         //console.log(data);
                         if (data == true) {
                             alert('התוכן נמחק בהצלחה')
                             var currentRow = $('#TR_Resource_' + resourceIdToDeleteId);
                             curResources = $.grep(curResources, function (e) {
                                 return e.ResourceId != resourceIdToDeleteId;
                             });
                             currentRow.remove();
                         }
                         else {
                             alert('ארעה שגיאה, לא ניתן למחוק את התוכן')
                             window.location.href = "../default.aspx";
                         }

                     }
                 });
             }
             else {

             }
         }

         function loadScreens() {
             var pageName = 'Resources';
             var jsonObj =
                     { page: pageName, op: 'loadScreens' };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     curScreens = data[0];
                     fillScreens(curScreens);
                 }
             });
         }

         function loadResources() {
             var pageName = 'Resources';
             var jsonObj =
                     { page: pageName, op: 'loadResources' };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     curResources = data[0];
                 }
             });
         }


         function loadPageData() {
             var pageName = 'Resources';
             var jsonObj =
                     { page: pageName, op: 'loadPageData' };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);
                     curScreens = data[0];
                     curCombos = data[1];
                     curResources = data[2];
                     fillScreens(curScreens);
                 }
             });
         }

         function fillScreens(data) {
             var DDL_Screen = $('#DDL_Screen');
             DDL_Screen.append('<option value="' + '0' + '">' + 'בחר עמוד' + '</option>');
             $(curScreens).each(function () {
                 DDL_Screen.append('<option value="' + (this).ScreenName + '">' + (this).ScreenNameHeb + '</option>');
             });
         }

         function findScreenCombo() {

             var cbo = $('#DDL_PageCombo');
             cbo.empty();
             cbo.append('<option value="' + '0' + '">' + 'בחר רשימת גלילה' + '</option>');
             filterField = $('#DDL_Screen option:selected').val();
             filteredDataTable = jQuery.grep(curCombos, function (n, i) {
                 return n['ComboScreen'] == filterField;
             });
             if (filterField != null && filterField != 0) {
                 $(filteredDataTable).each(function () {
                     text = this.ComboDescription;
                     val = this.ComboId;
                     cbo.append($("<option>").attr('value', val).text(text));
                 });
             }

             if ($('#INP_AddResource').css('display') == 'inline-block') {
                 $('#INP_AddResource').fadeOut()
                 $('#TBL_resources').fadeOut();
                 selectedComboId = 0;
                 $('#DDL_PageCombo').val(selectedComboId);
             }

         }

         function loadComboResources() {
             $("#TBL_resources").find("tr:gt(0)").remove();
             filterField = $('#DDL_PageCombo option:selected').val();
             selectedComboId = filterField;
             if (selectedComboId != null && selectedComboId != 0) {
                 $('#INP_AddResource').fadeIn()
                 $('#TBL_resources').fadeIn();
             }
             else {
                 $('#INP_AddResource').fadeOut()
                 $('#TBL_resources').fadeOut();
             }
             filteredDataTable = jQuery.grep(curResources, function (n, i) {
                 return n['ComboId'] == filterField;
             });

             $(filteredDataTable).each(function () {
                 var tr = buildResourceTR(this);
                 $('#TBL_resources').append(tr);
             });


         }

         function EditInsertModalResource(show, obj) {
             var resourceIdToEditId;
             if (obj != -1) {
                 var objId = obj.attr('id')
                 resourceIdToEditId = objId.replace('INP_EditResource_', '');
             }
             else {
                 resourceIdToEditId = -1;
             }
             if (show) {
                 $("#dialog-modalResource").dialog({
                     height: 250,
                     width: 350,
                     closeOnEscape: false,
                     modal: true,
                     title: resourceIdToEditId == -1 ? "הזנת תוכן חדש" : "עדכון תוכן מספר " + resourceIdToEditId,
                     open: function (event, ui) {
                         $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                     },
                     buttons: [
                                         {
                                             id: "addRowConfirm",
                                             text: "אשר",
                                             click: function () {
                                                 saveResource(resourceIdToEditId);

                                                 $(this).dialog("close");
                                                 resetButtons();

                                             }
                                         },
                                           {
                                               id: "addRowCancel",
                                               text: "בטל",
                                               click: function () {
                                                   $(this).dialog("close");
                                                   resetButtons();

                                               }
                                           }
                     ]
                 })
                 buildEditTableResource(resourceIdToEditId);


                 //disable
                 $(".btnEditKtumaSmall").attr('class', 'btnEditDisabledKtumaSmall').attr('disabled', 'disabled');
                 //$(".btnEditWidthKtuma").attr('class', 'btnEditWidthKtumaDisabled').attr('disabled', 'disabled');


             }
             else {

             }
         }

         function saveResource(resourceId) {
             var resourceText = $('#INP_Resource_Edit').val();
             var comboId = selectedComboId;
             var pageName = 'Resources';
             var jsonObj =
                     { page: pageName, op: 'saveResource', 'resourceText': resourceText, 'resourceId': resourceId, 'comboId': comboId };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                    
                     if (data.length == 0)
                     {
                         alert('ארעה שגיאה, נא לפנות למנהל המערכת');
                         window.location = "../default.aspx";
                     }
                     var res = data[0].actionResult;
                     if (res == 1) {
                         alert('התוכן הוזן בהצלחה')
                         addResourceToGrid(data[0]);
                     }
                     else if (res == 2) {
                         alert('התוכן עודכן בהצלחה')
                         updateResourceInGrid(data[0])
                     }
                     else if (res == -1) {
                         alert('התוכן לא מוכר במערכת')
                     }
                     else {
                         alert('ארעה שגיאה, נא לפנות למהל המערכת');
                     }



                 }
             });
         }

         function addResourceToGrid(data) {
             tr = buildResourceTR(data);
             $('#TBL_resources').append(tr);
             //add to curResources            
             curResources.push(data);
         }

         function updateResourceInGrid(row) {
             $('#TD_ResourceText_' + row.ResourceId).text(row.ResourceText);
             //update resource in grid
             loadResources();
         }

         function buildEditTableResource(resourceId) {
             tblData = $('#tblDataDialogResource');
             tblData.empty();

             var resourceText = $('#TD_ResourceText_' + resourceId).text();
             var inputResource = $('<textarea rows="5"   id="INP_Resource_Edit"' + '" maxlength="250" style="width:200px;"   ></textarea>');
             inputResource.val(resourceText);
             var tdLabel = $('<td class="searchTdCenterBold">טקסט</td>');
             var tdText = $('<td class="searchTdCenterBold"></td>');
             tdText.append(inputResource);
             trFirstRow = $('<tr class="dialogTR">');
             trFirstRow.append(tdLabel).append(tdText);
             tblData.append(trFirstRow);
             setDialogStyle('addRowConfirm', 'addRowCancel');

         }

         function resetTables() {
             var jsonObj =
                       {
                           "op": "resetTables"
                       };
             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //alert("koki" + dataArray);
                     if (data == "false") //
                     {
                         alert("אתחול המערכת נכשל");
                     }
                     else//
                     {
                         alert("הנתונים נשמרו בארכיון בהצלחה");
                     }
                 }
             });
         }

         function archiveTables() {
             var fromDate = $(TXT_FromDate).datepicker("getDate");
             var toDate = $(TXT_ToDate).datepicker("getDate");
             var fromHour = $('#Select_FromDate option:selected').text()
             var arrFrom = fromHour.split(/[ ,:]+/);
             var toHour = $('#Select_ToDate option:selected').text()
             var toHourInt;
             var fromHourInt;
             var totalFromDate;
             var totalToDate;
             var arrTo = toHour.split(/[ ,:]+/);
             if (fromDate == null) {
                 alert("הכנס תאריך התחלה")
                 return false;
             }
             if (toDate == null) {
                 alert("הכנס תאריך סיום")
                 return false;
             }
             if (arrFrom[2] == 'AM') {
                 fromDate.setHours(arrFrom[0]);
             }
             else {
                 fromHourInt = parseInt(arrFrom[0]);
                 fromHourInt = fromHourInt + 12; 
                 fromDate.setHours(fromHourInt);
             }
             fromDate.setMinutes(arrFrom[1]);
             if (arrTo[2] == 'AM') {
                 toDate.setHours(arrTo[0]);
             }
             else {
                 toHourInt = parseInt(arrTo[0]);
                 toHourInt = toHourInt + 12; 
                 toDate.setHours(toHourInt);
             }
             toDate.setMinutes(arrTo[1]);

             if (fromDate >= toDate) {
                 alert("זמן התחלה גדול מזמן סיום");
                 return false;
             }
             var jsonObj =
                        {
                            "op": "moveToArchive", "fromDate": JSDateToFullFormat(fromDate),
                            "toDate": JSDateToFullFormat(toDate)
                        };
             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //alert("koki" + dataArray);
                     if (data != null) {
                         if (data == 0) //
                         {
                             alert("השמירה בארכיון נכשלה, כבר קיים מידע עבור טווח תאריכים זה");
                         }
                         else if (data == 1) //
                         {
                             alert("השמירה בארכיון נכשלה, חסרים נתונים עבור אזורי התגוננות בטבלת final_editing");
                         }
                         else if (data == 2) //
                         {
                             alert("השמירה בארכיון נכשלה, חסרים נתונים עבור אזורי התגוננות בטבלת final_grade");
                         }
                         else if (data == 3) {
                             alert("הנתונים נשמרו בארכיון בהצלחה");
                         }
                         else {
                             alert("השמירה בארכיון נכשלה");
                         }
                     }
                     else {
                         alert("השמירה בארכיון נכשלה");
                     }
                 }
             });
         }

          function createUnitsDDL(unitsDT, td, selectedText, userId) {
             var cbo = $("<select id='SELECT_UnitEdit_" + userId + "' style='max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;width:100%;'></select>");
             $(unitsDT).each(function () {
                 if (selectedText == this.YehidaName) {
                     cbo.append($("<option selected >").attr('value', this.UnitId +'_'+ this.groupid).text(this.YehidaName));
                 }
                 else {
                     cbo.append($("<option>").attr('value', this.UnitId +'_'+ this.groupid).text(this.YehidaName));
                 }
             });
             td.append(cbo);
         }


          </script>
    <div id="snackbar">הנתונים נשמרו בהצלחה.</div>
</asp:Content>

