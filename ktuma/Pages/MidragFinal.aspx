﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Heading.Master"
     AutoEventWireup="true" CodeFile="MidragFinal.aspx.cs" Inherits="Pages_MidragFinal" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <script type="text/javascript">
         $(document).ready(function ()//
         {
             var uid = '<%=Session["userID"]%>';
             checkSession(uid);
             loadParams('<%=Session["userID"]%>',true, false);  
             loadAdminSviva('<%=Session["SvivaId"]%>');
         });

         function refresh(isFullRefresh) //
         {

             var userName = '<%=Session["UserName"]%>';
             checkSession(userName);
              var jsonObj =
                      { op: 'loadMidrag', 'userName': userName };

              $.ajax({
                  type: "POST",
                  url: "../Handler.ashx",
                  data: jsonObj,
                  dataType: "json",
                  failure: function (response) {
                      alert(response.d);
                  },
                  success: function (data) {
                      //console.log(data);

                      fiilMidragTable(data, isFullRefresh);

                  }
              });
         }



         function fiilMidragTable(data, isFullRefresh)
         {
             $('#MainDiv').empty();
             for(var i=0;i<data.length;i++)
             {
                 createRegionsDiv(data[i].ItgonenutRegionsList);
                 createHanchaiutTable(data[i].Hanchaiut, data[i].MidragFinal);
                 
             }
             
         }

         function createRegionsDiv(itgoenenutList)
         {
             var res = '';
             for(var i=0; i<itgoenenutList.length-1;i++)
             {
                 res += itgoenenutList[i] + ", ";
             }
             res += itgoenenutList[i];
             var div = $('<div style="padding-right:10px;">' + res + '</div>');
             $('#MainDiv').append(div);
         }

         function createHanchaiutTable(hanchaiut, midragFinal)
         {
             //console.log(hanchaiut)
             var table = $('<table cellspacing="0" cellpadding="0"></table>');
             table.addClass('TblNoBorders');
             var trHead = $('<tr></tr>');
             trHead.addClass('TblNoBordersTH');
             var tdMidrag = $('<td style="width:10%">' + 'מדרג' + '</td>');
             var tdItcansoot = $('<td style="width:20%">' + 'התכנסויות מופעים וארועים' + '</td>');
             var tdMosach = $('<td style="width:20%">' + 'פעילות חינוכית' + '</td>');
             var tdJob = $('<td style="width:20%;display:none;">' + 'מקומות עבודה ומתחמי קניות שאינם חיוניים' + '</td>');
             var tdSherootim = $('<td  style="width:20%">' + 'שירותים לקהל הרחב' + '</td>');
             var tdSector = $('<td style="width:10%">' + 'סקטור חיוני' + '</td>');

             table.append(trHead);
             trHead.append(tdMidrag).append(tdItcansoot).append(tdMosach).append(tdJob).append(tdSherootim).append(tdSector)

             var trBody = $('<tr></tr>');
             changeColoredDataValueInLineByValPop(trBody, midragFinal);
             var tdMidragData = $('<td>' + midragFinal + '</td>');
             var tdSectorData = $('<td>' + hanchaiut.Sector + '</td>');
             var tdItcansootData = $('<td>' + hanchaiut.Itcansoot + '</td>');
             var tdMosachData = $('<td>' + hanchaiut.Mosach + '</td>');
             var tdJobData = $('<td style="display:none;">' + hanchaiut.Job + '</td>');
             var tdSherootimData = $('<td>' + hanchaiut.Sherootim + '</td>');
             table.append(trBody);
             trBody.append(tdMidragData).append(tdItcansootData).append(tdMosachData).append(tdJobData).append(tdSherootimData).append(tdSectorData)
             $('#MainDiv').append(table);
         }
      </script>
     <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/מדיניות התגוננות מומלצת-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; מדיניות התגוננות מומלצת</span>
       </div>
    <div id="MainDiv" class="MainDiv" style="font-weight:700;" ></div>
</asp:Content>
