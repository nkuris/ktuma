﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Shigra.aspx.cs" Inherits="Pages_Shigra" 
    MasterPageFile="~/MasterPages/Heading.Master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;

        var glbEditedLine_Comander_Affect_BeforeRefresh = {};
        var glbEditedLine_Explantion_BeforeRefresh = '';
        var glbEditedLine_Kamut_BeforeRefresh = '';
        var glbEditedLine_Shigra_BeforeRefresh = {};
        var glbEditedLine_FinalGrade_BeforeRefresh = {};

        $(document).ready(function ()//
        {
            loadCombosShigra();
            var uid = '<%=Session["userID"]%>'
            checkSession(uid);
            loadParams('<%=Session["userID"]%>',true, false);   
            loadAdminSviva('<%=Session["SvivaId"]%>');
        });


        function loadCombosShigra() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName = 'Shigra';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;
                }
            });


        }

        function refresh(isFullRefresh) //
        {

            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
             var jsonObj =
                     { op: 'loadShigra', 'userName': userName };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data, isFullRefresh, userName);

                 }
             });
         }

        function fillTable(data, isFullRefresh, userName) //
        {


            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i)
                }
                setRangesCol('tblData');


            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {
                            //  $(this).find('input#data_24_' + data[i].Itgonenut_Id).val(glbEditedLine_Val24_BeforeRefresh);
                            //  $(this).find('input#data_48_' + data[i].Itgonenut_Id).val(glbEditedLine_Val48_BeforeRefresh);
                            //  $(this).find('input#data_72_' + data[i].Itgonenut_Id).val(glbEditedLine_Val72_BeforeRefresh);
                        }
                        else {
                            $('#TD_Kamut_' + data[i].Itgonenut_Id).html(data[i].Kamut);
                            setTDValTextAndColor($('#TD_Amida_' + data[i].Itgonenut_Id), data[i].AmidaValue, data[i].AmidaText);
                            setTDValTextAndColor($('#TD_Gilui_' + data[i].Itgonenut_Id), data[i].GiluiValue, data[i].GiluiText);
                            setTDValTextAndColor($('#TD_Hatraa_' + data[i].Itgonenut_Id), data[i].HatraaValue, data[i].HatraaText);
                            setTDValTextAndColor($('#TD_Comander_Affect_' + data[i].Itgonenut_Id), data[i].ComanderAffectValue, data[i].ComanderAffectText);
                            $('#TD_Explanation_' + data[i].Itgonenut_Id).html(data[i].Explantion);
                            setTDValTextAndColor($('#TD_Final_Grade_' + data[i].Itgonenut_Id), data[i].FinalGradeValue, data[i].FinalGradeText);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i) {
            var row = '';
            row = '';
            row += '<td>' + data[i].Name + '</td>';
            if (data[i].Kamut == null || data[i].Kamut == undefined) {
                data[i].Kamut = '';
            }
            row += '<td id="TD_Kamut_' + data[i].Itgonenut_Id + '" >' + data[i].Kamut + '</td>';
            bgClass = getClassByExactValue(data[i].ShigraValue);
            row += '<td id="TD_Shigra_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].ShigraValue + ' - ' + data[i].ShigraText + '</td>';
            if (data[i].ComanderAffectText == null || data[i].ComanderAffectText == undefined) {
                data[i].ComanderAffectText = '';
            }
            if (data[i].ComanderAffectValue == null || data[i].ComanderAffectValue == undefined) {
                data[i].ComanderAffectValue = '';
            }

            if (data[i].Explanation == null || data[i].Explanation == undefined) {
                data[i].Explanation = '';
            }
            if (data[i].ComanderAffectValue == '') {
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" ></td>';
            }
            else {
                bgClass = getClassByExactValue(data[i].ComanderAffectValue);
                row += '<td id="TD_Comander_Affect_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].ComanderAffectValue + ' - ' + data[i].ComanderAffectText + '</td>';
            }
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explanation', data[i].Explanation);            
            bgClass = getClassByExactValue(data[i].FinalGradeValue);
            row += '<td id="TD_Final_Grade_' + data[i].Itgonenut_Id + '" class="' + bgClass + '">' + data[i].FinalGradeValue + ' - ' + data[i].FinalGradeText + '</td>';


            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);

            if (data[i].user_name == null) // no user took this button yet, for reguler users
            {
                row += strButton;
            }
            else if (data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
            {
                strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                row += strButton;
                //first time
                if (timer_gear == -1)//
                {
                    glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                    startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                }
            }
            else //
            {
                row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
            }


            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }


        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Shigra'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Kamut_BeforeRefresh, glbEditedLine_Comander_Affect_BeforeRefresh,
                glbEditedLine_Explantion_BeforeRefresh, glbEditedLine_FinalGrade_BeforeRefresh,
                glbEditedLine_Shigra_BeforeRefresh, Itgonenut_Id);
        }
        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }

        function checkOutOrSave(ItgonenutId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
             var pageName = "Shigra";
             if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
             {
                 var jsonObj =
                         { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                 $.ajax({
                     type: "POST",
                     url: "../Handler.ashx",
                     data: jsonObj,
                     dataType: "json",
                     failure: function (response) {
                         alert(response.d);
                     },
                     success: function (data) {
                         //console.log(data);
                         if (data[0].result == "false") //
                         {
                             alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                         }
                         else//
                         {
                             ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                             createEditDDL(ItgonenutId);
                             glbEditedLine_ItgonenutId = ItgonenutId;
                             startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)

                         }
                     }
                 });
             }
             else//
             {
                 var comander_Affect = {};
                 var explantion = $("#INP_Explanation_" + ItgonenutId).val();
                 var kamut = $("#INP_Kamut_" + ItgonenutId).val();
                 setValuesForUndoCheckoutFromTextOnly(comander_Affect, "Comander_Affect", ItgonenutId);
                 if (!($.isNumeric(kamut))) {
                     alert('אנא הכנס לשדה כמות רק ערכים מספריים גדולים או שווים ל-0 ')
                     return false;
                 }
                 if (parseInt(kamut) < 0)
                 {
                     alert('אנא הכנס לשדה כמות רק ערכים מספריים גדולים או שווים ל-0 ')
                     return false;
                 }

                 if ((comander_Affect.val != '0' && comander_Affect.val != '') && explantion == '') {
                     alert('הכנס ערך בשדה "הסבר"');
                     return false;
                 }
                 var comander_AffectText = $("#SELECT_Comander_Affect_" + ItgonenutId + ' option:selected').text();

                 var jsonObj =
                         {
                             page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>', "kamut": kamut,
                            "explantion": explantion, "comander_Affect": comander_Affect.val
                        };


                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //alert("koki" + dataArray);
                        if (data[0].result == "false") //
                        {
                            alert("השמירה נכשלה");
                        }
                        else//
                        {
                            var finalGrade = { val: data[0].FinalGrade, text: data[0].FinalGradeText };
                            var shigra = { val: data[0].Shigra, text: data[0].ShigraText };
                            resetEdit(kamut, comander_Affect, explantion, finalGrade, shigra, ItgonenutId);


                        }
                    }
                });
            }
        }

        function createEditDDL(ItgonenutId) {
            var tdComanderAffect = $("#TD_Comander_Affect_" + ItgonenutId)
            setValuesForUndoCheckout(glbEditedLine_Comander_Affect_BeforeRefresh, tdComanderAffect.html())
            glbEditedLine_Kamut_BeforeRefresh = $("#TD_Kamut_" + ItgonenutId).html();
            glbEditedLine_Explantion_BeforeRefresh = $("#TD_Explanation_" + ItgonenutId).html();
            var tdShigra = $("#TD_Shigra_" + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Shigra_BeforeRefresh, tdShigra.html());
            var tdExplanation = $("#TD_Explanation_" + ItgonenutId);
            var tdFinalGrade = $('#TD_Final_Grade_' + ItgonenutId);
            var tdKamut = $("#TD_Kamut_" + ItgonenutId);            
            setValuesForUndoCheckout(glbEditedLine_FinalGrade_BeforeRefresh, tdFinalGrade.html())
            tdComanderAffect.empty();
            tdComanderAffect.attr('class', 'tdEditClass');
            tdKamut.empty();
            tdExplanation.empty();
            cboComander_Affect = createCombo(curCombosDT, 'Comander_Affect', tdComanderAffect, ItgonenutId, true, glbEditedLine_Comander_Affect_BeforeRefresh.val, true);
            createTextArea(tdExplanation, 'Explanation', ItgonenutId, glbEditedLine_Explantion_BeforeRefresh, 270);
            createInput(tdKamut, 'Kamut', ItgonenutId, glbEditedLine_Kamut_BeforeRefresh);
        }




        function resetEdit(kamut, comander_Affect, explantion, finalGrade, shigra, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
            setTDValTextAndColorObj($('#TD_Comander_Affect_' + glbEditedLine_ItgonenutId), comander_Affect, false);
            $('#TD_Kamut_' + glbEditedLine_ItgonenutId).html(kamut);
            $('#TD_Explanation_' + glbEditedLine_ItgonenutId).html(explantion);
            setTDValTextAndColorObj($('#TD_Shigra_' + glbEditedLine_ItgonenutId),shigra,false);
            setTDValTextAndColorObj($('#TD_Final_Grade_' + glbEditedLine_ItgonenutId), finalGrade);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>
    <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/התרעה-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; התרעה</span>
          </div>
     <div style="width: 99%; margin: 0 auto;">
           
        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 6%;" />
                    <col style="width: 6%;" />
                    <col style="width: 12%;" /><!-- 24%-->
                    <col style="width: 15%;" />
                    <col style="width: 15%;" /><!-- 54%-->
                    <col style="width: 22%;" />
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />
                   <col style="width: 8%;" />
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">כמות
                    </th>                  
                    <th class="TBLHeader">הפרעה לשגרת החיים
                    </th>
                     <th class="TBLHeader">הערכת מפקד
                    </th>
                    <th class="TBLHeader">
                        הסבר (מלל חופשי)
                    </th>
                    <th class="TBLHeader">
                        ציון סופי
                    </th>
                    <th class="TBLHeader"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
