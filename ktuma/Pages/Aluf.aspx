﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Heading.Master" CodeFile="Aluf.aspx.cs" Inherits="Pages_Aluf" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var curCombosDT;
        var glbEditedLine_ItgonenutId = -1;
        var glbEditedLine_Champ_Affect_BeforeRefresh = {};
        var glbEditedLine_Explantion_Champ_Affect_BeforeRefresh = '';

    $(document).ready(function ()//
    {
        loadCombosAluf();
        var uId = '<%=Session["userID"]%>';
        checkSession(uId)
        loadParams(uId, true, false);
        loadAdminSviva('<%=Session["SvivaId"]%>');
        
    });



        function loadCombosAluf() {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName)
            var pageName = 'Aluf';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


        }

    
  
     function refresh(isFullRefresh) //
     {
         var Itgonenut_Id = -1;
         if ('<%=urlItgonenutId%>' != '')
         {
             Itgonenut_Id = '<%=urlItgonenutId%>';
         }
         var userName = '<%=Session["UserName"]%>';
         checkSession(userName);
             var jsonObj =
                     { op: 'loadAluf', 'userName': userName, 'Itgonenut_Id': Itgonenut_Id};

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json", 
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data, isFullRefresh);
                    

                 }
             });
         }

        function fillTable(data, isFullRefresh) //
        {
            var userTypeId = $('#HID_UserTypeId').val();
            var userGroupId = '<%=Session["UserGroupId"]%>';

            if (userGroupId != 1) {
                userTypeId = "-1";
            }
            if (userTypeId == "-1") {
                $('#TH_Actions').css('display', 'none');
            }
           
            if (isFullRefresh) //********************* full redraw table
            {
                //remove all rows except first one
                $("#tblData").find("tr:gt(0)").remove();
                for (var i = 0; i < data.length; i++)//
                {
                    buildTableRow(data, i, userTypeId)
                }
                setRangesCol('tblData');
            }
            else //only refresh values !!!
            {
                //only refresh don't redraw table
                var i = 0;
                $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
                (//
                    function ()//
                    {
                        if (glbEditedLine_ItgonenutId != -1 && glbEditedLine_ItgonenutId == data[i].Itgonenut_Id)//we have edited line so skip it
                        {

                        }
                        else {                            
                            setTDValTextAndColor($('#TD_Champ_Affect_' + data[i].Itgonenut_Id), data[i].ChampAffectValue, data[i].ChampAffectText, true);
                            setTDValTextAndColor($('#TD_Midrag_' + data[i].Itgonenut_Id), data[i].MidragValue, data[i].MidragText, true);
                            $('#TD_Explantion_Champ_Affect_' + data[i].Itgonenut_Id).html(data[i].Explantion_Champ_Affect);
                            var disableAllEditButtons = '';
                            if (data[0].isUserEdited == 1)//
                            {
                                disableAllEditButtons = 'disabled="disabled"';
                            }
                            if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() == '') {//add edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                var strClick = createSTRClick(data[i].Itgonenut_Id);
                                var strButton = createEditButtonNoExternal(data[i].Itgonenut_Id, disableAllEditButtons, strClick);
                                //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].Itgonenut_Id + strClick; //edit button (checkOut)
                                edit.append(strButton);
                            }
                            else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].Itgonenut_Id).html() != '') {//remove edit button
                                var edit = $('#TD_Edit_' + data[i].Itgonenut_Id);
                                edit.html('');
                            }
                        }
                        //update user checkout on row
                        $(this).find('.data_user').html(data[i].user_name);
                        i++;
                    }//
                ); //END .each
            }
        }

        function buildTableRow(data, i, userTypeId) {
            var row = '';
            row = '';
            if (data[i].Champ_Affect == null || data[i].Champ_Affect == undefined) {
                data[i].Champ_Affect = '';
            }
            if (data[i].Explantion_Champ_Affect == null || data[i].Explantion_Champ_Affect == undefined) {
                data[i].Explantion_Champ_Affect = '';
            }

            if (data[i].Midrag == null || data[i].Midrag == undefined) {
                data[i].Midrag = '';
            }


            row += '<td>' + data[i].Name + '</td>';
            bgClass = getClassByExactValuePop(data[i].MidragValue);
            row += createOrigTD(bgClass, 'Midrag', data[i].Itgonenut_Id, data[i].MidragText, data[i].MidragValue);
            bgClass = getClassByExactValuePop(data[i].ChampAffectValue);
            row += createOrigTD(bgClass, 'Champ_Affect', data[i].Itgonenut_Id, data[i].ChampAffectText, data[i].ChampAffectValue);
           
            row += createTDWithWrap(data[i].Itgonenut_Id, 'Explantion_Champ_Affect', data[i].Explantion_Champ_Affect);


            var strClick = createSTRClick(data[i].Itgonenut_Id);
            var strClickUndo = createSTRUndoClick(data[i].Itgonenut_Id);
            var disableAllEditButtons = '';
            if (data[0].isUserEdited == 1)//
            {
                disableAllEditButtons = 'disabled="disabled"';
            }

            var strButton = createEditButton(data[i].Itgonenut_Id, disableAllEditButtons, strClick, strClickUndo);

            if (userTypeId != "-1") {
                if (data[i].user_name == null) // no user took this button yet, for reguler users
                {
                    row += strButton;
                }
                else if ('<%=Session["UserName"]%>'  != '' && data[i].user_name == '<%=Session["UserName"]%>' ) // the current user took this button
                {
                    strUndoClick = createSTRUndoClick(data[i].Itgonenut_Id)
                    strButton = createSaveButton(data[i].Itgonenut_Id, strClick, strUndoClick);
                    row += strButton;
                    //first time
                    if (timer_gear == -1)//
                    {
                        glbEditedLine_ItgonenutId = data[i].Itgonenut_Id;
                        startTimerForUndoCheckout(data[i].Itgonenut_Id, data_undoCheckoutTimeoutInMS);

                    }
                }
                else //
                {
                    row += '<td id="TD_Edit_' + data[i].Itgonenut_Id + '" ></td>';
                }
            }

            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].Itgonenut_Id + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_ItgonenutId);
            }
        }

        function createEditDDL(ItgonenutId) {

            var tdChamp_Affect = $('#TD_Champ_Affect_' + ItgonenutId);
            setValuesForUndoCheckout(glbEditedLine_Champ_Affect_BeforeRefresh, tdChamp_Affect.html())
            var tdExplantion_Champ_Affect = $('#TD_Explantion_Champ_Affect_' + ItgonenutId);
            glbEditedLine_Explantion_Champ_Affect_BeforeRefresh = tdExplantion_Champ_Affect.html();
            tdChamp_Affect.empty();
            tdChamp_Affect.attr('class', 'tdEditClass');
            tdExplantion_Champ_Affect.empty();
            tdExplantion_Champ_Affect.attr('class', 'tdEditClass');
            cboComander_Affect = createCombo(curCombosDT, 'Champ_Affect', tdChamp_Affect, ItgonenutId, true, glbEditedLine_Champ_Affect_BeforeRefresh.val, true);
            createTextArea(tdExplantion_Champ_Affect, 'Explantion_Champ_Affect', ItgonenutId, glbEditedLine_Explantion_Champ_Affect_BeforeRefresh, 400);
        }

        //make 3 values textboxes with color ranges
        function createTdColoredData(dataRow, fieldId) //
        {
            //if glbEditedLine_ItgonenutId != -1 then put the saved value because we are with edited line
            editValue = dataRow[fieldId]; //original value from db

            var disabled = 'disabled="disabled"';
            //(time_gear=-1) = disabled     (time_gear=1) = enabled  
            if (timer_gear == 1 && glbEditedLine_ItgonenutId == dataRow['Itgonenut_Id'])//
            {
                disabled = "";
            }

            //value textBox by fieldId with colors
            if (dataRow[fieldId] >= data_color_red_gt_or_eq && dataRow[fieldId] <= data_color_red_lt_or_eq) //
            {
                return '<td><input class="data_color_red" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_yellow_gt_or_eq && dataRow[fieldId] <= data_color_yellow_lt_or_eq) //
            {
                return '<td><input class="data_color_yellow" type="text" ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '" onblur="checkLower100(this);"/></td>';
            }
            else if (dataRow[fieldId] >= data_color_green_gt_or_eq && dataRow[fieldId] <= data_color_green_lt_or_eq) //
            {
                return '<td><input class="data_color_green" type="text"  ' + disabled + ' id="' + fieldId + '_' + dataRow['Itgonenut_Id'] + '" value="' + editValue + '"  onblur="checkLower100(this);"/></td>';
            }
        }


        function checkLower100(obj)//
        {
            if (isNaN(obj.value) || obj.value > 100 || obj.value < 0) //
            {
                alert('יש להזין מספר בין 0 ל 100');
            }
        }


        function checkOutOrSave(ItgonenutId)//
        {
            var userName = '<%=Session["UserName"]%>';
            checkSession(userName);
            var pageName ='Aluf';
            if ($("#btnEdit" + ItgonenutId).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName, op: 'checkout', 'Itgonenut_Id': ItgonenutId, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(ItgonenutId, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName)
                            createEditDDL(ItgonenutId);
                            glbEditedLine_ItgonenutId = ItgonenutId;
                            startTimerForUndoCheckout(ItgonenutId, data_undoCheckoutTimeoutInMS)
                        }
                    }
                });
            }
            else//
            {
                var champ_affect = {};
                setValuesForUndoCheckoutFromTextOnly(champ_affect, "Champ_Affect", ItgonenutId)
                var explantion_champ_affect = $("#INP_Explantion_Champ_Affect_" + ItgonenutId).val();
                if ((champ_affect.val != '0' && champ_affect.val != '') && explantion_champ_affect == '') {
                    alert('הכנס ערך בשדה "הסבר להנחיית אלוף"');
                    return;
                }
                var jsonObj =
                        {
                            page: pageName, "op": "save", "ItgonenutId": ItgonenutId, "userId": '<%=Session["userID"]%>',
                            "champ_affect": champ_affect.val, "explantion_champ_affect": explantion_champ_affect
                        };
            //alert(JSON.stringify(jsonObj));


            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);
                    if (data[0].result == "false") //
                    {
                        alert("השמירה נכשלה");
                    }
                    else//
                    {
                        resetEdit(champ_affect, explantion_champ_affect, ItgonenutId);

                        // enableDisbaleUserLine(userType, 'disable');
                        //$("#cph_region" + userType + "_EditedBy").val("");//after save clear my user from "active user cell"
                    }
                }
            });
        }
    }

        function UndoCheckout(Itgonenut_Id) //
        {

            pageName = 'Final_Editing'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'Itgonenut_Id': Itgonenut_Id };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_Champ_Affect_BeforeRefresh, glbEditedLine_Explantion_Champ_Affect_BeforeRefresh);
        }


        function resetEdit(champ_affect, explantion_champ_affect, ItgonenutId) {
            resetEditButtonsConfiguration(glbEditedLine_ItgonenutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME');
            setTDValTextAndColorObj($('#TD_Champ_Affect_' + glbEditedLine_ItgonenutId), champ_affect, true);
            $('#TD_Explantion_Champ_Affect_' + glbEditedLine_ItgonenutId).html(explantion_champ_affect);

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_ItgonenutId = -1;

            clearTimeout(glbTimerObject);
        }






        </script>

      <div style="background-color:#fe6f3e; direction:rtl;margin-bottom:30px;">
              <!--h1 style="text-align:center;font-size:20px;color:white; margin-bottom: 6px; height:50px;" >גיל"ה והפצה</!--h1-->
              <img src="../Images/החלטת אלוף-01.png" class="mainBTNSmall" style="text-align:right; vertical-align:bottom" />
            <span style=" color:white; vertical-align:middle;text-align: center;  font-size:50px;line-height: 80px;">&nbsp; החלטת אלוף</span>
          </div>
      <div style="width: 99%; margin: 0 auto;">
    
        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                    <col style="width: 12%;" />
                    <col style="width: 12%;" /><!-- 24%-->
                    <col style="width: 12%;" /><!-- 36%-->          
                    <col style="width: 12%;" />
                    <col style="width: 36%;" /><!-- 84%-->          
                    <col style="width: 8%;" />
                    <col style="width: 8%;" />

                   
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">טווחים </th>
                    <th class="TBLHeader">אזור התגוננות</th>
                    <th class="TBLHeader">החלטת מערכת</th>       
                    <th class="TBLHeader">הנחיית אלוף</th> 
                    <th class="TBLHeader">הסבר להנחיית אלוף</th>
              
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

