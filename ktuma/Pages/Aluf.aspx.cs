﻿using pikudGui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Aluf : BasePage
{
    public string urlItgonenutId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            urlItgonenutId = Request.QueryString.Get("Itgonenut_Id".ToLower());
        }
    }
}
