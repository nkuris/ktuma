﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true" CodeFile="NafaFill.aspx.cs" Inherits="Pages_NafaFill" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
        var curCombosDT;
        var glbEditedLine_rashutId = -1;
        var glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh = {};
        var glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh = '';
        var glbEditedLine_ExceptionPolicy_BeforeRefresh = '';

    $(document).ready(function ()//
    {
        var uId = '<%=Session["userID"]%>';
        checkSession(uId)
        loadCombosNafa();
        loadParams(uId,true, false);
        loadAdminSviva('<%=Session["SvivaId"]%>');
        
    });

     function loadCombosNafa() {
            var userName = '<%=Session["UserName"]%>';
         checkSession(userName)

            var pageName = 'NafaFill';
            var jsonObj =
                    { page: pageName, op: 'loadCombosByPage', 'userName': userName };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    curCombosDT = data;

                }
            });


    }

    function refresh(isFullRefresh) //
    {
        var userName = '<%=Session["UserName"]%>';
        var userId = '<%=Session["userID"]%>';
        checkSession(userName)
        var jsonObj =
            { op: 'loadNafaFill', 'userName': userName, 'userId': userId };

        $.ajax({
            type: "POST",
            url: "../Handler.ashx",
            data: jsonObj,
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            },
            success: function (data) {
                //console.log(data);

                fillTable(data, isFullRefresh, userName);

            }
        });
    }

     function fillTable(data, isFullRefresh) //
     {
         var userTypeId = $('#HID_UserTypeId').val();var userGroupId = '<%=Session["UserGroupId"]%>';
         if (userGroupId != 3) {
                userTypeId = "-1";
            }
         if (userTypeId == "-1") {
             $('#TH_Actions').css('display', 'none');
         }
         if (isFullRefresh) //********************* full redraw table
         {
             //remove all rows except first one
             $("#tblData").find("tr:gt(0)").remove();
             for (var i = 0; i < data.length; i++)//
             {
                 buildTableRow(data, i, userTypeId)
             }           
             
         }
         else //only refresh values !!!
         {
             //only refresh don't redraw table
             var i = 0;
             $('#tblData tr:gt(0)').each //start from seconed row (:greater(0)), first row is titles
             (//
                 function ()//
                 {
                     if (glbEditedLine_rashutId != -1 && glbEditedLine_rashutId == data[i].rashutid)//we have edited line so skip it
                     {
                         //  $(this).find('input#data_24_' + data[i].rashutid).val(glbEditedLine_Val24_BeforeRefresh);
                         //  $(this).find('input#data_48_' + data[i].rashutid).val(glbEditedLine_Val48_BeforeRefresh);
                         //  $(this).find('input#data_72_' + data[i].rashutid).val(glbEditedLine_Val72_BeforeRefresh);
                     }
                     else {
                         $('#TD_Rashut_' + data[i].rashutid).html(data[i].RashutName);
                         $('#TD_ItgonenutRegion_' + data[i].rashutid).html(data[i].regionName);
                         $('#TD_CurrentItgonenutPolicy_' + data[i].rashutid).html(data[i].CurrentItgonenutPolicyText);
                         setTDValTextAndColorShowTextForNull($('#TD_RecommendedItgonenutPolicy_' + data[i].rashutid), data[i].RecommendedItgonenutPolicy, data[i].RecommendedItgonenutPolicyText, true);
                         $('#TD_ExplanationRecommendedPolicy_' + data[i].rashutid).html(data[i].ExplanationRecommendedPolicy);
                         $('#TD_ExceptionPolicy_' + data[i].rashutid).html(data[i].ExceptionPolicy);                         
                         var disableAllEditButtons = '';
                         if (data[0].isUserEdited == 1)//
                         {
                             disableAllEditButtons = 'disabled="disabled"';
                         }
                         if ((data[i].user_name == '' || data[i].user_name == null) && $('#TD_Edit_' + data[i].rashutid).html() == '') {//add edit button
                             var edit = $('#TD_Edit_' + data[i].rashutid);
                             var strClick = createSTRClick(data[i].rashutid);
                             var strButton = createEditButtonNoExternal(data[i].rashutid, disableAllEditButtons, strClick);
                             //var strButton = '<input type="button" ' + disableAllEditButtons + ' class="btnEdit" value="ערוך" id="btnEdit' + data[i].rashutid + strClick; //edit button (checkOut)
                             edit.html(strButton);
                         }
                         else if ((data[i].user_name != '' && data[i].user_name != null) && $('#TD_Edit_' + data[i].rashutid).html() != '') {//remove edit button
                             var edit = $('#TD_Edit_' + data[i].rashutid);
                             edit.html('');
                         }
                     }
                     //update user checkout on row
                     $(this).find('.data_user').html(data[i].user_name);
                     i++;
                 }//
             ); //END .each
         }
     }

     function buildTableRow(data, i, userTypeId) {
         var row = '';
         row = '';

         if (data[i].CurrentItgonenutPolicyText == null || data[i].CurrentItgonenutPolicyText == undefined) {
             data[i].CurrentItgonenutPolicyText = '';
         }
         if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
             data[i].RecommendedItgonenutPolicyText = '';
         }

         if (data[i].ExplanationRecommendedPolicy == null || data[i].ExplanationRecommendedPolicy == undefined) {
             data[i].ExplanationRecommendedPolicy = '';
         }
         if (data[i].ExceptionPolicy == null || data[i].ExceptionPolicy == undefined) {
             data[i].ExceptionPolicy = '';
         }
         row += '<td style="text-align:center">' + data[i].RashutName + '</td>';
         row += '<td style="text-align:center">' + data[i].regionName + '</td>';
         bgClass = getClassByExactValuePop(data[i].CurrentItgonenutPolicy);
         row += createOrigTDIncNull(bgClass, 'CurrentItgonenutPolicy', data[i].rashutid, data[i].CurrentItgonenutPolicyText, data[i].CurrentItgonenutPolicy);                  
         bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
         row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].rashutid, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);                           
         row += '<td id="TD_ExplanationRecommendedPolicy_' + data[i].rashutid + '" class="">' + data[i].ExplanationRecommendedPolicy + '</td>';
         row += '<td id="TD_ExceptionPolicy_' + data[i].rashutid + '" class="">' + data[i].ExceptionPolicy + '</td>';


         var strClick = createSTRClick(data[i].rashutid);
         var strClickUndo = createSTRUndoClick(data[i].rashutid);
         var disableAllEditButtons = '';
         if (data[0].isUserEdited == 1)//
         {
             disableAllEditButtons = 'disabled="disabled"';
         }

         var strButton = createEditButton(data[i].rashutid, disableAllEditButtons, strClick, strClickUndo);

         if (userTypeId != "-1") {
             if (data[i].user_name == null) // no user took this button yet, for reguler users
             {
                 row += strButton;
             }
             else if ('<%=Session["UserName"]%>' != '' && data[i].user_name == '<%=Session["UserName"]%>') // the current user took this button
             {
                 strUndoClick = createSTRUndoClick(data[i].rashutid)
                 strButton = createSaveButton(data[i].rashutid, strClick, strUndoClick);
                 row += strButton;
                 //first time
                 if (timer_gear == -1)//
                 {
                     glbEditedLine_rashutId = data[i].rashutid;
                     startTimerForUndoCheckout(data[i].rashutid, data_undoCheckoutTimeoutInMS);

                 }
             }
             else //
             {
                 row += '<td id="TD_Edit_' + data[i].rashutid + '" ></td>';
             }

         }
            if (data[i].user_name == null)//
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].rashutid + '"></td>';
            }
            else //
            {
                row += '<td class="data_user" id="TD_USERNAME_' + data[i].rashutid + '">' + data[i].user_name + '</td>';
            }

            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
            if (data[i].user_name == '<%=Session["UserName"]%>') {
                createEditDDL(glbEditedLine_rashutId);
            }
    }

      function UndoCheckout(rashutid) //
        {

            pageName = 'NafaFill'
            var jsonObj =
                    { page: pageName, op: 'undoCheckout', 'rashutId': rashutid };
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);
                    //fillTable(data);
                }
            });
            resetEdit(glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh, glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh,
                glbEditedLine_ExceptionPolicy_BeforeRefresh,rashutid);
        }

     function checkOutOrSave(rashutid)//
        {
            var userName = '<%=Session["UserName"]%>';
         checkSession(userName);
         var pageName = "NafaFill";
         if ($("#btnEdit" + rashutid).val() == 'ערוך')//
            {
                var jsonObj =
                        { page: pageName,  op: 'checkout', 'rashutId': rashutid, 'user_name': userName };
                $.ajax({
                    type: "POST",
                    url: "../Handler.ashx",
                    data: jsonObj,
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data[0].result == "false") //
                        {
                            alert("לא ניתן לערוך פריט זה מאחר ומשתמש אחר מבצע עריכה");
                        }
                        else//
                        {
                            ShowSaveButtonsConfiguration(rashutid, 'btnEdit', 'btnEdit', 'btnUndo', 'TD_USERNAME', userName);

                            createEditDDL(rashutid);
                            glbEditedLine_rashutId = rashutid;                            
                            startTimerForUndoCheckout(rashutid, data_undoCheckoutTimeoutInMS)
                            
                        }
                    }
                });
            }
            else//
            {
                var recommendedPolicy = {};              
                var ExceptionPolicy = $("#INP_ExceptionPolicy_" + rashutid).val();
                var ExplanationRecommendedPolicy = $("#INP_ExplanationRecommendedPolicy_" + rashutid).val();

                setValuesForUndoCheckoutFromTextOnly(recommendedPolicy, "RecommendedItgonenutPolicy", rashutid)                               

                if ((recommendedPolicy.val != '0' && recommendedPolicy.val != '') && ExplanationRecommendedPolicy == '')
                {
                    alert('הכנס ערך בשדה "הסבר"');
                    return;
                }                

                var jsonObj =
                {
                    page: pageName, "op": "save", "rashutId": rashutid, "userId": '<%=Session["userID"]%>',
                    "recommendedPolicy": recommendedPolicy.val, "ExplanationRecommendedPolicy": ExplanationRecommendedPolicy
                    , "ExceptionPolicy": ExceptionPolicy
                };
         
                //console.log('1: ' + Date.now())
            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //alert("koki" + dataArray);
                    if (data[0].result == "false") //
                    {
                        alert("השמירה נכשלה");
                    }
                    else//
                    {
                                         
                        resetEdit(recommendedPolicy, ExplanationRecommendedPolicy, ExceptionPolicy, rashutid);
                         
                     
                    }
                }
            });
        }
    }

        function createEditDDL(rashutid)
        {
            var tdRecommendedItgonenutPolicy = $('#TD_RecommendedItgonenutPolicy_' + rashutid);
            setValuesForUndoCheckout(glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh, tdRecommendedItgonenutPolicy.html())
            var tdExplanationRecommendedPolicy = $("#TD_ExplanationRecommendedPolicy_" + rashutid);
            glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh = tdExplanationRecommendedPolicy.html();
            var tdExceptionPolicy = $("#TD_ExceptionPolicy_" + rashutid);
            glbEditedLine_ExceptionPolicy_BeforeRefresh = tdExceptionPolicy.html();            
            tdRecommendedItgonenutPolicy.empty();
            tdRecommendedItgonenutPolicy.attr('class', 'tdEditClass');
            tdExplanationRecommendedPolicy.empty();
            tdExplanationRecommendedPolicy.attr('class', 'tdEditClass');
            tdExceptionPolicy.empty();
            tdExceptionPolicy.attr('class', 'tdEditClass');
            cboRecommendedItgonenutPolicy = createCombo(curCombosDT, 'RecommendedItgonenutPolicy', tdRecommendedItgonenutPolicy, rashutid, false, glbEditedLine_RecommendedItgonenutPolicy_BeforeRefresh.val, true, false);            
            createTextArea(tdExplanationRecommendedPolicy, 'ExplanationRecommendedPolicy', rashutid, glbEditedLine_ExplanationRecommendedPolicy_BeforeRefresh, 120);
            createTextArea(tdExceptionPolicy, 'ExceptionPolicy', rashutid, glbEditedLine_ExceptionPolicy_BeforeRefresh,120);
        }

        


    function resetEdit(recommendedPolicy, ExplanationRecommendedPolicy, ExceptionPolicy, rashutId) {
        resetEditButtonsConfiguration(glbEditedLine_rashutId, 'btnEdit', 'btnEditDisabled', 'btnUndo', 'TD_USERNAME')
        setTDValTextAndColorObj($('#TD_RecommendedItgonenutPolicy_' + glbEditedLine_rashutId), recommendedPolicy, true);            
            $('#TD_ExplanationRecommendedPolicy_' + glbEditedLine_rashutId).html(ExplanationRecommendedPolicy);
            $('#TD_ExceptionPolicy_' + glbEditedLine_rashutId).html(ExceptionPolicy);           

            timer_gear = -1; //reset the flag to enable a new timeout
            glbEditedLine_rashutId = -1;

            clearTimeout(glbTimerObject);
        }



</script>
       <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 6%;" /> <!--12%-->
                    <col style="width: 18%;" />
                    <col style="width: 18%;" />
                    <col style="width: 18%;" /><!--66%-->
                    <col style="width: 10%;" />
                    <col style="width: 7%;" />
                    <col style="width:7%;" /> <!--90% -->
                </colgroup>
                <tr class="TBLHeader">
                      <th class="TBLHeader">רשות
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות נוכחית
                    </th>                  
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>
                    <th class="TBLHeader">הסבר להמלצה
                    </th>
                    
                     <th class="TBLHeader">דרישה להתאמת מדיניות לאזור אחר
                    </th>
                    <th class="TBLHeader" id="TH_Actions"></th>
                    <th class="TBLHeader">משתמש פעיל</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

