﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true" 
    CodeFile="ExceptionFillHistory.aspx.cs" Inherits="Pages_ExceptionFillHistory" %>
<%@ MasterType VirtualPath="~/MasterPages/HeadingNafot.master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <script type="text/javascript">
               
        $(document).ready(function ()//
        {
            var uId = '<%=Session["userID"]%>';
            loadParams(uId, false, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');           
            var a2 = loadDistrictsDT();
            var a3 = loadItgonenutRegionsDT();
            var a4 = loadRashuyotDT();
            var a5 = loadNafotDT();
            setSearchCriteria(true);
            loadExceptionsHistory(true);
            
        });
                    
        
        function loadExceptionsHistory(initialSearch) {
            //initial values are from the masterpage page            
             var jsonObj =
             {
                 op: 'loadExceptionFillHistory', 'initialSearch': initialSearch,
                 'searchFromDate': searchFromDate, 'searchToDate': searchToDate,
                 'searchDistricts': searchDistricts, 'searchRashuyot': searchRashuyot,
                 'searchNafot': searchNafot
             };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data);
                    
                 }
             }).then;
        }

        function fillTable(data, isFullRefresh) //
        {

            //remove all rows except first one
            $("#tblData").find("tr:gt(0)").remove();
            for (var i = 0; i < data.length; i++)//
            {
                buildTableRow(data, i)
            }

        }

        function buildTableRow(data, i) {
            var row = '';
            row = '';

             var row = '';
            row = '';

            if (data[i].ParameterText == null || data[i].ParameterText == undefined) {
                data[i].ParameterText = '';
            }
            if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
                data[i].RecommendedItgonenutPolicyText = '';
            }

            if (data[i].TourSecurityText == null || data[i].TourSecurityText == undefined) {
                data[i].TourSecurityText = '';
            }

            if (data[i].ParameterText == null || data[i].ParameterText == undefined) {
                data[i].ParameterText = '';
            }

            if (data[i].InstitutionName == null || data[i].InstitutionName == undefined) {
                data[i].InstitutionName = '';
            }
            if (data[i].ExplanationToException == null || data[i].ExplanationToException == undefined) {
                data[i].ExplanationToException = '';
            }
            if (data[i].regionsToName == null || data[i].regionsToName == undefined) {
                data[i].regionsToName = ''
            }
            if (data[i].Exception == null || data[i].Exception == undefined) {
                data[i].Exception = '';
            }
            if (data[i].SecurityTourText == null || data[i].SecurityTourText == undefined) {
                data[i].SecurityTourText = '';
            }
            row += '<td style="text-align:center">' + displayTime(data[i].ItgonenutPolicyDate, false) + '</td>';
            row += '<td id="TD_Itgonenut_Region_Name_' + data[i].ExceptionFillRowId + '" class="">' + data[i].Name + '<input id="Inp_ItgonenutRegion_Id_' + data[i].ExceptionFillRowId + '" type="hidden" value="' + data[i].Itgonenut_Id + '" />' + '</td>';
            row += '<td id="TD_RashutName_' + data[i].ExceptionFillRowId + '" class="">' + data[i].rashutName + '<input id="Inp_RashutId_' + data[i].ExceptionFillRowId + '" type="hidden" value="' + data[i].rashutId + '" />' + '</td>';    
            row += '<td id="TD_DistrictName_' + data[i].ExceptionFillRowId + '" class="">' + data[i].DistrictName + '</td>';            
            bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].ExceptionFillRowId, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);
            row += '<td id="TD_Parameter_' + data[i].ExceptionFillRowId + '" class="">' + data[i].ParameterText + '<input id="Inp_Parameter_' + data[i].ExceptionFillRowId + '" type="hidden" value="' + data[i].Parameter + '" />' + '</td>';
            row += '<td id="TD_InstitutionName_' + data[i].ExceptionFillRowId + '" class="">' + data[i].InstitutionName  +'</td>';
            row += '<td id="TD_ExplanationToException_' + data[i].ExceptionFillRowId + '" class="">' + data[i].ExplanationToException + '</td>';
            row += '<td id="TD_TourSecurity_' + data[i].ExceptionFillRowId + '" class="">' + '<input id="Inp_TourSecurity_' + data[i].ExceptionFillRowId + '" type="hidden" value="' + data[i].TourSecurity + '" />' + data[i].TourSecurityText + '</td>';    
            row += '<td id="TD_Exception_' + data[i].ExceptionFillRowId + '" class="">' + data[i].Exception + '</td>';    
            row += '<td id="TD_ItgonenutRegionExceptTo_' + data[i].ExceptionFillRowId + '" class="">' + data[i].regionsToName + '<input id="Inp_ItgonenutRegionExceptTo_' + data[i].ExceptionFillRowId + '" type="hidden" value="' + data[i].ItgonenutRegionExceptTo + '" />' + '</td>';                
            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')           
        }
        
   </script>
        <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 8%;" /> <!--14%-->                    
                    <col style="width: 10%;" /><!--24%-->
                    <col style="width: 10%;" /><!--34%-->
                    <col style="width: 10%;" /><!--44%-->
                    <col style="width: 10%;" /><!--54%-->          
                    <col style="width: 10%;" /><!--64%-->
                    <col style="width: 10%;" /><!--74%-->                    
                    <col style="width: 10%;" /><!--84%-->          
                    <col style="width: 10%;" /><!--94%-->                              
                    <col style="width: 6%;" /><!--100%-->                              
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">תאריך</th>
                    <th class="TBLHeader">אזור התגוננות</th>                    
                    <th class="TBLHeader">שם הרשות</th>
                    <th class="TBLHeader">מחוז</th>
                    <th class="TBLHeader">מדיניות התגוננות מומלצת</th>                  
                    <th class="TBLHeader">הפרמטר</th>
                    <th class="TBLHeader">שם המוסד</th>                                                        
                    <th class="TBLHeader">הסבר לבקשה</th>                 
                    <th class="TBLHeader">האם בוצע סיור איש מקצוע במקום</th>                 
                    <th class="TBLHeader">הבקשה</th>                 
                     <th class="TBLHeader">החרגה לאזור התגוננות</th>
                </tr>
            </table>
        </div>
        
    </div>
</asp:Content>

