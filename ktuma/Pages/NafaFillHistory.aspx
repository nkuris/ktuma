﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true"
    CodeFile="NafaFillHistory.aspx.cs" Inherits="Pages_NafaFillHistory" %>
<%@ MasterType VirtualPath="~/MasterPages/HeadingNafot.master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function ()//
        {
            var uId = '<%=Session["userID"]%>';
            loadParams(uId, false, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');
            var a1 = loadNafotDT();
            var a2 = loadDistrictsDT();
            var a3 = loadItgonenutRegionsDT();
            setSearchCriteria(true);    
            loadNafotHistory(true);

        });

          function loadNafotHistory(initialSearch) {
            //initial values are from the masterpage page            
             var jsonObj =
             {
                 op: 'loadNafotFillHistory', 'initialSearch': initialSearch,
                 'searchFromDate': searchFromDate, 'searchToDate': searchToDate,
                 'searchDistricts': searchDistricts, 'searchItgonenutRegions': searchItgonenutRegions,
                 'searchNafot': searchNafot
             };

             $.ajax({
                 type: "POST",
                 url: "../Handler.ashx",
                 data: jsonObj,
                 dataType: "json",
                 failure: function (response) {
                     alert(response.d);
                 },
                 success: function (data) {
                     //console.log(data);

                     fillTable(data);
                    
                 }
             }).then;
        }

        function fillTable(data, isFullRefresh) //
        {

            //remove all rows except first one
            $("#tblData").find("tr:gt(0)").remove();
            for (var i = 0; i < data.length; i++)//
            {
                buildTableRow(data, i)
            }

        }

        function buildTableRow(data, i) {
            var row = '';
            row = '';

            if (data[i].CurrentItgonenutPolicyText == null || data[i].CurrentItgonenutPolicyText == undefined) {
                data[i].CurrentItgonenutPolicyText = '';
            }
            if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
                data[i].RecommendedItgonenutPolicyText = '';
            }

            if (data[i].ExplanationRecommendedPolicy == null || data[i].ExplanationRecommendedPolicy == undefined) {
                data[i].ExplanationRecommendedPolicy = '';
            }
            if (data[i].ExceptionPolicy == null || data[i].ExceptionPolicy == undefined) {
                data[i].ExceptionPolicy = '';
            }
           
            row += '<td style="text-align:center">' + displayTime(data[i].ItgonenutPolicyDate, false) + '</td>';
            row += '<td style="text-align:center">' + data[i].RashutName + '</td>';
            row += '<td style="text-align:center">' + data[i].regionName + '</td>';
          
            bgClass = getClassByExactValuePop(data[i].CurrentItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'CurrentItgonenutPolicy', data[i].Itgonenut_Id, data[i].CurrentItgonenutPolicyText, data[i].CurrentItgonenutPolicy);
            bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].Itgonenut_Id, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);            
            row += '<td id="TD_ExplanationRecommendedPolicy_' + data[i].Itgonenut_Id + '" class="">' + data[i].ExplanationRecommendedPolicy + '</td>';
            row += '<td id="TD_ExceptionPolicy_' + data[i].rashutid + '" class="">' + data[i].ExceptionPolicy + '</td>';
            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
        }
    </script>
         <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 6%;" /> <!--12%-->
                    <col style="width: 18%;" />
                    <col style="width: 18%;" />
                    <col style="width: 18%;" /><!--66%-->
                    <col style="width: 10%;" />
                    <col style="width: 7%;" />                    
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">תאריך
                    </th>
                      <th class="TBLHeader">רשות
                    </th>
                    <th class="TBLHeader">אזור התגוננות
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות נוכחית
                    </th>                  
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>
                    <th class="TBLHeader">הסבר להמלצה
                    </th>
                    
                     <th class="TBLHeader">דרישה להתאמת מדיניות לאזור אחר
                    </th>                
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

