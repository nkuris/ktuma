﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewLogs.aspx.cs" Inherits="ViewLogs" MasterPageFile="~/MasterPages/Heading.master"%>
<%@ Register TagPrefix="uc1" Src="~/Controls/drpOnlySelect,ascx.ascx" TagName="drpOnlySelect" %>
<%@ Register TagPrefix="ctl" TagName="viewOnlyGrid" Src="~/controls/viewOnlyGrid.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="MainDivider"><h1 class="h1MainDiv">תוצאות הכנסה לארכיון של מערכת כתומ"ה</h1></div>
    <div style="margin-top:30px;">
    <div style="margin-bottom:20px;text-align:right;">
<br /><br />
  <div style="margin:auto;width:800px;">
<table  class="tableRegularRight" style="margin-bottom:20px;">
    <tr>
        <td>
            <uc1:drpOnlySelect fillOnLoad="true"  id="drpOrder" runat="server" 
            drpDataTextField="taskOrder_desc"
        drpDataValueField="taskOrder_id"
        LabelCaption="בחר קובץ:" SelectCommand="getTaskOrdersWithDesc"
        inParamsNames="taskType" />        
        </td>
</tr>
<tr>
    <td align="center" colspan="2">
       <asp:Button ID="btnDo" runat="server" Text="אישור" OnClick="btnDo_Click" CssClass="btnEditKtumaSmall"/>
    </td>
</tr>
<tr>
            <td>
            <a runat="server" id="lnkDownload" href="" visible="false">פתח / שמור את הקובץ שנוצר בתהליך </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a runat="server" id="lnkLog" style="color:Maroon;" href="" visible="false">פתח / שמור קובץ שגויים להזמנה נוכחית</a>
            </td>
            
      </tr>
</table>
</div>
<div style="margin:auto;width:800px;">
<ctl:viewOnlyGrid runat="server" ID="grid" />
    </div>
</div>
         <div style="margin:auto;width:800px;text-align:left;">
        <asp:Button ID="BTN_GoToUpload" runat="server" Text="חזור לעמוד ניהול" OnClick="btnGoToUpload_Click" CssClass="btnEditKtumaSmall"/>
     </div>
        </div>
    </asp:Content>
