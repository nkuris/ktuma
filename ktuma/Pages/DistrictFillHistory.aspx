﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/HeadingNafot.master" AutoEventWireup="true" CodeFile="DistrictFillHistory.aspx.cs"
    Inherits="Pages_DistrictFillHistory" %>
<%@ MasterType VirtualPath="~/MasterPages/HeadingNafot.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
               
        $(document).ready(function ()//
        {
            var uId = '<%=Session["userID"]%>';
            loadParams(uId, false, false);
            loadAdminSviva('<%=Session["SvivaId"]%>');           
            var a2 = loadDistrictsDT();
            var a3 = loadItgonenutRegionsDT();
            setSearchCriteria(true);
            loadDistrictHistory(true);
            
        });
                    
        
        function loadDistrictHistory(initialSearch) {
            //initial values are from the masterpage page            
            var jsonObj =
            {
                op: 'loadDistrictFillHistory', 'initialSearch': initialSearch,
                'searchFromDate': searchFromDate, 'searchToDate': searchToDate,
                'searchDistricts': searchDistricts, 'searchItgonenutRegions': searchItgonenutRegions
            };

            $.ajax({
                type: "POST",
                url: "../Handler.ashx",
                data: jsonObj,
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (data) {
                    //console.log(data);

                    fillTable(data);

                }
            });
        }

        function fillTable(data, isFullRefresh) //
        {

            //remove all rows except first one
            $("#tblData").find("tr:gt(0)").remove();
            for (var i = 0; i < data.length; i++)//
            {
                buildTableRow(data, i)
            }

        }

        function buildTableRow(data, i) {
            var row = '';
            row = '';

            if (data[i].CurrentItgonenutPolicyText == null || data[i].CurrentItgonenutPolicyText == undefined) {
                data[i].CurrentItgonenutPolicyText = '';
            }
            if (data[i].RecommendedItgonenutPolicyText == null || data[i].RecommendedItgonenutPolicyText == undefined) {
                data[i].RecommendedItgonenutPolicyText = '';
            }

            if (data[i].ExplanationRecommendedPolicy == null || data[i].ExplanationRecommendedPolicy == undefined) {
                data[i].ExplanationRecommendedPolicy = '';
            }           
            if (data[i].Adequacy == null || data[i].Adequacy == undefined) {
                data[i].Adequacy = '';
            }
            row += '<td style="text-align:center">' + displayTime(data[i].ItgonenutPolicyDate, false) + '</td>';
            row += '<td style="text-align:center">' + data[i].Name + '</td>';
            row += '<td style="text-align:center">' + data[i].districtName + '</td>';
            bgClass = getClassByExactValuePop(data[i].CurrentItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'CurrentItgonenutPolicy', data[i].Itgonenut_Id, data[i].CurrentItgonenutPolicyText, data[i].CurrentItgonenutPolicy);
            bgClass = getClassByExactValuePop(data[i].RecommendedItgonenutPolicy);
            row += createOrigTDIncNull(bgClass, 'RecommendedItgonenutPolicy', data[i].Itgonenut_Id, data[i].RecommendedItgonenutPolicyText, data[i].RecommendedItgonenutPolicy);
            row += '<td id="TD_Adequacy_' + data[i].Itgonenut_Id + '" class="">' + data[i].Adequacy + '</td>';
            row += '<td id="TD_ExplanationRecommendedPolicy_' + data[i].Itgonenut_Id + '" class="">' + data[i].ExplanationRecommendedPolicy + '</td>';
            $('#tblData').append('<tr class="TBLRow">' + row + '</tr>')
        }
        
   </script>
        <div style="width: 99%; margin: 0 auto;">

        <div style="height: 700px; overflow-y: auto;">
            <table style="width: 100%;" class="tblRes" id="tblData">
                <colgroup>
                     <col style="width: 6%;" />
                    <col style="width: 10%;" /> <!--16%-->                    
                    <col style="width: 12%;" /><!--32%-->
                    <col style="width: 20%;" /><!--44%-->
                    <col style="width: 20%;" />
                    <col style="width: 12%;" />                    
                    <col style="width: 12%;" />                    
                </colgroup>
                <tr class="TBLHeader">
                    <th class="TBLHeader">תאריך</th>
                      <th class="TBLHeader">אזור התגוננות</th>                    
                    <th class="TBLHeader">מחוז</th>
                    <th class="TBLHeader">מדיניות התגוננות נוכחית
                    </th>
                    <th class="TBLHeader">מדיניות התגוננות מומלצת
                    </th>                  
                    <th class="TBLHeader">הלימה במדיניות התגוננות
                    </th>
                    <th class="TBLHeader">הסבר להמלצה
                    </th>                                                        
                </tr>
            </table>
        </div>
        
    </div>
</asp:Content>

